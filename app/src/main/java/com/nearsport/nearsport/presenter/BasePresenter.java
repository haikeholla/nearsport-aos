package com.nearsport.nearsport.presenter;
/**
 * Created by sguo on 6/9/16.
 */
public interface BasePresenter<T extends View> {
    void onCreate();
    void onStart();
    void onStop();
    void onPause();
    void attachView(T view);
}
