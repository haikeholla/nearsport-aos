package com.nearsport.nearsport.signup;

import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.presenter.View;

/**
 * Created by sguo on 6/9/16.
 */
public interface SignupView extends View {
    void showProgressing();
    void showSignupSuccess(ApiBaseResponse res);
    void showSignupFailure(Throwable t);

    void showNameError(String msg);
    void showEmailError(String msg);
    void showPasswordError(String msg);
}
