package com.nearsport.nearsport.signup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.login.LoginActivity;
import com.nearsport.nearsport.util.Util;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity implements SignupView {
    private static final String TAG = "SignupActivity";

    @Inject
    public UserService userService;

    @Bind(R.id.input_name)
    EditText nameText;
    @Bind(R.id.input_email) EditText emailText;
    @Bind(R.id.input_password) EditText passwordText;
    @Bind(R.id.btn_signup)
    Button signupButton;
    @Bind(R.id.link_login)
    TextView loginLink;

    private ProgressDialog progressDialog;

    public SignupPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        NearSportApplication.component().inject(this);
        ButterKnife.bind(this);
//        InjectHelper.getRootComponent().inject(this);
        initializePresenter();
        initUI();
    }

    public void initUI() {

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                finish();
            }
        });

        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
    }

    private void initializePresenter() {
        presenter = new SignupPresenter(userService);
        presenter.attachView(this);
    }

    public void toggleProgressDialog() {
        if(!progressDialog.isShowing()) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void goToLogin(String msg) {

        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra("token", msg);
        startActivity(intent);
        finish();
    }

    public void signup() {
        Log.d(TAG, "Signup");
        UserSignupRequest req = new UserSignupRequest();
        req.username = nameText.getText().toString();
        req.email = emailText.getText().toString();
        req.password = passwordText.getText().toString();
        req.repeatPassword = req.password;
        if(presenter != null) {
            presenter.doSignup(req);
        }
    }


//    public void onSignupSuccess(ApiBaseResponse res) {
//        progressDialog.dismiss();
//        signupButton.setEnabled(true);
//        setResult(RESULT_OK, null);
//
//        Toast.makeText(getBaseContext(), "Signup Success", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//        intent.putExtra("token", res.msg);
//        startActivity(intent);
//        finish();
//    }
//
//    public void onSignupFailed(ApiBaseResponse res) {
//        progressDialog.dismiss();
//        Toast.makeText(getBaseContext(), "Login failed: ", Toast.LENGTH_LONG).show();
//        signupButton.setEnabled(true);
//    }

    @Override
    public void showProgressing() {
        signupButton.setEnabled(false);
        progressDialog.show();
    }

    @Override
    public void showSignupSuccess(ApiBaseResponse res) {
        progressDialog.dismiss();
        signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        if(res == null) return;
        if(res.status == 0) {
            Toast.makeText(getBaseContext(), "Signup Success", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//            intent.putExtra("token", res.msg);
//            startActivity(intent);
//            finish();
            goToLogin(res.msg);
        } else {
            Toast.makeText(getBaseContext(), res.msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void showSignupFailure(Throwable t) {
        Toast.makeText(getBaseContext(), t.toString(), Toast.LENGTH_SHORT).show();
        signupButton.setEnabled(true);
        progressDialog.hide();
    }

    @Override
    public void showNameError(String msg) {
        if(!Util.stringIsNullOrEmpty(msg)) {
            nameText.setError(msg);
        }
    }

    @Override
    public void showEmailError(String msg) {
        if(!Util.stringIsNullOrEmpty(msg)) {
            emailText.setError(msg);
        }
    }

    @Override
    public void showPasswordError(String msg) {
        if(!Util.stringIsNullOrEmpty(msg)) {
            passwordText.setError(msg);
        }
    }

//    private static class HttpRequestTask extends AsyncTask<String, Void, ApiBaseResponse> {
//        private SignupActivity activity;
//        public HttpRequestTask(SignupActivity activity) {
//            this.activity = activity;
//        }
//
//        @Override
//        protected ApiBaseResponse doInBackground(String... params) {
//            try {
//                //final String url = "http://10.0.2.2:8080/v1/user/signup";
//                final String url = "http://192.168.0.191:9000/v1/user/signup";
//                RestTemplate restTemplate = new RestTemplate();
//                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
//                UserSignupRequest req = new UserSignupRequest();
//                req.nickname = params[0];
//                req.email = params[1];
//                req.password = params[2];
//                req.repeatPassword = req.password;
//                Log.d(TAG, req.nickname);
//                Log.d(TAG, req.email);
//                Log.d(TAG, req.password);
//                ApiBaseResponse res = restTemplate.postForObject(url, req, ApiBaseResponse.class);
//                // = restTemplate.getForObject(url, ApiBaseResponse.class);
//                return res;
//            } catch (Exception e) {
//                Log.e(TAG, e.getMessage(), e);
//            }
//
//            return null;
//        }
//        @Override
//        protected void onProgressUpdate(Void... progress) {
//        }
//
//        @Override
//        protected void onPostExecute(ApiBaseResponse res) {
//            this.activity.toggleProgressDialog(false);
//            Log.d(TAG, res.msg);
//            if(res.status == 0) {
//                this.activity.onSignupSuccess(res);
//            } else {
//                this.activity.onSignupFailed(res);
//            }
//        }
//    }
}
