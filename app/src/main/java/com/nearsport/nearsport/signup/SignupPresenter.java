package com.nearsport.nearsport.signup;

import android.app.FragmentManager;
import android.content.Intent;
import android.widget.Toast;

import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.presenter.BasePresenter;

import javax.inject.Inject;

/**
 * Created by sguo on 6/9/16.
 */
public class SignupPresenter implements BasePresenter<SignupView>, IUserService.DoSignupListener{

    private SignupView signupView;

    private IUserService userService;

    public SignupPresenter(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(SignupView view) {
        signupView = view;
    }

    public void doSignup(UserSignupRequest request) {
        if(null != userService && null != request) {
            if (validate(request)) {
                signupView.showProgressing();
                userService.doSignup(request, this);
            } else {
                signupView.showSignupFailure(new Exception("Fail to signup"));
            }
        }
    }

    public boolean validate(UserSignupRequest request) {
        boolean valid = true;
        String name = request.username;
        String email = request.email;
        String password = request.password;
        if (name.isEmpty() || name.length() < 3) {
            signupView.showNameError("at least 3 characters");
            valid = false;
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            signupView.showEmailError("enter a valid email address");
            valid = false;
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            signupView.showPasswordError("between 4 and 10 alphanumeric characters");
            valid = false;
        }
        return valid;
    }

    @Override
    public void onDoSignupSuccess(ApiBaseResponse res) {
        signupView.showSignupSuccess(res);
    }

    @Override
    public void onDoSignupFailure(Throwable t) {
        signupView.showSignupFailure(t);
    }
}
