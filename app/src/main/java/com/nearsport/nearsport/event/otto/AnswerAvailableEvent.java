package com.nearsport.nearsport.event.otto;

/**
 * Created by sguo on 5/23/16.
 */
public class AnswerAvailableEvent {
    public AnswerAvailableEvent(int code) {
        this.code = code;
    }

    public int code;

}
