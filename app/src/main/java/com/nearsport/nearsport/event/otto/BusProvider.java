package com.nearsport.nearsport.event.otto;

//import com.squareup.otto.Bus;
//import com.squareup.otto.ThreadEnforcer;

import com.google.android.gms.common.api.Scope;
import com.squareup.otto.Bus;


/**
 * Created by sguo on 5/23/16.
 */

public class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }
}
