package com.nearsport.nearsport;

import android.Manifest;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.permission.NSPermissionManager;
import com.nearsport.nearsport.permission.PermissionUtils;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.nearby.fragments.NSMapTabFragment;

import javax.inject.Inject;

public class MapsActivity extends AppCompatActivity {
    private static final String TAG = MapsActivity.class.getSimpleName();

    @Inject
    NSMapTabFragment mapFragment;

    @Inject
    NearSportSessionManager sessionManager;

    @Inject
    public NSPermissionManager permissionManager;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
//        InjectHelper.getRootComponent().inject(this);
        NearSportApplication.component().inject(this);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.maps_coordinator_layout);
        if(!sessionManager.isLoggedIn()) {
            FrameLayout layout = (FrameLayout) findViewById(R.id.index_container);
            mapFragment.setActivity(this);
            getSupportFragmentManager().beginTransaction().add(layout.getId(), mapFragment, "nearsport map fragment").commit();

        } else {
            Intent intent = new Intent(getApplicationContext(), MainTabActivity.class);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case NSPermissionManager.LOCATION_RESULT:
                if(PermissionUtils.isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    this.mapFragment.tryUpdateLocation();
                } else {
                    PermissionUtils.makePostRequestSnack(coordinatorLayout, "Location is required to access nearby games", new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
                break;
        }
    }
}
