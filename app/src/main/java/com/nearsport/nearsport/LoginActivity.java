package com.nearsport.nearsport;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.util.Constant;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements UserService.OnDoLoginListener {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @Inject
    public UserService userService;

    @Bind(R.id.input_email)
    EditText emailText;
    @Bind(R.id.input_password)
    EditText passwordText;
    @Bind(R.id.btn_login)
    Button loginButton;
    @Bind(R.id.link_signup)
    TextView signupLink;

    private ProgressDialog progressDialog;

    @Inject
    public NearSportSessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        InjectHelper.getRootComponent().inject(this);
        NearSportApplication.component().inject(this);
        ButterKnife.bind(this);
        initUI();
    }

    public void initUI() {
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the login activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
            }
        });

        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed("validation failure");
            return;
        }

        loginButton.setEnabled(false);

        UserLoginRequest req = new UserLoginRequest();
        req.email = emailText.getText().toString();
        req.password = passwordText.getText().toString();

        progressDialog.show();
        userService.doLogin(req, this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess(DoLoginResponse res) {
        progressDialog.dismiss();
        loginButton.setEnabled(true);
        this.goToMainLanding(res.authToken);
        finish();
    }

    public void onLoginFailed(String msg) {
        progressDialog.dismiss();
        Log.e(TAG, msg);
        Toast.makeText(getBaseContext(), "Login failed: " + msg, Toast.LENGTH_LONG).show();

        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    public void toggleProgressDialog() {
        if(!progressDialog.isShowing()) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void goToMainLanding(String msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), MainTabActivity.class);
        intent.putExtra(Constant.AUTH_TOKEN, msg);
        startActivity(intent);
    }

    @Override
    public void onDoLoginSuccess(DoLoginResponse response) {
        progressDialog.dismiss();
        loginButton.setEnabled(true);
        if(null != response && response.status == 0) {
            sessionManager.createLoginSession(response.authToken, emailText.getText().toString());
            this.goToMainLanding(response.authToken);
            finish();
        } else {
            Toast.makeText(getBaseContext(), response.msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDoLoginFailure(Throwable t) {
        Toast.makeText(getBaseContext(), t.toString(), Toast.LENGTH_SHORT).show();
    }
}
