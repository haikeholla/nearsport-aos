package com.nearsport.nearsport.session;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.nearsport.nearsport.MapsActivity;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.io.File;
import java.util.HashMap;

/**
 * Created by sguo on 5/9/16.
 */
public class NearSportSessionManager {
    protected SharedPreferences nearsportPref;

    // Shared pref mode
    private static final int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "NearSportPref";

    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    private Context context;

    private HashMap<String, String> user = new HashMap<String, String>();

    // Constructor
    public NearSportSessionManager(Context context){
        this.context = context;
        nearsportPref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = nearsportPref.edit();
    }

    public void createLoginSession(final PersonSkeleton personSkeleton) {
        if(null == personSkeleton) return;
        editor.putBoolean(Constant.IS_LOGIN, true);
        editor.putLong(Constant.PID, personSkeleton.pid);
        editor.putString(Constant.AUTH_TOKEN, personSkeleton.authToken);
        editor.putString(Constant.FNAME, personSkeleton.fname);
        editor.putString(Constant.LNAME, personSkeleton.lname);
        editor.putString(Constant.USERNAME, personSkeleton.username);
        editor.putString(Constant.GENDER, personSkeleton.gender);
        // Storing email in pref
        editor.putString(Constant.EMAIL, personSkeleton.email);
        editor.putString(Constant.ICON_URL_REMOTE, personSkeleton.iconUrl);
        // commit changes
        editor.commit();
    }

    public void createLoginSession(final String token, final String email) {
        // Storing login value as TRUE
        editor.putBoolean(Constant.IS_LOGIN, true);

        // Storing token in pref
        editor.putString(Constant.AUTH_TOKEN, token);

        // Storing email in pref
        editor.putString(Constant.EMAIL, email);

        // commit changes
        editor.commit();
    }

    public void updateLocation(double lng, double lat) {
        // Storing token in pref
        editor.putString(Constant.LONGITUDE, String.valueOf(lng));

        // Storing email in pref
        editor.putString(Constant.LATITUDE, String.valueOf(lat));

        // commit changes
        editor.commit();
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        // user token
        user.put(Constant.AUTH_TOKEN, nearsportPref.getString(Constant.AUTH_TOKEN, ""));
        // user email id
        user.put(Constant.EMAIL, nearsportPref.getString(Constant.EMAIL, ""));
        user.put(Constant.PID, String.valueOf(nearsportPref.getLong(Constant.PID, -1)));
        user.put(Constant.FNAME, nearsportPref.getString(Constant.FNAME, ""));
        user.put(Constant.LNAME, nearsportPref.getString(Constant.LNAME, ""));
        user.put(Constant.USERNAME, nearsportPref.getString(Constant.USERNAME, ""));
        user.put(Constant.PHONE, nearsportPref.getString(Constant.PHONE, ""));
        user.put(Constant.GENDER, nearsportPref.getString(Constant.GENDER, ""));
        user.put(Constant.ICON_URL_REMOTE, nearsportPref.getString(Constant.ICON_URL_REMOTE, ""));
        user.put(Constant.MOMENT_COVER_IMAGE_URL_REMOTE, nearsportPref.getString(Constant.MOMENT_COVER_IMAGE_URL_REMOTE, ""));

        // user last known location
        user.put(Constant.LONGITUDE, nearsportPref.getString(Constant.LONGITUDE, ""));
        user.put(Constant.LATITUDE, nearsportPref.getString(Constant.LATITUDE, ""));

        // return user
        return user;
    }

    public long getUserId() {
        return nearsportPref.getLong(Constant.PID, -1);
    }
    public String getUserStringInfo(final String field) {
        if(!user.containsKey(field)) {
            user.put(field, nearsportPref.getString(field, ""));
        }
        return user.get(field);
    }

    public void updateUserInfo(final String field, final String value) {
        if(!Util.stringIsNullOrEmpty(field) && null != value) {
            user.put(field, value);
            editor.putString(field, value);
            editor.commit();
        }
    }

    public String getUserAvatar() {
        String local = getUserStringInfo(Constant.ICON_URL);
        File localImage = new File(local);
        return (Util.stringIsNullOrEmpty(local) || localImage == null || !localImage.exists()) ? Constant.BASE_URL + getUserStringInfo(Constant.ICON_URL_REMOTE) : local;
    }

    public String getMomentCoverImage() {
        String local = getUserStringInfo(Constant.MOMENT_COVER_IMAGE_URL);
        File localImage = new File(local);
        String remote = getUserStringInfo(Constant.MOMENT_COVER_IMAGE_URL_REMOTE);
        return (Util.stringIsNullOrEmpty(local) || localImage == null || !localImage.exists()) ? (Util.stringIsNullOrEmpty(remote) ? "android.resource://com.nearsport.nearsport/drawable/moment_cover_image_placeholder" : Constant.BASE_URL + getUserStringInfo(Constant.MOMENT_COVER_IMAGE_URL_REMOTE)) : local;
    }

    public boolean getBoolean(String key, boolean defaultVal) {
        return nearsportPref.getBoolean(key, defaultVal);
    }

    public void putBoolean(String key, boolean val) {
        if(!Util.stringIsNullOrEmpty(key)) {
            editor.putBoolean(key, val).apply();
        }
    }

    public String getSessionToken() {
        return getUserStringInfo(Constant.AUTH_TOKEN);
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
//        editor.clear();
//        editor.commit();
        clearSession();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(context, MapsActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        context.startActivity(i);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }
    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return nearsportPref.getBoolean(Constant.IS_LOGIN, false);
    }
}
