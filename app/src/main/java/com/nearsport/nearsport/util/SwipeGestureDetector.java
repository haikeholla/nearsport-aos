package com.nearsport.nearsport.util;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by sguo on 8/5/16.
 */
public class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

    public static interface SwipeGestureListener {
        void onRightSwipe();
        void onLeftSwipe();
    }

    // Swipe properties, you can change it to make the swipe
    // longer or shorter and speed
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 200;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    protected SwipeGestureListener listener;

    public SwipeGestureDetector(SwipeGestureListener listener) {
        super();
        this.listener = listener;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2,
                           float velocityX, float velocityY) {
        try {
            float diffAbs = Math.abs(e1.getY() - e2.getY());
            float diff = e1.getX() - e2.getX();

            if (diffAbs > SWIPE_MAX_OFF_PATH)
                return false;

            // Left swipe
            if (diff > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                listener.onLeftSwipe();

                // Right swipe
            } else if (-diff > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                listener.onRightSwipe();
            }
        } catch (Exception e) {
            Log.e(listener.getClass().getSimpleName(), "Error on gestures");
        }
        return false;
    }
}
