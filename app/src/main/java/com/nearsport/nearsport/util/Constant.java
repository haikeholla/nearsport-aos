package com.nearsport.nearsport.util;

import com.nearsport.nearsport.R;

import java.text.SimpleDateFormat;

/**
 * Created by sguo on 4/30/16.
 */
public class Constant {

    public static final String BASE_URL = "http://192.168.0.190:9000/";
    public static final String AUTH_TOKEN = "X-AUTH-TOKEN";
    public static final String LNGLAT = "lnglat";
    public static final String GAME = "game";

    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";

    // Email address (make variable public to access from outside)
    public static final String PID = "pid";
    public static final String EMAIL = "email";
    public static final String FNAME = "fname";
    public static final String LNAME = "lname";
    public static final String GENDER = "gender";
    public static final String USERNAME = "username";
    public static final String PHONE = "phone";
    public static final String ICON_URL = "iconUrl";
    public static final String ICON_URL_REMOTE = "iconUrlRemote";
    public static final String MOMENT_COVER_IMAGE_URL = "momentCoverImageUrl";
    public static final String MOMENT_COVER_IMAGE_URL_REMOTE = "momentCoverImageUrlRemote";
    public static final String MOOD = "mood";
    public static final String REGION = "region";
    public static final String LONGITUDE = "lng";
    public static final String LATITUDE  = "lat";

    public static final String serverSideDateFormat = "yyyy-mm-dd hh:mm";
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat(serverSideDateFormat);

    public static final String USER_POST_CREATE_DATE_FORMAT = "yyyy-mm-dd";

    public static final String THREE_DOTS = "...";

    public static final int SOCKET_TIMEOUT = 45; //second

    public static final int POST_MAX_UPLOAD_PHOTO = 6;
    public static final int MOMENT_POSTS_PER_LOAD = 10;

    public static final int UI_NAV_TAB_NEARBY   = 0;
    public static final int UI_NAV_TAB_CHAT     = 1;
    public static final int UI_NAV_TAB_CONTACTS = 2;
    public static final int UI_NAV_TAB_DISCOVER = 3;
    public static final int UI_NAV_TAB_ME       = 4;

    public static final int UI_NAV_ME_PROF_INDEX = 0;
    public static final int UI_NAV_ME_MY_POST_INDEX = 1;
    public static final int UI_NAV_ME_FB_INDEX = 2;
    public static final int UI_NAV_ME_TWITTER_INDEX = 3;
    public static final int UI_NAV_ME_SETTING_INDEX = 4;

    public static final int UI_NAV_PROF_AVATAR_INDEX = 0;
    public static final int UI_NAV_PROF_USERNAME_INDEX = 1;
    public static final int UI_NAV_PROF_REALNAME_INDEX = 2;
    public static final int UI_NAV_PROF_GENDER_INDEX = 3;
    public static final int UI_NAV_PROF_REGION_INDEX = 4;
    public static final int UI_NAV_PROF_MOOD_INDEX = 5;

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;

    public static final int LIST_VIEW_DEFAULT_SEPERATION_WIDTH = 2;

    public static final int MPA_PAGE_BOOM_SUBBUTTON_REFRESH = 0;
    public static final int MPA_PAGE_BOOM_SUBBUTTON_ZOOMIN = 1;
    public static final int MPA_PAGE_BOOM_SUBBUTTON_ZOOMOUT = 2;
    public static final int MPA_PAGE_BOOM_SUBBUTTON_NEW = 3;
    public static final int CONTACT_PAGE_BOOM_SUBBUTTON_SEARCH = 0;
    public static final int CONTACT_PAGE_BOOM_SUBBUTTON_ADD = 1;

    public static final int USER_PASSWORD_MIN_LEN = 6;
    public static final int USER_PASSWORD_MAX_LEN = 16;

    public static final int INTENT_REQUEST_PHOTO_PICKER = 1;
    public static final int INTENT_REQUEST_GET_IMAGES = 13;
    public static final int INTENT_REQUEST_SELECT_CONTACTS = 14;
    public static final int INTENT_REQUEST_PHOTO_VIEWER_WITH_DELETE = 15;
    public static final int INTENT_REQUEST_ADD_MORE_POST_PHOTOS = 16;

    public static final int GET_USER_POST_LIMIT = 10;

    public static final String CONTACT_LIST = "contacts";

    public static final int COMPOSE_POST_MAX_CHOSEN_PHOTO_COUNT = 6;

    public static char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#".toCharArray();

    public static final String UPLOAD_FORM_FIELD_POST_PHOTO = "post_photo";
    public static final String UPLOAD_FORM_FIELD_POST = "post_content";

    public static final String INTENT_PARAM_PHOTO_LIST = "photoList";
    public static final String INTENT_PARAM_INDEX = "index";

    public static final String INTENT_PARAM_USER_POST = "userPost";

    public static final int[] MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_LOGINED = {
            R.drawable.ic_add_circle_outline_white_24dp
    };
    public static final int[] MAP_PAGE_BOOM_SUBBUTTON_COLOR_LOGINED = {
            R.color.lightblue,
            R.color.blue
    };
    public static final int[] MAP_PAGE_BOOM_SUBBUTTON_TITLE_LOGINED = {
            R.string.com_nearsport_ui_button_create
    };
    public static final int[] MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_NON_LOGINED = {
            R.drawable.ic_refresh_white_24dp,
            R.drawable.ic_zoom_in_white_24dp,
            R.drawable.ic_zoom_out_white_24dp
    };

    public static final int[] MAP_PAGE_BOOM_SUBBUTTON_COLOR_NON_LOGINED = {
            com.nightonke.boommenu.Util.getInstance().getPressedColor(R.color.lightblue),
            R.color.lightblue
    };

    public static final int[] MAP_PAGE_BOOM_SUBBUTTON_TITLE_NON_LOGINED = {
            R.string.com_nearsport_ui_button_refresh,
            R.string.com_nearsport_ui_button_zoomin,
            R.string.com_nearsport_ui_button_zoomout
    };

    public static final int[] CONTACT_PAGE_BOOM_SUBBUTTON_DRAWABLES = {
            R.drawable.ic_find_user,
            R.drawable.ic_add_user
    };

    public static final int[] CONTACT_PAGE_BOOM_SUBBUTTON_TITLE = {
            R.string.com_nearsport_ui_button_search,
            R.string.com_nearsport_ui_button_add
    };
}
