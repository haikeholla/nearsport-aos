package com.nearsport.nearsport.util;

import android.os.Build;

import com.activeandroid.query.Select;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nearsport.nearsport.model.PersonSkeleton;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sguo on 4/30/16.
 */
public class Util {

    public static final ObjectMapper objectMapper = new ObjectMapper();

    public static boolean stringIsNullOrEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static boolean isValidDateTime(String dateToValidate) {
        if(stringIsNullOrEmpty(dateToValidate)) return false;
        return isValidDateTime(dateToValidate, Constant.dateFormat);

    }

    public static boolean isValidDateTime(String dateToValidate, SimpleDateFormat dateFormat) {
        if(dateToValidate == null){
            return false;
        }
//        dateFormat.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            Date date = dateFormat.parse(dateToValidate);

        } catch (ParseException e) {

            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean isValidDateTime(String dateToValidate, String dateFormat) {

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return isValidDateTime(dateToValidate, new SimpleDateFormat(dateFormat));

    }

    public static String abbrieveate(String s, int limit) {
        if(stringIsNullOrEmpty(s)) return s;
        s = s.trim();
        if(s.length() <= limit) return s;
        StringBuilder sb = new StringBuilder();
        sb.append(s.substring(0, limit-3)).append(Constant.THREE_DOTS);
        return sb.toString();
    }

    public static String[] getNames(Class<? extends Enum<?>> e) {
        return Arrays.toString(e.getEnumConstants()).toLowerCase().replaceAll("^.|.$", "").split(", ");
    }

    public static boolean canMakeSmores() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    public static String getTodayDate(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
//        Calendar cal = Calendar.getInstance();
//        return dateFormat.format(cal.getTime());
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(Calendar.getInstance().getTime().getTime());
    }

    public static int getPostPolicyIndex(String policy) {
        switch (policy.toLowerCase()) {
            case "public": return 0;
            case "private": return 1;
            case "share list": return 2;
            case "do not share list": return 3;
            default: return 0;
        }
    }

    public static boolean isDBEmpty() {
        return new Select().from(PersonSkeleton.class).count() == 0;
    }

    public static boolean isAlphabet(char ch) {
        return (ch>='a'&&ch<='z') || (ch>='A'&&ch<='Z');
    }
}
