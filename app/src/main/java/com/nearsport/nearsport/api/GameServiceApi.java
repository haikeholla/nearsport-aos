package com.nearsport.nearsport.api;

import com.nearsport.nearsport.http.rest.request.CreateCasualGameRequest;
import com.nearsport.nearsport.http.rest.request.FetchNearbyGamesRequest;
import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiIdResponse;
import com.nearsport.nearsport.http.rest.response.CreateCasualGameResponse;
import com.nearsport.nearsport.http.rest.response.FetchNearbyGamesResponse;
import com.nearsport.nearsport.http.rest.response.GetSportTypesResponse;
import com.nearsport.nearsport.model.Game;
import com.nearsport.nearsport.model.GameSkeleton;
import com.nearsport.nearsport.model.Page;
import com.nearsport.nearsport.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by sguo on 4/30/16.
 */
public interface GameServiceApi {
    @GET("v1/games/nearby/{longitude}/{latitude}/{radius}/{type}/p={page}/s={size}")
    public Call<FetchNearbyGamesResponse> getNearbyGames(
            @Path("longitude") double                 longitude,
            @Path("latitude")  double                 latitude,
            @Path("radius")    double                 radius,
            @Path("type")      /*GameSkeleton.SportType*/ String type,
            @Path("page")      int                    page,
            @Path("size")      int                    size,
            @Header(Constant.AUTH_TOKEN) String token
    );

    @POST("v1/post/games/nearby")
    public Call<FetchNearbyGamesResponse> getNearbyGames(@Body FetchNearbyGamesRequest request);

    @POST("v1/games/casual/create")
    public Call<CreateCasualGameResponse> createCasualGame(@Body CreateCasualGameRequest request, @Header(Constant.AUTH_TOKEN) String token);

    @GET("v1/game/meta/sporttypes")
    public Call<GetSportTypesResponse> getSportTypes();
}
