package com.nearsport.nearsport.api;

import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sguo on 4/30/16.
 */
public interface UserServiceApi {

    @POST("v1/user/signup")
    Call<ApiBaseResponse> doSignup(@Body UserSignupRequest request);

    @POST("v1/user/login")
    Call<DoLoginResponse> doLogin(@Body UserLoginRequest request);
}
