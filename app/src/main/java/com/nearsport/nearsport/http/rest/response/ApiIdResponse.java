package com.nearsport.nearsport.http.rest.response;

/**
 * Created by sguo on 5/7/16.
 */
public class ApiIdResponse extends ApiBaseResponse {
    public long id;
}
