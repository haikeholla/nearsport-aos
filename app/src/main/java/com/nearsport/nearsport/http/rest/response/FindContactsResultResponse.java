package com.nearsport.nearsport.http.rest.response;

import java.util.List;

/**
 * Created by sguo on 6/21/16.
 */
public class FindContactsResultResponse {
    public List<FindContactsResultUser> foundUsers;
}
