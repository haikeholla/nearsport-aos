package com.nearsport.nearsport.http.rest.service;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nearsport.nearsport.http.rest.api.UserServiceApi;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.ComposePostRequest;
import com.nearsport.nearsport.http.rest.request.FindContactsResultRequest;
import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.response.ApiIdResponse;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.http.rest.response.FindContactsResultResponse;
import com.nearsport.nearsport.http.rest.response.FindContactsResultUser;
import com.nearsport.nearsport.http.rest.response.UploadUserAvatarResponse;
import com.nearsport.nearsport.http.rest.response.UploadUserPostResponse;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sguo on 5/7/16.
 */
public class UserService implements IUserService {

    private static final String TAG = "*UserService*";

    @Inject
    public UserServiceApi userServiceApi;

    public UserService() {
//        InjectHelper.getRootComponent().inject(this);
        NearSportApplication.component().inject(this);
    }

    public void doSignup(@NonNull final UserSignupRequest request, final DoSignupListener listener) {
        try {
            Call<ApiBaseResponse> signupRes = this.userServiceApi.doSignup(request);
            signupRes.enqueue(new Callback<ApiBaseResponse>() {
                @Override
                public void onResponse(Call<ApiBaseResponse> call, Response<ApiBaseResponse> response) {
                    ApiBaseResponse resMeet = response.body();
                    listener.onDoSignupSuccess(resMeet);
                }

                @Override
                public void onFailure(Call<ApiBaseResponse> call, Throwable t) {
                    Log.e(TAG, "something wrong");
                    listener.onDoSignupFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onDoSignupFailure(iae);
        }
    }

    public void doLogin(@NonNull final UserLoginRequest request, final DoLoginListener listener) {
        try {
            Call<DoLoginResponse> loginCall = this.userServiceApi.doLogin(request);
            loginCall.enqueue(new Callback<DoLoginResponse>() {
                @Override
                public void onResponse(Call<DoLoginResponse> call, Response<DoLoginResponse> response) {
                    DoLoginResponse res = response.body();
                    Log.e(TAG, response.raw().toString());
                    listener.onDoLoginSuccess(res);
                }

                @Override
                public void onFailure(Call<DoLoginResponse> call, Throwable t) {
                    listener.onDoLoginFailure(t);
                }
            });
        } catch(IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onDoLoginFailure(iae);
        }
    }

    public void uploadPictures(Uri imageUri, final IUserService.UploadPicturesListener listener) {
        try {
            File file = new File(imageUri.getPath());
            // create RequestBody instance from file
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

            // add another part within the multipart request
            String descriptionString = "hello, this is description speaking";
            RequestBody description =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), descriptionString);

            Call<ApiIdResponse> uploadPicturesCall = this.userServiceApi.uploadPictures(description, body);
            uploadPicturesCall.enqueue(new Callback<ApiIdResponse>() {
                @Override
                public void onResponse(Call<ApiIdResponse> call, Response<ApiIdResponse> response) {
                    Log.v("Upload", "success");
                    if(null != listener) {
                        listener.onUploadPicturesSuccess(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ApiIdResponse> call, Throwable t) {
                    Log.e("Upload error:", t.getMessage());
                    if(null != listener) {
                        listener.onUploadPicturesFailure(t);
                    }
                }
            });

        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onUploadPicturesFailure(iae);
        }
    }

    public void findUsers(@NonNull final FindContactsResultRequest request, final FindContactsListener listener) {
        FindContactsResultResponse response = new FindContactsResultResponse();
        response.foundUsers = new ArrayList<>(5);
        FindContactsResultUser user = new FindContactsResultUser();
        user.iconUrl = "";
        user.userId = 2;
        user.username = "username 1";
        response.foundUsers.add(user);
        user = new FindContactsResultUser();
        user.iconUrl = "";
        user.userId = 4;
        user.username = "username 3";
        response.foundUsers.add(user);
        user = new FindContactsResultUser();
        user.iconUrl = "";
        user.userId = 5;
        user.username = "username 5";
        response.foundUsers.add(user);
        listener.onFindContactsSuccess(response);
    }

    @Override
    public void uploadUserAvatar(@NonNull Uri imageUri, String token, final UploadUserAvatarListener listener) {
        try {
            File file = new File(imageUri.getPath());
            // create RequestBody instance from file
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);


            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

            // add another part within the multipart request
            String descriptionString = "This is my new avatar";
            RequestBody description =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), descriptionString);

            Call<UploadUserAvatarResponse> uploadUserAvatarCall = this.userServiceApi.uploadUserAvatar(body, token);
            uploadUserAvatarCall.enqueue(new Callback<UploadUserAvatarResponse>() {
                @Override
                public void onResponse(Call<UploadUserAvatarResponse> call, Response<UploadUserAvatarResponse> response) {
                    Log.v("Upload", "success");
                    if (null != listener) {
                        listener.onUploadUserAvatarSuccess(response.body());
                    }
                }

                @Override
                public void onFailure(Call<UploadUserAvatarResponse> call, Throwable t) {
                    Log.e("Upload error:", t.getMessage());
                    if (null != listener) {
                        listener.onUploadUserAvatarFailure(t);
                    }
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onUploadUserAvatarFailure(iae);
        }
    }

    @Override
    public void updateUserProfile(@NonNull final Map<String, String> map, String token, final UpdateUserProfListener listener) {
        try {
            if(null == map || map.isEmpty() || Util.stringIsNullOrEmpty(token)) return;
            Call<ApiBaseResponse> loginCall = this.userServiceApi.updateUserProfile(map, token);
            loginCall.enqueue(new Callback<ApiBaseResponse>() {
                @Override
                public void onResponse(Call<ApiBaseResponse> call, Response<ApiBaseResponse> response) {
                    ApiBaseResponse res = response.body();
                    Log.e(TAG, response.raw().toString());
                    listener.onUpdateUserProfSuccess(res);
                }

                @Override
                public void onFailure(Call<ApiBaseResponse> call, Throwable t) {
                    listener.onUpdateUserProfFailure(t);
                }
            });
        } catch(IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onUpdateUserProfFailure(iae);
        }
    }

    @Override
    public void getContacts(long id, @NonNull String token, final GetContactsListener listener) {
        try {
            if(Util.stringIsNullOrEmpty(token)) return;
            Call<List<PersonSkeleton>> loginCall = this.userServiceApi.getContacts(id, token);
            loginCall.enqueue(new Callback<List<PersonSkeleton>>() {
                @Override
                public void onResponse(Call<List<PersonSkeleton>> call, Response<List<PersonSkeleton>> response) {
                    List<PersonSkeleton> res = response.body();
                    Log.e(TAG, response.raw().toString());
                    listener.onGetContactsSuccess(res);
                }

                @Override
                public void onFailure(Call<List<PersonSkeleton>> call, Throwable t) {
                    listener.onGetContactsFailure(t);
                }
            });
        } catch(IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onGetContactsFailure(iae);
        }
    }

    @Override
    public void composePost(long userId, @NonNull ComposePostRequest request, @NonNull final ComposePostListener listener) throws JsonProcessingException {
        try {
            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            for (File photo : request.photos) {
                // create RequestBody instance from file
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), photo);

                // MultipartBody.Part is used to send also the actual file name
//            MultipartBody.Part body =  MultipartBody.Part.createFormData("post_photo", photo.getName(), requestFile);
                builder.addFormDataPart(Constant.UPLOAD_FORM_FIELD_POST_PHOTO, photo.getName(), requestFile);
            }
//            builder.addFormDataPart(Constant.UPLOAD_FORM_FIELD_POST, new Gson().toJson(request, ComposePostRequest.class));
            builder.addFormDataPart(Constant.UPLOAD_FORM_FIELD_POST, Util.objectMapper.writeValueAsString(request));
            RequestBody requestBody = builder.build();
            Call<UploadUserPostResponse> composePostCall = this.userServiceApi.uploadPost(userId, requestBody, request.token);
            composePostCall.enqueue(new Callback<UploadUserPostResponse>() {
                @Override
                public void onResponse(Call<UploadUserPostResponse> call, Response<UploadUserPostResponse> response) {
                    UploadUserPostResponse res = response.body();
                    Log.e(TAG, response.raw().toString());
                    listener.onComposePostSuccess(res);
                }

                @Override
                public void onFailure(Call<UploadUserPostResponse> call, Throwable t) {
                    listener.onComposePostFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onComposePostFailure(iae);
        }
    }

    @Override
    public void getUserPost(@NonNull long userId, int offset, int limit, @NonNull String token, final GetUserPostListener listener) {
        try {
            Call<List<UserPost>> getUserPostCall = this.userServiceApi.getUserPost(userId, offset, limit, token);
            getUserPostCall.enqueue(new Callback<List<UserPost>>() {
                @Override
                public void onResponse(Call<List<UserPost>> call, Response<List<UserPost>> response) {
                    List<UserPost> res = response.body();
                    Log.e(TAG, response.raw().toString());
                    listener.onGetUserPostSuccess(res);
                }

                @Override
                public void onFailure(Call<List<UserPost>> call, Throwable t) {
                    listener.onGetUserPostFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onGetUserPostFailure(iae);
        }
    }
}
