package com.nearsport.nearsport.http.rest.service;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nearsport.nearsport.http.rest.request.ComposePostRequest;
import com.nearsport.nearsport.http.rest.request.FindContactsResultRequest;
import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.response.ApiIdResponse;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.http.rest.response.FindContactsResultResponse;
import com.nearsport.nearsport.http.rest.response.UploadUserAvatarResponse;
import com.nearsport.nearsport.http.rest.response.UploadUserPostResponse;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.UserPost;

import java.util.List;
import java.util.Map;


/**
 * Created by sguo on 6/9/16.
 */
public interface IUserService {
    interface DoSignupListener {
        void onDoSignupSuccess(ApiBaseResponse response);
        void onDoSignupFailure(Throwable t);
    }

    interface DoLoginListener {
        void onDoLoginSuccess(DoLoginResponse response);
        void onDoLoginFailure(Throwable t);
    }

    interface UploadPicturesListener {
        void onUploadPicturesSuccess(ApiIdResponse response);
        void onUploadPicturesFailure(Throwable t);
    }

    interface UploadUserAvatarListener {
        void onUploadUserAvatarSuccess(UploadUserAvatarResponse response);
        void onUploadUserAvatarFailure(Throwable t);
    }

    interface FindContactsListener {
        void onFindContactsSuccess(FindContactsResultResponse response);
        void onFindContactsFailure(Throwable t);
    }

    interface UpdateUserProfListener {
        void onUpdateUserProfSuccess(ApiBaseResponse response);
        void onUpdateUserProfFailure(Throwable t);
    }

    interface GetContactsListener {
        void onGetContactsSuccess(List<PersonSkeleton> reponse);
        void onGetContactsFailure(Throwable t);
    }

    interface ComposePostListener {
        void onComposePostSuccess(UploadUserPostResponse response);
        void onComposePostFailure(Throwable t);
    }

    interface GetUserPostListener {
        void onGetUserPostSuccess(List<UserPost> response);
        void onGetUserPostFailure(Throwable t);
    }

    void doSignup(final UserSignupRequest request, final DoSignupListener listener);
    void doLogin(final UserLoginRequest request, final DoLoginListener listener);
    void findUsers(final FindContactsResultRequest request, final FindContactsListener listener);
    void uploadUserAvatar(final Uri imageUri, final String token, final UploadUserAvatarListener listener);
    void updateUserProfile(final Map<String, String> map, final String token, final UpdateUserProfListener listener);
    void getContacts(final long id, final String token, final GetContactsListener listener);
    void composePost(long userId, final ComposePostRequest request, ComposePostListener listener) throws JsonProcessingException;
    void getUserPost(@NonNull long userId, int offset, int limit, @NonNull final String token, final GetUserPostListener listener);
}
