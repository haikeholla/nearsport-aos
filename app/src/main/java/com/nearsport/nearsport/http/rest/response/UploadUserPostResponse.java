package com.nearsport.nearsport.http.rest.response;

import com.nearsport.nearsport.model.NSPhoto;

import java.util.List;
import java.util.Map;

/**
 * Created by sguo on 7/18/16.
 */
public class UploadUserPostResponse extends ApiIdResponse {
    public List<String> failedPhotos;
    public List<NSPhoto> uploadedPhotos;
}
