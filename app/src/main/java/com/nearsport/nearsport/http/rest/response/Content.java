
package com.nearsport.nearsport.http.rest.response;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "gid",
    "location",
    "name",
    "startTime",
    "endTime",
    "description",
    "sportType",
    "status",
    "accessLevel",
    "creator"
})
public class Content {

    @JsonProperty("gid")
    private Integer gid;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("name")
    private String name;
    @JsonProperty("startTime")
    private Timestamp startTime;
    @JsonProperty("endTime")
    private Timestamp endTime;
    @JsonProperty("description")
    private String description;
    @JsonProperty("sportType")
    private String sportType;
    @JsonProperty("status")
    private String status;
    @JsonProperty("accessLevel")
    private String accessLevel;
    @JsonProperty("creator")
    private Creator creator;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The gid
     */
    @JsonProperty("gid")
    public Integer getGid() {
        return gid;
    }

    /**
     * 
     * @param gid
     *     The gid
     */
    @JsonProperty("gid")
    public void setGid(Integer gid) {
        this.gid = gid;
    }

    /**
     * 
     * @return
     *     The location
     */
    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    /**
     * 
     * @param location
     *     The location
     */
    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The startTime
     */
    @JsonProperty("startTime")
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * 
     * @param startTime
     *     The startTime
     */
    @JsonProperty("startTime")
    public void setStartTime(Long startTime) {
        this.startTime = new Timestamp(startTime);
    }

    /**
     * 
     * @return
     *     The endTime
     */
    @JsonProperty("endTime")
    public Timestamp getEndTime() {
        return endTime;
    }

    /**
     * 
     * @param endTime
     *     The endTime
     */
    @JsonProperty("endTime")
    public void setEndTime(Long endTime) {
        this.endTime = new Timestamp(endTime);
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The sportType
     */
    @JsonProperty("sportType")
    public String getSportType() {
        return sportType;
    }

    /**
     * 
     * @param sportType
     *     The sportType
     */
    @JsonProperty("sportType")
    public void setSportType(String sportType) {
        this.sportType = sportType;
    }

    /**
     * 
     * @return
     *     The status
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The accessLevel
     */
    @JsonProperty("accessLevel")
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * 
     * @param accessLevel
     *     The accessLevel
     */
    @JsonProperty("accessLevel")
    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    /**
     * 
     * @return
     *     The creator
     */
    @JsonProperty("creator")
    public Creator getCreator() {
        return creator;
    }

    /**
     * 
     * @param creator
     *     The creator
     */
    @JsonProperty("creator")
    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
