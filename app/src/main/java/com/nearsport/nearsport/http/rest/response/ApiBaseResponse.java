package com.nearsport.nearsport.http.rest.response;

/**
 * Created by sguo on 4/26/16.
 */
public class ApiBaseResponse {
    public int status;
    public String msg;
}
