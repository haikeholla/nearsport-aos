package com.nearsport.nearsport.http.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nearsport.nearsport.model.tag.NSPOITag;

import java.io.File;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Created by sguo on 7/17/16.
 */
public class ComposePostRequest extends BaseRequest {

    @JsonProperty("content")
    public String content;
    @JsonIgnore
    public ArrayList<File> photos;
    @JsonProperty("poiTag")
    public NSPOITag poiTag;
    @JsonProperty("policyCode")
    public int policyCode;
    @JsonProperty("mentionedUsers")
    public ArrayList<Integer> mentionedUsers;
    @JsonProperty("createdTime")
    public Date createdTime;

    public ComposePostRequest() {
        photos = new ArrayList<>();
        mentionedUsers = new ArrayList<>();
    }
}
