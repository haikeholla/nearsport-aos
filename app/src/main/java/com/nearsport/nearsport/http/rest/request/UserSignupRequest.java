package com.nearsport.nearsport.http.rest.request;

/**
 * Created by sguo on 4/26/16.
 */
public class UserSignupRequest extends UserLoginRequest {
    public String repeatPassword;
    public String username;
}
