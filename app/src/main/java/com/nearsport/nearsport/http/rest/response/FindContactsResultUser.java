package com.nearsport.nearsport.http.rest.response;

/**
 * Created by sguo on 6/21/16.
 */
public class FindContactsResultUser {
    public String iconUrl;
    public String username;
    public long userId;
}
