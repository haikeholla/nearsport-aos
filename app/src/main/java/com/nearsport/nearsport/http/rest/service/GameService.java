package com.nearsport.nearsport.http.rest.service;

import android.util.Log;

import com.nearsport.nearsport.http.rest.api.GameServiceApi;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.CreateCasualGameRequest;
import com.nearsport.nearsport.http.rest.request.FetchGamesRequest;
import com.nearsport.nearsport.http.rest.response.CreateCasualGameResponse;
import com.nearsport.nearsport.http.rest.response.FetchGamesResponse;
import com.nearsport.nearsport.http.rest.response.GetSportTypesResponse;
import com.nearsport.nearsport.session.NearSportSessionManager;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sguo on 5/7/16.
 */
public class GameService implements IGameService {

    private static final String TAG = "*GameService*";

    @Inject
    GameServiceApi gameServiceApi;

    @Inject
    NearSportSessionManager sessionManager;

    public GameService() {
        NearSportApplication.component().inject(this);
    }

    public void createGame(CreateCasualGameRequest request, final CreateGameListener listener) {
        if(null == listener) return;
        try {
//            String token = sessionManager.getUserDetails().get(Constant.AUTH_TOKEN);
            Call<CreateCasualGameResponse> createGameCall = this.gameServiceApi.createCasualGame(request, request.token);
            createGameCall.enqueue(new Callback<CreateCasualGameResponse>() {
                @Override
                public void onResponse(Call<CreateCasualGameResponse> call, Response<CreateCasualGameResponse> response) {
                    Log.d(TAG, response.raw().toString());
                    listener.onCreateGameSuccess(response.body());
                }

                @Override
                public void onFailure(Call<CreateCasualGameResponse> call, Throwable t) {
                    listener.onCreateGameFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onCreateGameFailure(iae);
        }
    }

    public void getSportTypes(final GetSportTypesListener listener) {
        if(null == listener) return;
        try {
            Call<GetSportTypesResponse> getSportTypeCall = this.gameServiceApi.getSportTypes();
            getSportTypeCall.enqueue(new Callback<GetSportTypesResponse>() {

                @Override
                public void onResponse(Call<GetSportTypesResponse> call, Response<GetSportTypesResponse> response) {
                    GetSportTypesResponse res = response.body();
                    listener.onGetSportTypesSuccess(res.getTypes());
                }

                @Override
                public void onFailure(Call<GetSportTypesResponse> call, Throwable t) {
                    listener.onGetSportTypesFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onGetSportTypesFailure(iae);
        }
    }

    public void fetchGames(final FetchGamesRequest request, final FetchGamesListener listener) {
        if(null == listener) return;
        try {
            Call<FetchGamesResponse> fetchNearbyGamesCall =
                    this.gameServiceApi.getNearbyGames(
                            request.loc.longitude, request.loc.latitude, request.radius,
                            request.sportType.name(), request.page, request.size, request.token);
            fetchNearbyGamesCall.enqueue(new Callback<FetchGamesResponse>() {
                @Override
                public void onResponse(Call<FetchGamesResponse> call, Response<FetchGamesResponse> response) {
//                    Log.d(TAG, call.request().header(Constant.AUTH_TOKEN));
                    FetchGamesResponse games = response.body();
                    listener.onFetchGamesSuccess(games);
                }

                @Override
                public void onFailure(Call<FetchGamesResponse> call, Throwable t) {
                    listener.onFetchGamesFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
        }
    }
}
