package com.nearsport.nearsport.http.rest.api;

import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.util.Constant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by sguo on 8/5/16.
 */
public interface NSPhotoServiceApi {
    @GET("v1/photo/{id}")
    Call<NSPhoto> getPhotoWithCommentsAndLikes(@Path("id") long id, @Header(Constant.AUTH_TOKEN) String token);

    @POST("v1/photo/{id}/comments")
    Call<ApiBaseResponse> addComment(@Path("id") long id, String comment, @Header(Constant.AUTH_TOKEN) String token);

    @POST("v1/photo/{id}/likes")
    Call<ApiBaseResponse> addLike(@Path("id") long id, @Header(Constant.AUTH_TOKEN) String token);
}
