package com.nearsport.nearsport.http.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sguo on 6/15/16.
 */
public abstract class BaseRequest {
    @JsonIgnore
    public String token;
}
