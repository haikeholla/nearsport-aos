package com.nearsport.nearsport.http.rest.request;

import com.nearsport.nearsport.model.GameSkeleton;
import com.nearsport.nearsport.model.SimpleLatLng;

/**
 * Created by sguo on 5/1/16.
 */
public class FetchGamesRequest extends BaseRequest {
    public SimpleLatLng loc = new SimpleLatLng();
    public double   radius;
    public GameSkeleton.SportType sportType;
    public int      page;
    public int      size;

    public static class Builder {

    }
}
