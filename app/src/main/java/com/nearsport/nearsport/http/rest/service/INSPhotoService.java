package com.nearsport.nearsport.http.rest.service;

import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.model.NSPhoto;

/**
 * Created by sguo on 8/6/16.
 */
public interface INSPhotoService {
    interface GetPhotoCommentsAndLikesListener {
        void onGetPhotoCommentsAndLikesSuccess(NSPhoto photo);
        void onGetPhotoCommentsAndLikesFailure(Throwable t);
    }

    interface AddCommentListener {
        void onAddCommentSuccess(ApiBaseResponse response);
        void onAddCommentFailure(Throwable t);
    }

    interface AddLikeListener {
        void onAddLikeSuccess(ApiBaseResponse response);
        void onAddLikeFailure(Throwable t);
    }

    void getPhotoCommentsAndLikes(final long photoId, final String token, final GetPhotoCommentsAndLikesListener listener);

    void addComment(final long photoId, final String comment, final String token, final AddCommentListener listner);

    void addLike(final long photoId, final String token, final AddLikeListener listener);
}
