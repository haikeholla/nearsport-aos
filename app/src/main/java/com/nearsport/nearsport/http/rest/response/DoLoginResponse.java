package com.nearsport.nearsport.http.rest.response;

import com.nearsport.nearsport.model.PersonSkeleton;

/**
 * Created by sguo on 4/30/16.
 */
public class DoLoginResponse extends ApiBaseResponse {
    public PersonSkeleton personSkeleton;
}
