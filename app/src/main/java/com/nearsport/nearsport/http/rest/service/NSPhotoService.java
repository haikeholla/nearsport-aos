package com.nearsport.nearsport.http.rest.service;

import android.util.Log;

import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.api.NSPhotoServiceApi;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.model.NSPhoto;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sguo on 8/6/16.
 */
public class NSPhotoService implements INSPhotoService {

    private static final String TAG = NSPhotoService.class.getSimpleName();

    @Inject
    protected NSPhotoServiceApi photoServiceApi;

    public NSPhotoService() {
        NearSportApplication.component().inject(this);
    }

    @Override
    public void getPhotoCommentsAndLikes(long photoId, String token, final GetPhotoCommentsAndLikesListener listener) {
        try {
            Call<NSPhoto> requestCall = photoServiceApi.getPhotoWithCommentsAndLikes(photoId, token);
            requestCall.enqueue(new Callback<NSPhoto>() {
                @Override
                public void onResponse(Call<NSPhoto> call, Response<NSPhoto> response) {
                    NSPhoto photo = response.body();
                    listener.onGetPhotoCommentsAndLikesSuccess(photo);
                }

                @Override
                public void onFailure(Call<NSPhoto> call, Throwable t) {
                    Log.e(TAG, "something wrong");
                    listener.onGetPhotoCommentsAndLikesFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onGetPhotoCommentsAndLikesFailure(iae);
        }
    }

    @Override
    public void addComment(long photoId, final String comment, String token, final AddCommentListener listener) {
        try {
            Call<ApiBaseResponse> requestCall = photoServiceApi.addComment(photoId, comment, token);
            requestCall.enqueue(new Callback<ApiBaseResponse>() {
                @Override
                public void onResponse(Call<ApiBaseResponse> call, Response<ApiBaseResponse> response) {
                    ApiBaseResponse res = response.body();
                    listener.onAddCommentSuccess(res);
                }

                @Override
                public void onFailure(Call<ApiBaseResponse> call, Throwable t) {
                    Log.e(TAG, "something wrong");
                    listener.onAddCommentFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onAddCommentFailure(iae);
        }
    }

    @Override
    public void addLike(long photoId, final String token, final AddLikeListener listener) {
        try {
            Call<ApiBaseResponse> requestCall = photoServiceApi.addLike(photoId, token);
            requestCall.enqueue(new Callback<ApiBaseResponse>() {
                @Override
                public void onResponse(Call<ApiBaseResponse> call, Response<ApiBaseResponse> response) {
                    ApiBaseResponse res = response.body();
                    listener.onAddLikeSuccess(res);
                }

                @Override
                public void onFailure(Call<ApiBaseResponse> call, Throwable t) {
                    Log.e(TAG, "something wrong");
                    listener.onAddLikeFailure(t);
                }
            });
        } catch (IllegalArgumentException iae) {
            Log.e(TAG, "Server is busy.....");
            listener.onAddLikeFailure(iae);
        }
    }
}
