package com.nearsport.nearsport.http.rest.request;

/**
 * Created by sguo on 4/26/16.
 */
public class UserIdentity {
    public String email;

    public UserIdentity() {}
    public UserIdentity(String email) { this.email = email; }
}
