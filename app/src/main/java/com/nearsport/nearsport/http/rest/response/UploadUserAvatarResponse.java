package com.nearsport.nearsport.http.rest.response;

/**
 * Created by sguo on 7/8/16.
 */
public class UploadUserAvatarResponse extends ApiBaseResponse {
    public String thumbnailUrl;
    public String origianlUrl;
}
