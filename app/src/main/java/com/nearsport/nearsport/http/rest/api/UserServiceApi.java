package com.nearsport.nearsport.http.rest.api;

import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.request.UserSignupRequest;
import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.response.ApiIdResponse;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.http.rest.response.FindContactsResultResponse;
import com.nearsport.nearsport.http.rest.response.UploadUserAvatarResponse;
import com.nearsport.nearsport.http.rest.response.UploadUserPostResponse;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.util.Constant;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by sguo on 4/30/16.
 */
public interface UserServiceApi {

    @POST("v1/user/signup")
    Call<ApiBaseResponse> doSignup(@Body UserSignupRequest request);

    @POST("v1/user/login")
    Call<DoLoginResponse> doLogin(@Body UserLoginRequest request);

    @Multipart
    @POST
    Call<ApiIdResponse> uploadPictures(@Part("description") RequestBody description, @Part MultipartBody.Part file);

    @GET("v1/user/find/{username}")
    Call<FindContactsResultResponse> findUsers(@Path("username") String username);

    @Multipart
    @POST("v1/user/avatar")
    Call<UploadUserAvatarResponse> uploadUserAvatar(@Part MultipartBody.Part image, @Header(Constant.AUTH_TOKEN) String token);

    @POST("v1/user/profile")
    Call<ApiBaseResponse> updateUserProfile(@Body Map<String, String> map, @Header(Constant.AUTH_TOKEN) String token);

    @GET("v1/user/{id}/contacts")
    Call<List<PersonSkeleton>> getContacts(@Path("id") long userId, @Header(Constant.AUTH_TOKEN) String token);

    @POST("v1/user/{id}/posts")
    Call<UploadUserPostResponse> uploadPost(@Path("id") long userId, @Body RequestBody post, @Header(Constant.AUTH_TOKEN) String token);

    @GET("v1/user/{id}/posts?page={page}&limit={limit}")
    Call<List<UserPost>> getUserPost(@Path("id") long userId, @Path("page") int offset, @Path("limit") int limit, @Header(Constant.AUTH_TOKEN) String token);
}
