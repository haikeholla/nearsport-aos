package com.nearsport.nearsport.http.rest.service;

import com.nearsport.nearsport.http.rest.request.CreateCasualGameRequest;
import com.nearsport.nearsport.http.rest.request.FetchGamesRequest;
import com.nearsport.nearsport.http.rest.response.CreateCasualGameResponse;
import com.nearsport.nearsport.http.rest.response.FetchGamesResponse;

import java.util.List;

/**
 * Created by sguo on 6/15/16.
 */
public interface IGameService {
    public interface GetSportTypesListener {
        void onGetSportTypesSuccess(List<String> typeList);
        void onGetSportTypesFailure(Throwable t);
    }

    public interface CreateGameListener {
        void onCreateGameSuccess(CreateCasualGameResponse response);
        void onCreateGameFailure(Throwable t);
    }

    public interface FetchGamesListener {
        void onFetchGamesSuccess(FetchGamesResponse response);
        void onFetchGamesFailure(Throwable t);
    }

    void createGame(final CreateCasualGameRequest request, final CreateGameListener listener);
    void fetchGames(final FetchGamesRequest request, final FetchGamesListener listener);
}
