
package com.nearsport.nearsport.http.rest.response;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "pid",
    "email",
    "fname",
    "lname",
    "activated",
    "createDate",
    "lastAccess",
    "phoneNum",
    "nickname"
})
public class Creator {

    @JsonProperty("pid")
    private Long pid;
    @JsonProperty("email")
    private String email;
    @JsonProperty("fname")
    private Object fname;
    @JsonProperty("lname")
    private Object lname;
    @JsonProperty("activated")
    private Boolean activated;
    @JsonProperty("createDate")
    private Timestamp createDate;
    @JsonProperty("lastAccess")
    private Timestamp lastAccess;
    @JsonProperty("phoneNum")
    private Object phoneNum;
    @JsonProperty("nickname")
    private Object nickname;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The pid
     */
    @JsonProperty("pid")
    public Long getPid() {
        return pid;
    }

    /**
     * 
     * @param pid
     *     The pid
     */
    @JsonProperty("pid")
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The fname
     */
    @JsonProperty("fname")
    public Object getFname() {
        return fname;
    }

    /**
     * 
     * @param fname
     *     The fname
     */
    @JsonProperty("fname")
    public void setFname(Object fname) {
        this.fname = fname;
    }

    /**
     * 
     * @return
     *     The lname
     */
    @JsonProperty("lname")
    public Object getLname() {
        return lname;
    }

    /**
     * 
     * @param lname
     *     The lname
     */
    @JsonProperty("lname")
    public void setLname(Object lname) {
        this.lname = lname;
    }

    /**
     * 
     * @return
     *     The activated
     */
    @JsonProperty("activated")
    public Boolean getActivated() {
        return activated;
    }

    /**
     * 
     * @param activated
     *     The activated
     */
    @JsonProperty("activated")
    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    /**
     * 
     * @return
     *     The createDate
     */
    @JsonProperty("createDate")
    public Timestamp getCreateDate() {
        return createDate;
    }

    /**
     * 
     * @param createDate
     *     The createDate
     */
    @JsonProperty("createDate")
    public void setCreateDate(Long createDate) {
        this.createDate = new Timestamp(createDate);
    }

    /**
     * 
     * @return
     *     The lastAccess
     */
    @JsonProperty("lastAccess")
    public Timestamp getLastAccess() {
        return lastAccess;
    }

    /**
     * 
     * @param lastAccess
     *     The lastAccess
     */
    @JsonProperty("lastAccess")
    public void setLastAccess(Long lastAccess) {
        this.lastAccess = new Timestamp(lastAccess);
    }

    /**
     * 
     * @return
     *     The phoneNum
     */
    @JsonProperty("phoneNum")
    public Object getPhoneNum() {
        return phoneNum;
    }

    /**
     * 
     * @param phoneNum
     *     The phoneNum
     */
    @JsonProperty("phoneNum")
    public void setPhoneNum(Object phoneNum) {
        this.phoneNum = phoneNum;
    }

    /**
     * 
     * @return
     *     The nickname
     */
    @JsonProperty("nickname")
    public Object getNickname() {
        return nickname;
    }

    /**
     * 
     * @param nickname
     *     The nickname
     */
    @JsonProperty("nickname")
    public void setNickname(Object nickname) {
        this.nickname = nickname;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
