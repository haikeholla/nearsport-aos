package com.nearsport.nearsport.http.rest.api;

import com.nearsport.nearsport.http.rest.request.CreateCasualGameRequest;
import com.nearsport.nearsport.http.rest.request.FetchGamesRequest;
import com.nearsport.nearsport.http.rest.response.CreateCasualGameResponse;
import com.nearsport.nearsport.http.rest.response.FetchGamesResponse;
import com.nearsport.nearsport.http.rest.response.GetSportTypesResponse;
import com.nearsport.nearsport.util.Constant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by sguo on 4/30/16.
 */
public interface GameServiceApi {
    @GET("v1/games/nearby/{longitude}/{latitude}/{radius}/{type}/p={page}/s={size}")
    public Call<FetchGamesResponse> getNearbyGames(
            @Path("longitude") double                 longitude,
            @Path("latitude")  double                 latitude,
            @Path("radius")    double                 radius,
            @Path("type")      /*GameSkeleton.SportType*/ String type,
            @Path("page")      int                    page,
            @Path("size")      int                    size,
            @Header(Constant.AUTH_TOKEN) String token
    );

    @POST("v1/userPost/games/nearby")
    public Call<FetchGamesResponse> getNearbyGames(@Body FetchGamesRequest request);

    @POST("v1/games/casual/create")
    public Call<CreateCasualGameResponse> createCasualGame(@Body CreateCasualGameRequest request, @Header(Constant.AUTH_TOKEN) String token);

    @GET("v1/game/meta/sporttypes")
    public Call<GetSportTypesResponse> getSportTypes();
}
