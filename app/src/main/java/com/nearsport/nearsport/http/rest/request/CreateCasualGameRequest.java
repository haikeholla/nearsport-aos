package com.nearsport.nearsport.http.rest.request;

import com.nearsport.nearsport.http.rest.response.Location;
import com.nearsport.nearsport.model.ui.Address;

/**
 * Created by sguo on 5/7/16.
 */
public class CreateCasualGameRequest extends BaseRequest {
    public String name;
    public String startTime;
    public String endTime;
    public String type;
    public String sportType;
    public String accessLevel;
    public String description;
//    public Location location;
    public Address address;

    public CreateCasualGameRequest() {}

    public CreateCasualGameRequest(
            String token, String name, String startTime, String endTime, String type,
            String sportType, String accessLevel, String description, Address address) {
        this.token = token;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.type = type;
        this.sportType = sportType;
        this.accessLevel = accessLevel;
        this.description = description;
        this.address = address;
    }
}
