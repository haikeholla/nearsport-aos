package com.nearsport.nearsport.http.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.nearsport.nearsport.model.GameSkeleton;
import com.nearsport.nearsport.model.SimpleLatLng;

/**
 * Created by sguo on 5/8/16.
 */
public class CreateCasualGameResponse extends ApiIdResponse {
    public GameSkeleton game;

}
