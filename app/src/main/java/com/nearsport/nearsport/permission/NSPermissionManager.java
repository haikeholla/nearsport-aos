package com.nearsport.nearsport.permission;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.util.Util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import static android.Manifest.permission_group.LOCATION;
/**
 * Created by sguo on 6/11/16.
 */
public class NSPermissionManager {
    public static final int LOCATION_RESULT = 100;

    private Set<String> rejectedPermissions = new HashSet<>();
    private Set<String> grantedPermissions = new HashSet<>();

    @Inject
    public NearSportSessionManager sessionManager;

    public NSPermissionManager() {
        NearSportApplication.component().inject(this);
    }

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     * @param permission
     * @return
     */
    public boolean hasPermission(Activity activity, String permission) {
        if(grantedPermissions.contains(permission)) {
            return true;
        }
        if (Util.canMakeSmores()) {
            boolean ret = activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
            if(ret) {
                grantedPermissions.add(LOCATION);
            } else {
                markAsRejected(LOCATION);
            }
            return ret;
        }
        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     * @param permission
     * @return
     */
    public  boolean shouldWeAsk(String permission) {
        return(sessionManager.getBoolean(permission, true));
    }


    /**
     * we will save that we have already asked the user
     * @param permission
     */
    public void markAsAsked(String permission) {
        sessionManager.putBoolean(permission, false);
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     * @param permission
     */
    public void clearMarkAsAsked(String permission) {
        sessionManager.putBoolean(permission, true);
    }

    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     * @param wanted
     * @return
     */
    public ArrayList<String> findUnAskedPermissions(Activity activity, ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(activity, perm) && shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     * @param wanted
     * @return
     */
    public ArrayList<String> findRejectedPermissions(Activity activity, ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(activity, perm) && !shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    public void requestLocationPermission(Activity activity) {
        if (!hasPermission(activity, LOCATION.toString()) && shouldWeAsk(LOCATION.toString())) {
            ActivityCompat.requestPermissions(activity, new String[] { LOCATION }, LOCATION_RESULT);
            markAsAsked(LOCATION);
        }
    }

    public boolean hasLocationPermission(Activity activity) {
        return hasPermission(activity, LOCATION);
    }

    public void markAsRejected(String perm) {
        rejectedPermissions.add(perm);
    }
}
