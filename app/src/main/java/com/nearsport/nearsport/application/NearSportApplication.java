package com.nearsport.nearsport.application;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.dagger.ApplicationModule;
import com.nearsport.nearsport.dagger.DaggerRootComponent;
import com.nearsport.nearsport.dagger.RootComponent;
import com.nearsport.nearsport.dagger.ServiceModule;
import com.nearsport.nearsport.model.PersonSkeleton;

// adb pull /data/data/com.nearsport.nearsport/databases/nearsport.db ~/nearsport.db

/**
 * Created by sguo on 5/9/16.
 */
public class NearSportApplication extends Application {
    private static RootComponent rootComponent;
     //private static ApplicationComponent appComponent;
    @Override public void onCreate() {
        super.onCreate();
//        appComponent = DaggerApplicationComponent
//                .builder()
//                .applicationModule(new ApplicationModule(this))
//                .build();
        rootComponent = DaggerRootComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .serviceModule(new ServiceModule())
                .build();
        deleteDatabase("nearsport.db");
        ActiveAndroid.initialize(this);
        initDB();
    }
    public static RootComponent component() {
        return rootComponent;
    }
    //public static ApplicationComponent getAppComponent() { return appComponent; }

    protected void initDB() {
//        if(Util.isDBEmpty()) {
//        } else {
            new Delete().from(PersonSkeleton.class).execute();
            PersonSkeleton p1 = new PersonSkeleton();
            p1.username = "a bo";
            p1.iconUrl = String.valueOf(R.drawable.ic_ironman_48);
            p1.save();

            PersonSkeleton p2 = new PersonSkeleton();
            p2.username = "b bo";
            p2.iconUrl = String.valueOf(R.drawable.ic_spiderman_48);
            p2.save();

            PersonSkeleton p3 = new PersonSkeleton();
            p3.username = "c bo";
            p3.iconUrl = String.valueOf(R.drawable.ic_thor_48);
            p3.save();
//        }
    }
}
