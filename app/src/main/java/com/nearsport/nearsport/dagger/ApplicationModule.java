package com.nearsport.nearsport.dagger;

import android.app.Application;
import android.content.Context;

import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.session.NearSportSessionManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sguo on 5/9/16.
 */
@Module
public class ApplicationModule {

    private final NearSportApplication application;

    public ApplicationModule(NearSportApplication application) {
        this.application = application;
    }

    /**
     * Allow the application context to be injected but require that it be annotated with
     * {@link ForApplication @Annotation} to explicitly differentiate it from an activity context.
     */
    @Provides
    @Singleton
//    @ForApplication
    public NearSportApplication providesNearSportApplication() {
        return application;
    }

}
