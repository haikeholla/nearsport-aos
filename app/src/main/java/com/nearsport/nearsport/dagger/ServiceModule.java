package com.nearsport.nearsport.dagger;

import com.nearsport.nearsport.http.rest.api.GameServiceApi;
import com.nearsport.nearsport.http.rest.api.NSPhotoServiceApi;
import com.nearsport.nearsport.http.rest.api.UserServiceApi;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.service.GameService;
import com.nearsport.nearsport.http.rest.service.NSPhotoService;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.permission.NSPermissionManager;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.dialog.CreateGameDialogFragment;
import com.nearsport.nearsport.ui.nav.chat.fragments.NSChatTabFragment;
import com.nearsport.nearsport.ui.nav.contacts.fragments.NSContactsTabFragment;
import com.nearsport.nearsport.ui.nav.discover.fragments.NSDiscoverTabFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSSettingsFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSUserProfFragment;
import com.nearsport.nearsport.ui.nav.nearby.fragments.NSMapTabFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSMeTabFragment;
//import com.squareup.otto.Bus;
//import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by sguo on 4/30/16.
 */
@Module
public class ServiceModule {
    @Provides
    @Singleton
    public RetrofitService providesRetrofitService() {
        return new RetrofitService();
    }

    @Provides
    @Singleton
    public GameServiceApi providesGameServiceApi(RetrofitService retrofitService) {
        Retrofit retrofit = retrofitService.buildNearSportRetrofit();
        return retrofit.create(GameServiceApi.class);
    }

    @Provides
    @Singleton
    public UserServiceApi providesUserServiceApi(RetrofitService retrofitService) {
        Retrofit retrofit = retrofitService.buildNearSportRetrofit();
        return retrofit.create(UserServiceApi.class);
    }

    @Provides
    @Singleton
    public NSPhotoServiceApi providesNSPhotoServiceApi(RetrofitService retrofitService) {
        Retrofit retrofit = retrofitService.buildNearSportRetrofit();
        return retrofit.create(NSPhotoServiceApi.class);
    }

    @Provides
//    @Singleton
    public NSMapTabFragment providesNSMapTabFragment() {
        return new NSMapTabFragment();
    }

    @Provides
    @Singleton
    public NSMeTabFragment providesNSMeTabFragment() {
        return new NSMeTabFragment();
    }

    @Provides
    @Singleton
    public NSChatTabFragment providesNSChatTabFragment() {
        return new NSChatTabFragment();
    }

    @Provides
    @Singleton
    public NSDiscoverTabFragment providesNSDiscoverTabFragment() {
        return new NSDiscoverTabFragment();
    }

    @Provides
    @Singleton
    public NSContactsTabFragment providesNSContactsTabFragment() {
        return new NSContactsTabFragment();
    }

    @Provides
    @Singleton
    public CreateGameDialogFragment providesCreateGameDialogFragment() {
        return new CreateGameDialogFragment();
    }

    @Provides
    @Singleton
    public GameService providesGameService() {
        return new GameService();
    }

    @Provides
    @Singleton
    public UserService providesUserService() {
        return new UserService();
    }


    @Provides
    @Singleton
    public NSPhotoService providesNSPhotoService() {
        return new NSPhotoService();
    }

    @Provides
    @Singleton
    public NearSportSessionManager providesNearSportSessionManager(NearSportApplication application) {
        return new NearSportSessionManager(application);
    }
//
//    @Provides
//    public NearSportLocation providesNearSportLocation() {
//        return new NearSportLocation();
//    }
    @Provides
    public NSSettingsFragment providesNSSettingsFragment() {
        return new NSSettingsFragment();
    }

//    @Provides
//    public Bus providesBus() {
//        return new Bus(ThreadEnforcer.MAIN);
//    }

    @Provides
    @Singleton
    public NSUserProfFragment providesNSUserProfFragment() {
         return new NSUserProfFragment();
    }

    @Provides
    @Singleton
    public NSPermissionManager providesNSPermissionManager() {
        return new NSPermissionManager();
    }

}
