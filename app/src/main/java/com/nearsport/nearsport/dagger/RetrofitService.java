package com.nearsport.nearsport.dagger;

import com.nearsport.nearsport.util.Constant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by sguo on 4/30/16.
 */
public class RetrofitService {
// add multiple converters and the order seems to matter.
    public Retrofit buildNearSportRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient().newBuilder()
                        .readTimeout(Constant.SOCKET_TIMEOUT, TimeUnit.SECONDS)
                        .build())
                .build();
    }
}
