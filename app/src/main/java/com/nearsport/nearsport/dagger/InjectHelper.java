package com.nearsport.nearsport.dagger;

/**
 * Created by sguo on 4/30/16.
 */
public class InjectHelper {
    private static RootComponent sRootComponent;

    static {
        initModules();
    }

    private static void initModules() {
        sRootComponent = getRootComponentBuilder().build();
    }

    public static DaggerRootComponent.Builder getRootComponentBuilder() {
        return DaggerRootComponent.builder();
    }

    public static RootComponent getRootComponent() {
        if (sRootComponent == null) {
            initModules();
        }
        return sRootComponent;
    }
}
