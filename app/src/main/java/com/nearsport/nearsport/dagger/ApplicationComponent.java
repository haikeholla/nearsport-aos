package com.nearsport.nearsport.dagger;

/**
 * Created by sguo on 5/9/16.
 */

import android.app.Application;

import com.nearsport.nearsport.application.NearSportApplication;

import javax.inject.Singleton;

import dagger.Component;


@Component(modules = {
        ApplicationModule.class
})
@Singleton
public interface ApplicationComponent {
    NearSportApplication getNearSportApplication();
}
