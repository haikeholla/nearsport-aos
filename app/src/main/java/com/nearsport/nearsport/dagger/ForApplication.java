package com.nearsport.nearsport.dagger;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by sguo on 5/9/16.
 */
@Qualifier
@Retention(RUNTIME)
public @interface ForApplication {
}