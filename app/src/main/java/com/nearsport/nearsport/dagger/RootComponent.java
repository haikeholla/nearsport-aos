package com.nearsport.nearsport.dagger;

import com.nearsport.nearsport.http.rest.service.NSPhotoService;
import com.nearsport.nearsport.login.LoginActivity;
import com.nearsport.nearsport.MainTabActivity;
import com.nearsport.nearsport.MapsActivity;
import com.nearsport.nearsport.permission.NSPermissionManager;
import com.nearsport.nearsport.signup.SignupActivity;
import com.nearsport.nearsport.http.rest.service.GameService;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.ui.dialog.CreateGameDialogFragment;
import com.nearsport.nearsport.ui.nav.me.activities.NSUserPostLanderActivity;
import com.nearsport.nearsport.ui.nav.contacts.presenters.NSContactsTabPresenter;
import com.nearsport.nearsport.ui.nav.me.activities.NSMeSelectionLanderActivity;
import com.nearsport.nearsport.ui.nav.me.activities.NSUserProfLanderActivity;
import com.nearsport.nearsport.ui.nav.me.fragments.NSComposePostFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSMeTabFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSMyPostsFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSSettingsFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSUserProfFragment;
import com.nearsport.nearsport.ui.nav.nearby.fragments.NSMapTabFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by sguo on 4/30/16.
 */
//@ApplicationScoped
//@ActivityScope
//@Component(modules = {
//        ServiceModule.class
//}, dependencies = {ApplicationComponent.class})
@Singleton
@Component(modules = {ServiceModule.class, ApplicationModule.class})
public interface RootComponent {
    void inject(MapsActivity mainActivity);
    void inject(LoginActivity loginActivity);
    void inject(SignupActivity signupActivity);
    void inject(NSMapTabFragment nearSportMapFragment);
    void inject(MainTabActivity mainTabActivity);
    void inject(CreateGameDialogFragment createGameDialogFragment);
    void inject(GameService gameService);
    void inject(UserService userService);
    void inject(NSMeSelectionLanderActivity meSelectionLanderActivity);
    void inject(NSUserProfLanderActivity userProfLanderActivity);
    void inject(NSSettingsFragment settingsFragment);
    void inject(NSMeTabFragment meTabFragment);
    void inject(NSUserProfFragment userProfFragment);
//    void inject(NSUserProfRecyclerAdapter nsUserProfRecyclerAdapter);
    void inject(NSPermissionManager permissionManager);
    void inject(NSContactsTabPresenter presenter);
    void inject(NSMyPostsFragment myPostsFragment);
    void inject(NSComposePostFragment composePostFragment);
    void inject(NSPhotoService photoService);
    void inject(NSUserPostLanderActivity userPostLanderActivity);
}

