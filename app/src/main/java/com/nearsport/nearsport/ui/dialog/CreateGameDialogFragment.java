package com.nearsport.nearsport.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.dagger.InjectHelper;
import com.nearsport.nearsport.http.rest.request.CreateCasualGameRequest;
import com.nearsport.nearsport.http.rest.response.CreateCasualGameResponse;
import com.nearsport.nearsport.http.rest.service.GameService;
import com.nearsport.nearsport.model.GameSkeleton;
import com.nearsport.nearsport.model.ui.Address;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.picker.DateTimePicker;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
//import android.databinding.ViewDataBinding;

//https://maps.googleapis.com/maps/api/geocode/json?&address=3312%20wheeler%20dr%2C%20doravile%20ga%2030340

/**
 * Created by sguo on 5/7/16.
 */
public class CreateGameDialogFragment extends DialogFragment implements GameService.GetSportTypesListener, GameService.CreateGameListener {
    private static final String TAG = CreateGameDialogFragment.class.getSimpleName();

    private AlertDialog.Builder builder;

    private AlertDialog createGameDialog;

    private AlertDialog sportTypeDialog;

    private AlertDialog stateSelectionDialog;

    private LayoutInflater inflater;

    private DateTimePicker dateTimePicker;

    @Inject
    public GameService gameService;
    @Inject
    public NearSportSessionManager sessionManager;

    @Bind(R.id.input_create_game_name)
    EditText nameText;
    @Bind(R.id.input_create_game_start_time)
    EditText startTimeText;
    @Bind(R.id.input_create_game_end_time)
    EditText endTimeText;
    @Bind(R.id.input_create_game_sporttype)
    EditText sportTypeText;
    @Bind(R.id.input_create_game_street)
    EditText streetText;
    @Bind(R.id.input_create_game_city)
    EditText cityText;
    @Bind(R.id.input_create_game_state)
    EditText stateText;
    @Bind(R.id.input_create_game_zipcode)
    EditText zipcodeText;
    @Bind(R.id.input_create_game_description)
    EditText descriptionText;

    private boolean isStartTime = true;

    private Resources res;

    private String[] usStates;

    private static final String[] LOCAL_SPORT_TYPES = GameSkeleton.SportType.toStringArray();

//    private ViewDataBinding binding;

    private CreateCasualGameRequest createCasualGameRequest = new CreateCasualGameRequest();

    private ProgressDialog progressDialog;

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
////        if (container == null) return null;
//        View view = inflater.inflate(R.layout.fragment_dialog_create_game, container, false);
//        ButterKnife.bind(this, view);
//        InjectHelper.getRootComponent().inject(this);
//        initUI(view);
//        Bundle bundle = getArguments();
//        if(bundle != null) {
//            token = bundle.getString(Constant.AUTH_TOKEN);
//        }
//        return view;
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the layout inflater
        if(null == inflater) {
            inflater = getActivity().getLayoutInflater();
        }
        View view = inflater.inflate(R.layout.fragment_dialog_create_game, null);
        NearSportApplication.component().inject(this);
        ButterKnife.bind(this, view);
        initUI(view);
        return createGameDialog;
    }

    protected void clearCreateGameInputs() {
        nameText.setText("");
        startTimeText.setText("");
        endTimeText.setText("");
        sportTypeText.setText("");
        streetText.setText("");
        cityText.setText("");
        stateText.setText("");
        zipcodeText.setText("");
        descriptionText.setText("");
    }

    protected void initUI(View view) {
        if(res == null) {
            res = getResources();
            usStates = res.getStringArray(R.array.us_states);
        }
//        if (builder == null) {
            builder = new AlertDialog.Builder(getActivity());
//        }
        initDateTimePicker();
        initSportTypeDialogLocally();
        initCreateGameDialog(view);
        initStatesSelectionDialog();
        initInputTexts();
        initProgressDialog();
    }

    protected void initProgressDialog() {
        if(null == progressDialog) {
            progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creating...");
        }
    }

    protected void initStatesSelectionDialog() {
        if(null == stateSelectionDialog) {
            final AlertDialog.Builder tempBuilder = new AlertDialog.Builder(getActivity());
            stateSelectionDialog =
                    tempBuilder.setTitle("Select State")
                            .setSingleChoiceItems(usStates, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    stateText.setText(usStates[item]);
                    stateSelectionDialog.dismiss();
                }
            })
            .create();
        }
    }

    protected void initInputTexts() {
        startTimeText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    isStartTime = true;
                    dateTimePicker.showDialog();
                }
            }
        });

        endTimeText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    isStartTime = false;
                    dateTimePicker.showDialog();
                }
            }
        });

        stateText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    stateSelectionDialog.show();
                }
            }
        });

        sportTypeText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    sportTypeDialog.show();
                }
            }
        });
    }

    protected void initDateTimePicker() {
        if(null == dateTimePicker) {
            dateTimePicker = new DateTimePicker(getActivity(),
                    new DateTimePicker.ICustomDateTimeListener() {

                        @Override
                        public void onSet(Dialog dialog, Calendar calendarSelected,
                                          Date dateSelected, int year, String monthFullName,
                                          String monthShortName, int monthNumber, int date,
                                          String weekDayFullName, String weekDayShortName,
                                          int hour24, int hour12, int min, int sec,
                                          String AM_PM) {
                            String dateTimeStr = String.format(
                                    "%d-%s-%s %s:%s", year, DateTimePicker.pad(monthNumber+1), DateTimePicker.pad(date),
                                    DateTimePicker.pad(hour24), DateTimePicker.pad(min));
                            if(isStartTime) {
                                startTimeText.setText(dateTimeStr);
                            } else {
                                endTimeText.setText(dateTimeStr);
                            }
                        }

                        @Override
                        public void onCancel() {
                            dateTimePicker.dismissDialog();
                        }
                    });

            /**
             * Pass Directly current time format it will return AM and PM if you set
             * false
             */
            dateTimePicker.set24HourFormat(true);
            /**
             * Pass Directly current data and time to show when it pop up
             */
            dateTimePicker.setDate(Calendar.getInstance());
        }

    }

    protected void initSportTypeDialogLocally() {
        if(null == sportTypeDialog) {
            final AlertDialog.Builder tempBuilder = new AlertDialog.Builder(getActivity());
            tempBuilder.setSingleChoiceItems(LOCAL_SPORT_TYPES, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    sportTypeText.setText(LOCAL_SPORT_TYPES[item]);
                    sportTypeDialog.dismiss();
                }
            });
            sportTypeDialog = tempBuilder.create();
            Log.e(TAG, "init sport type locally :" + LOCAL_SPORT_TYPES.length);
        }
    }
    protected void initSportTypeDialog() {
//        if (sportTypeDialog == null) {
//            final AlertDialog.Builder tempBuilder = new AlertDialog.Builder(getActivity());
//            tempBuilder.setTitle("Select Sport Type");
//            try {
//                Call<GetSportTypesResponse> getSportTypeCall =
//                        this.gameServiceApi.getSportTypes();
//                getSportTypeCall.enqueue(new Callback<GetSportTypesResponse>() {
//
//                    @Override
//                    public void onResponse(Call<GetSportTypesResponse> call, Response<GetSportTypesResponse> response) {
//                        GetSportTypesResponse res = response.body();
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<GetSportTypesResponse> call, Throwable t) {
//                        initSportTypeDialogLocally();
//                    }
//                });
//            } catch (IllegalArgumentException iae) {
//                Log.e(TAG, "Server is busy.....");
//                initSportTypeDialogLocally();
//            }
            gameService.getSportTypes(this);
//        }
    }

    protected void initCreateGameDialog(View view) {
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
//        if (createGameDialog == null) {
            builder.setView(view)
                    // Add action buttons
                    .setPositiveButton(R.string.com_nearsport_ui_button_create, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // send create casual game ...
                            if(validateInputs()) {
                                //sendCreateGame
//                                CreateCasualGameRequest request = new CreateCasualGameRequest();
                                createCasualGameRequest.name = nameText.getText().toString();
                                createCasualGameRequest.startTime = startTimeText.getText().toString();
                                createCasualGameRequest.endTime = endTimeText.getText().toString();
                                createCasualGameRequest.sportType = sportTypeText.getText().toString();
                                createCasualGameRequest.description = descriptionText.getText().toString();
                                createCasualGameRequest.address = new Address(
                                        streetText.getText().toString(), cityText.getText().toString(),
                                        stateText.getText().toString(), zipcodeText.getText().toString());
                                createCasualGameRequest.token = sessionManager.getUserDetails().get(Constant.AUTH_TOKEN);
                                Log.e(TAG, createCasualGameRequest.token);
                                progressDialog.show();
                                gameService.createGame(createCasualGameRequest, CreateGameDialogFragment.this);
//                                Log.e(TAG, "send create game request " + createCasualGameRequest.startTime);
                            } else {
                                createGameDialog.show();
//                                Log.e(TAG, "createGameDialog.show()");
                            }
                        }
                    })
                    .setNegativeButton(R.string.com_nearsport_ui_button_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //CreateGameDialogFragment.this.getDialog().cancel();
                            //createGameDialog.dismiss();
                            clearCreateGameInputs();
                        }
                    })
                    .setTitle("Create Game");
            createGameDialog = builder.create();
//        } else {
//            Log.e(TAG, "initCreateGameDialog: createGameDialog <> null");
//        }
    }

    protected boolean validateInputs() {
        if(Util.stringIsNullOrEmpty(nameText.getText().toString())) {
            nameText.setError(res.getString(R.string.com_nearsport_msg_error_game_name_empty));
            return false;
        }
        if(Util.stringIsNullOrEmpty(sportTypeText.getText().toString())) {
            sportTypeText.setError(res.getString(R.string.com_nearsport_msg_error_sporttype_empty));
            return false;
        }
        if(!Util.isValidDateTime(startTimeText.getText().toString())) {
            startTimeText.setError(res.getString(R.string.com_nearsport_msg_error_invalid_starttime));
            return false;
        }
        if(!Util.isValidDateTime(endTimeText.getText().toString())) {
            endTimeText.setError(res.getString(R.string.com_nearsport_msg_error_invalid_endtime));
            return false;
        }
        return true;
    }

    @Override
    public void onGetSportTypesSuccess(List<String> typeList) {
        if(typeList == null || typeList.size() == 0) {
            initSportTypeDialogLocally();
            return;
        }
        if (sportTypeDialog == null) {
            final AlertDialog.Builder tempBuilder = new AlertDialog.Builder(getActivity());
            tempBuilder.setTitle("Select Sport Type");
            final String[] arr = new String[typeList.size()];
            typeList.toArray(arr);
            Log.e(TAG, "init sport type from server :" + arr.length);
            tempBuilder.setSingleChoiceItems(arr, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    sportTypeText.setText(arr[item]);
                    sportTypeDialog.dismiss();
                }
            });
            sportTypeDialog = tempBuilder.create();
        }


        sportTypeText.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    sportTypeDialog.show();
                }
            }
        });
    }

    @Override
    public void onGetSportTypesFailure(Throwable t) {
        initSportTypeDialogLocally();
    }

    @Override
    public void onCreateGameSuccess(CreateCasualGameResponse response) {
        progressDialog.dismiss();
//        createGameDialog.dismiss();
        if(response != null && response.status == 0) {
            clearCreateGameInputs();
            Intent i = new Intent();
            i.putExtra("game",response.game);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
        }
    }

    @Override
    public void onCreateGameFailure(Throwable t) {
        progressDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
//        builder = null;
        if(null != createGameDialog) {
            createGameDialog.setOnShowListener(null);
            createGameDialog = null;
        }
//        if(null != sportTypeDialog) {
//            sportTypeDialog.setOnShowListener(null);
//        }
//        if(null != stateSelectionDialog) {
//            stateSelectionDialog = null;
//        }
//        if(null != dateTimePicker) {
//            dateTimePicker = null;
//        }
//        progressDialog = null;
    }
}
