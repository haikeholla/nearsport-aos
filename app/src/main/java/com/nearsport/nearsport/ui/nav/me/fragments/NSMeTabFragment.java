package com.nearsport.nearsport.ui.nav.me.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.nearsport.nearsport.MainTabActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.model.ui.NSChatListItem;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.NSRecycleViewFragment;
import com.nearsport.nearsport.ui.nav.me.activities.NSMeSelectionLanderActivity;
import com.nearsport.nearsport.ui.nav.me.activities.NSUserProfLanderActivity;
import com.nearsport.nearsport.ui.nav.me.adapters.NSNavMeTabRecyclerAdapter;
import com.nearsport.nearsport.util.Constant;

import javax.inject.Inject;

/**
 * Created by sguo on 5/4/16.
 */
public class NSMeTabFragment extends NSRecycleViewFragment<NSNavMeTabRecyclerAdapter, NSSelectionListItem, MainTabActivity> {

    public static final String TAG = NSMeTabFragment.class.getSimpleName();
    @Inject
    NearSportSessionManager sessionManager;

    private NSSelectionListItem userProfItem;

//    @Bind(R.id.main_toolbar)
//    protected Toolbar mainToolbar;

    public NSMeTabFragment() {
        super();
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = super.onCreateView(inflater, container, savedInstanceState);
//        mActivity.setSupportActionBar(mainToolbar);
//        actionBar = mActivity.getSupportActionBar();
//
//        // Enable the Up button
//        actionBar.setTitle(R.string.com_nearsport_ui_toolbar_title);
//        actionBar.setDisplayShowHomeEnabled(true);
//        return view;
//    }

    @Override
    public void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        NearSportApplication.component().inject(this);
        if(itemList.isEmpty()) {
            userProfItem = createUserProfItem();
            createMyPosts();
            createFacebookItem();
            createTwitterItem();
            createSettings();
        }
        mAdapter = new NSNavMeTabRecyclerAdapter(Glide.with(this), itemList);
        setSeparationDecoration(SeparatorDecoration.with(mActivity).colorFromResources(R.color.iron).width(Constant.LIST_VIEW_DEFAULT_SEPERATION_WIDTH).build());
    }

    protected NSSelectionListItem createUserProfItem() {
        NSChatListItem item = new NSChatListItem();
        item.lastMsger = sessionManager.getUserStringInfo(Constant.FNAME) + " " + sessionManager.getUserStringInfo(Constant.LNAME);
        item.lastMsg = "Username: " + sessionManager.getUserStringInfo(Constant.USERNAME);
        item.leftIconUrl = sessionManager.getUserAvatar();
        item.type = NSBaseListItem.ItemType.USER_PROF;
        item.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, NSUserProfLanderActivity.class);
                intent.putExtra("fragmentId", NSUserProfLanderActivity.PROFILE);
                mActivity.startActivity(intent);
            }
        });
        itemList.add(item);
        return item;
    }

    protected NSSelectionListItem createSettings() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Settings";
        item.setLeftIcon(R.drawable.ic_settings);
        item.setType(NSBaseListItem.ItemType.SETTINGS);
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, NSMeSelectionLanderActivity.class);
                intent.putExtra("fragmentId", NSMeSelectionLanderActivity.SETTINGS);
                mActivity.startActivity(intent);
            }
        };
        itemList.add(item);
        return item;
    }

    protected NSSelectionListItem createMyPosts() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.title = "My UserPost";
        item.leftIcon = R.drawable.ic_gallery;
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, NSMeSelectionLanderActivity.class);
                intent.putExtra("fragmentId", NSMeSelectionLanderActivity.MY_POSTS);
                mActivity.startActivity(intent);
            }
        };
        itemList.add(item);
        return item;
    }

    protected NSSelectionListItem createFacebookItem() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Like us on Facebook";
        item.leftIcon = R.drawable.ic_facebook;
        itemList.add(item);
        return item;
    }

    protected NSSelectionListItem createTwitterItem() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Follow us on Twitter";
        item.leftIcon = R.drawable.ic_twitter;
        itemList.add(item);
        return item;
    }

    @Override
    public void onStart() {
        super.onStart();
//        userProfItem.leftIconUrl = sessionManager.getUserInfo(Constant.ICON_URL);
//        mAdapter.updateUserAvatar(userProfItem.leftIconUrl);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        userProfItem.leftIconUrl = sessionManager.getUserAvatar();
        mAdapter.notifyItemChanged(Constant.UI_NAV_ME_PROF_INDEX);
    }
}
