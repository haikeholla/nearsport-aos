package com.nearsport.nearsport.ui.nav.chat.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.nearsport.nearsport.MainTabActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSChatListItem;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.ui.nav.NSRecycleViewFragment;
import com.nearsport.nearsport.ui.nav.chat.activities.NSChatLanderActivity;
import com.nearsport.nearsport.ui.nav.chat.adapters.NSNavChatTabRecyclerAdapter;
import com.nearsport.nearsport.util.Constant;

/**
 * Created by sguo on 5/6/16.
 */
public class NSChatTabFragment extends NSRecycleViewFragment<NSNavChatTabRecyclerAdapter, NSChatListItem, MainTabActivity> {
    private static final String TAG = NSChatTabFragment.class.getSimpleName();

//    @Bind(R.id.main_toolbar)
//    protected Toolbar mainToolbar;
    public NSChatTabFragment() {
        super();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        if(itemList.isEmpty()) {
            NSChatListItem item1 = new NSChatListItem();
            item1.setType(NSBaseListItem.ItemType.CHAT);
            item1.lastMsger = "jigsaw";
            item1.setTitle("Song");
            item1.lastMsg = "who are you";
            item1.setLeftIcon(R.drawable.ic_ironman_48);
            item1.chatType = NSChatListItem.ChatType.INDIVIDUAL;
            item1.onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), NSChatLanderActivity.class);
                    mActivity.startActivity(intent);
                }
            };
            itemList.add(item1);


            item1 = new NSChatListItem();
            item1.setType(NSBaseListItem.ItemType.CHAT);
            item1.lastMsger = "fiona";
            item1.setTitle("Dan");
            item1.lastMsg = "long time no see";
            item1.setLeftIcon(R.drawable.ic_flash_48);
            item1.chatType = NSChatListItem.ChatType.INDIVIDUAL;
            itemList.add(item1);

            item1 = new NSChatListItem();
            item1.setType(NSBaseListItem.ItemType.CHAT);
            item1.lastMsger = "Spiderman";
            item1.setTitle("Michael");
            item1.lastMsg = "I am the Spiderman!";
            item1.setLeftIcon(R.drawable.ic_spiderman_48);
            item1.chatType = NSChatListItem.ChatType.INDIVIDUAL;
            itemList.add(item1);
        }

        mAdapter = new NSNavChatTabRecyclerAdapter(Glide.with(this), itemList);
        setSeparationDecoration(SeparatorDecoration.with(mActivity).colorFromResources(R.color.iron).width(Constant.LIST_VIEW_DEFAULT_SEPERATION_WIDTH).build());
    }

}
