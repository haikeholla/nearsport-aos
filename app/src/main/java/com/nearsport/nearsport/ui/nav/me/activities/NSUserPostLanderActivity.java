package com.nearsport.nearsport.ui.nav.me.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.service.NSPhotoService;
import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.photoviewer.NSBaseSwipePhotoViewer;
import com.nearsport.nearsport.ui.nav.me.presenters.NSUserPostPresenter;
import com.nearsport.nearsport.ui.nav.me.presenters.NSUserPostView;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 8/5/16.
 */
public class NSUserPostLanderActivity extends NSBaseSwipePhotoViewer implements NSUserPostView {
    private static final String TAG = NSUserPostLanderActivity.class.getSimpleName();

    @Bind(R.id.user_post_image)
    ImageView displayedPhoto;

    @Bind(R.id.user_post_content)
    TextView content;

    @Bind(R.id.user_post_like_button)
    ImageView iconLike;

    @Bind(R.id.user_post_comment_button)
    ImageView iconComment;

    @Bind(R.id.user_post_my_likes)
    TextView myLikes;

    @Bind(R.id.user_post_my_comments)
    TextView myComments;

    @Bind(R.id.main_toolbar)
    Toolbar mainToolbar;

    protected ActionBar actionBar;

    protected UserPost userPost;

    protected RequestManager glide;

    @Inject
    NSPhotoService photoServiceService;

    @Inject
    NearSportSessionManager sessionManager;

    protected NSUserPostPresenter presenter;

    protected NSPhoto currentPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_post_lander);
        ButterKnife.bind(this);
        NearSportApplication.component().inject(this);

        setSupportActionBar(mainToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        presenter = new NSUserPostPresenter(photoServiceService, sessionManager);
        presenter.attachView(this);

        Intent intent = this.getIntent();
        long usePostId = intent.getLongExtra(Constant.INTENT_PARAM_USER_POST, -1);
        userPost = new Select().from(UserPost.class).where("id = ?", usePostId).executeSingle();
//        userPost = intent.getParcelableExtra(Constant.INTENT_PARAM_USER_POST);
        currentIndex = intent.getIntExtra(Constant.INTENT_PARAM_INDEX, 0);
        glide = Glide.with(this);
        userPost.photos.addAll(userPost.photos());
        count = userPost.photos.size();
        currentPhoto = userPost.photos.get(currentIndex);
        initView();
    }

    protected void initView() {
        presenter.fetchPhotoCommentsAndLikes(currentPhoto.id);
        content.setText(userPost.content);
        if(!Util.stringIsNullOrEmpty(currentPhoto.localPath)) {
            glide.load(currentPhoto.localPath)
                    .dontAnimate()
                    .centerCrop()
                    .error(R.drawable.user_icon_placeholder)
                    .into(displayedPhoto);
        } else if(!Util.stringIsNullOrEmpty(currentPhoto.url)) {
            glide.load(currentPhoto.url)
                    .dontAnimate()
                    .centerCrop()
                    .error(R.drawable.user_icon_placeholder)
                    .into(displayedPhoto);
        }
        actionBar.setTitle(userPost.createdTime.toString());
        iconLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.likePhoto(currentPhoto.id);
            }
        });
        iconComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(NSUserPostLanderActivity.this)
                        .title(R.string.com_nearsport_user_post_comment_title)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input(NSUserPostLanderActivity.this.getResources().getString(R.string.com_nearsport_user_post_comment_hint), null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                            }
                        })
                        .positiveText(R.string.com_nearsport_yes)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO
                                presenter.commentPhoto(currentPhoto.id, dialog.getInputEditText().toString());
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        myLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        myComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void updateView() {
        currentPhoto = userPost.photos.get(currentIndex);
        presenter.fetchPhotoCommentsAndLikes(currentPhoto.id);
    }

    @Override
    public void showCommentsAndLikes(NSPhoto photo) {
        if(photo.comments != null && photo.comments.size() > 0) {
            myComments.setText(photo.comments.size());
        }
        if(photo.likes != null && photo.likes.size() > 0) {
            myLikes.setText(photo.likes.size());
        }
    }

    @Override
    public void updateCommentsCount(int c) {
        int count = 0;
        try {
            count = Integer.parseInt(myComments.getText().toString());
            myComments.setText(String.valueOf(count+c));
        } catch (NumberFormatException nfe) {
            myComments.setText("1");
        }
    }

    @Override
    public void updateLikesCount(int c) {
        int count = 0;
        try {
            count = Integer.parseInt(myLikes.getText().toString());
            myLikes.setText(String.valueOf(count+c));
        } catch (NumberFormatException nfe) {
            myLikes.setText("1");
        }
    }
}
