package com.nearsport.nearsport.ui.nav.discover.activities;

import android.os.Bundle;

import net.datafans.android.timeline.controller.UserTimelineViewController;
import net.datafans.android.timeline.item.user.BaseUserLineItem;

/**
 * Created by sguo on 7/6/16.
 */
public class NSUserMomentFeedActivity extends UserTimelineViewController {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        addItems();
        super.onCreate(savedInstanceState);


        setHeader();
    }

    @Override
    public void onClickItem(BaseUserLineItem item) {

    }

    @Override
    protected String getNavTitle() {
        return "Allen的相册";
    }
    private void setHeader() {
        String coverUrl = String.format("http://file-cdn.datafans.net/temp/12.jpg_%dx%d.jpeg", coverWidth, coverHeight);
        setCover(coverUrl);


        String userAvatarUrl = String.format("http://file-cdn.datafans.net/avatar/1.jpeg_%dx%d.jpeg", userAvatarSize, userAvatarSize);
        setUserAvatar(userAvatarUrl);


        setUserNick("Allen");

        setUserSign("梦想还是要有的 万一实现了呢");

        setUserId(123456);
    }

    private void addItems() {
    }

    @Override
    public void onRefresh() {
        super.onRefresh();

        onEnd();
    }


    @Override
    public void onLoadMore() {
        super.onLoadMore();

        onEnd();
    }

    }
