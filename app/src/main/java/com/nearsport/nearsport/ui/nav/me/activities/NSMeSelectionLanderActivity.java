package com.nearsport.nearsport.ui.nav.me.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.gun0912.tedpicker.Config;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.nav.me.fragments.NSComposePostFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSMyPostsFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSSettingsFragment;
import com.nearsport.nearsport.util.Constant;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 5/15/16.
 */
public class NSMeSelectionLanderActivity extends AppCompatActivity {
    private static final String TAG = NSMeSelectionLanderActivity.class.getSimpleName();
    public static final int SETTINGS = 0;
    public static final int MY_POSTS= 1;
    public static final int CREATE_POST = 2;

    private NSSettingsFragment settingsFragment;
    private NSMyPostsFragment myPostsFragment;
    private NSComposePostFragment composePostFragment;
    private Fragment currentFragment;
    private int currentFragmentId;

    @Bind(R.id.main_toolbar)
    Toolbar mainToolbar;

    protected ActionBar abar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me_selection_lander);
        NearSportApplication.component().inject(this);
        ButterKnife.bind(this);
        setSupportActionBar(mainToolbar);
        abar = getSupportActionBar();
        abar.setHomeButtonEnabled(true);
        abar.setDisplayHomeAsUpEnabled(true);

        if (findViewById(R.id.me_selection_lander_fragment_container) != null) {
            Intent intent = getIntent();
            if(null != intent && intent.hasExtra("fragmentId")) {
                currentFragmentId = intent.getIntExtra("fragmentId", -1);
                switch (currentFragmentId) {
                    case SETTINGS: {
                        abar.setTitle("Settings");
                        settingsFragment = new NSSettingsFragment();
                        settingsFragment.setActivity(this);
                        settingsFragment.setArguments(getIntent().getExtras());
                        currentFragment = settingsFragment;
                        break;
                    }
                    case MY_POSTS: {
                        abar.setTitle("My Posts");
                        myPostsFragment = new NSMyPostsFragment();
                        myPostsFragment.setActivity(this);
                        myPostsFragment.setArguments(getIntent().getExtras());
                        currentFragment = myPostsFragment;
                        break;
                    }
                    case CREATE_POST: {
                        abar.setTitle("Create Post");

                    }
                }

                // Add the fragment to the 'fragment_container' FrameLayout
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.me_selection_lander_fragment_container, currentFragment).commit();
            }
        }
    }

    public void setSettingsFragment() {
        abar.setTitle("Settings");
        settingsFragment.setActivity(this);
        settingsFragment.setArguments(getIntent().getExtras());

        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.me_selection_lander_fragment_container, settingsFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                removeCurrentFragment();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resuletCode, Intent intent) {
        super.onActivityResult(requestCode, resuletCode, intent);

        if (requestCode == CREATE_POST && resuletCode == Activity.RESULT_OK) {
            final Config config = new Config();
            config.setSelectionLimit(Constant.POST_MAX_UPLOAD_PHOTO);
            ImagePickerActivity.setConfig(config);
            ArrayList<Uri> image_uris = intent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(ImagePickerActivity.EXTRA_IMAGE_URIS, image_uris);
            abar.setTitle("Create Post");
            composePostFragment = new NSComposePostFragment();
            composePostFragment.setActivity(this);
            composePostFragment.setArguments(bundle);
            currentFragment = composePostFragment;
            currentFragmentId = CREATE_POST;
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.me_selection_lander_fragment_container, currentFragment).commit();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.me_selection_lander_fragment_container, currentFragment);
            transaction.addToBackStack("mypost_createpost");
            transaction.commit();
        } else if(requestCode == Constant.INTENT_REQUEST_SELECT_CONTACTS && resuletCode == Activity.RESULT_OK) {
            if(intent.hasExtra(Constant.CONTACT_LIST) && currentFragmentId == CREATE_POST) {
                ArrayList<NSSelectionListItem> contacts = intent.getParcelableArrayListExtra(Constant.CONTACT_LIST);
                for(NSSelectionListItem contact : contacts) {
                    contact.type = NSBaseListItem.ItemType.MENTIONED_CONTACT;
                    contact.leftIconUrl = "https://otakuoverdrive.files.wordpress.com/2014/12/naruto-uzumaki.jpg";
                    composePostFragment.addMentionedContact(contact);
                }
            }
        } else if(requestCode == Constant.INTENT_REQUEST_PHOTO_VIEWER_WITH_DELETE && resuletCode == Activity.RESULT_OK) {
            if(intent.hasExtra(Constant.INTENT_PARAM_PHOTO_LIST) && currentFragmentId == CREATE_POST) {
                ArrayList<Integer> removedIndex = intent.getIntegerArrayListExtra(Constant.INTENT_PARAM_PHOTO_LIST);
                composePostFragment.removeSelectedPhotos(removedIndex);
            }
        } else if(requestCode == Constant.INTENT_REQUEST_ADD_MORE_POST_PHOTOS && resuletCode == Activity.RESULT_OK) {
            if(currentFragmentId == CREATE_POST) {
                ArrayList<Uri> image_uris = intent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
                composePostFragment.addPhotos(image_uris);
            }
        }
    }

    public void removeCurrentFragment() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0){
//                if(currentFragmentId == CREATE_POST) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
}
