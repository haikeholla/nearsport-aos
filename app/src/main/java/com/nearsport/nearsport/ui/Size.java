package com.nearsport.nearsport.ui;

/**
 * Created by sguo on 5/5/16.
 */
public class Size {
    public int w = -1;
    public int h = -1;

    public Size(int w, int h) {
        this.w = w;
        this.h = h;
    }
}
