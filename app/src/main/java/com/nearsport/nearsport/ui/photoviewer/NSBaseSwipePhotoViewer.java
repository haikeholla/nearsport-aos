package com.nearsport.nearsport.ui.photoviewer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;

import com.nearsport.nearsport.util.SwipeGestureDetector;

/**
 * Created by sguo on 8/5/16.
 */
public abstract class NSBaseSwipePhotoViewer extends AppCompatActivity implements SwipeGestureDetector.SwipeGestureListener {

    private static final String TAG = NSBaseSwipePhotoViewer.class.getSimpleName();

    protected int currentIndex = 0;

    protected int count = 0;

    protected GestureDetector gestureDetector;

    protected void moveToNext() {
        currentIndex++;
        currentIndex %= count;
    }

    protected void moveToPrev() {
        currentIndex--;
        currentIndex += count;
        currentIndex %= count;
    }

    @Override
    public void onRightSwipe() {
        Log.d(TAG, "Left to Right swipe performed");
        moveToPrev();
        updateView();
    }

    @Override
    public void onLeftSwipe() {
        Log.d(TAG, "Right to Left swipe performed");
        moveToNext();
        updateView();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gestureDetector = new GestureDetector(this, new SwipeGestureDetector(this));
    }

    public abstract void updateView();
}
