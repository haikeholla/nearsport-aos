package com.nearsport.nearsport.ui.nav.contacts.presenters;

import com.nearsport.nearsport.http.rest.response.FindContactsResultUser;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.presenter.View;

import java.util.List;
import java.util.Map;

/**
 * Created by sguo on 6/18/16.
 */
public interface NSContactsTabView extends View {
    void addContants(Map<Character, List<NSSelectionListItem>> contacts);
    void addFindContactsResult(List<FindContactsResultUser> results);
}
