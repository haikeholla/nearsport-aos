package com.nearsport.nearsport.ui.nav.contacts.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 6/18/16.
 */
public class ItemViewHolder extends RecyclerView.ViewHolder {

    private final View rootView;
    @Bind(R.id.listview_item_left_icon)
    public ImageView imgItem;
    @Bind(R.id.listview_item_title)
    public TextView title;

    public ItemViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);

        rootView = view;
    }
}
