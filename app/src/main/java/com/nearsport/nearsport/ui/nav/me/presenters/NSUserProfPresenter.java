package com.nearsport.nearsport.ui.nav.me.presenters;

import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.presenter.BasePresenter;
import com.nearsport.nearsport.signup.SignupView;

import java.util.Map;

/**
 * Created by sguo on 7/9/16.
 */
public class NSUserProfPresenter implements BasePresenter<NSUserProfView>, IUserService.UpdateUserProfListener {

    private NSUserProfView view;

    private IUserService userService;

    public NSUserProfPresenter(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(NSUserProfView view) {
        this.view = view;
    }

    @Override
    public void onUpdateUserProfSuccess(ApiBaseResponse response) {

    }

    @Override
    public void onUpdateUserProfFailure(Throwable t) {

    }

    public void updateUserProf(Map<String, String> map, String token) {
        userService.updateUserProfile(map, token, this);
    }
}
