package com.nearsport.nearsport.ui.nav.me.presenters;

import com.nearsport.nearsport.presenter.View;

/**
 * Created by sguo on 7/9/16.
 */
public interface NSUserProfView extends View {
    void updateUserName(String name);
    void updateUserAvartar(String url);
    void updateUserGender(String gender);
    void updateUserRegion(String region);
    void updateUserMood(String mood);
}
