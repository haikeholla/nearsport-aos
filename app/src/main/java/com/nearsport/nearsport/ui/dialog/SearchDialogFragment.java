package com.nearsport.nearsport.ui.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 6/19/16.
 */
public class SearchDialogFragment extends DialogFragment {
    private static final String TAG = SearchDialogFragment.class.getSimpleName();

    @Bind(R.id.dialog_search_text)
    EditText searchText;

    protected TextWatcher textWatcher;

    public static SearchDialogFragment newInstance(TextWatcher textWatcher) {
        SearchDialogFragment f = new SearchDialogFragment();
        f.setTextWatcher(textWatcher);
        // Supply num input as an argument.
        Bundle args = new Bundle();
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_search, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    protected void initView() {
        initSearchText();
    }

    protected void initSearchText() {
        searchText.addTextChangedListener(textWatcher);
    }

    public void setTextWatcher(TextWatcher textWatcher) {
        if(textWatcher != null) {
            this.textWatcher = textWatcher;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        searchText.removeTextChangedListener(this.textWatcher);
    }
}
