package com.nearsport.nearsport.ui.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nearsport.nearsport.R;
import com.nearsport.nearsport.ui.nav.contacts.adapters.FindContactsResultListAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 6/19/16.
 */
public class AddContactDialogFragment extends DialogFragment {
    private static final String TAG = AddContactDialogFragment.class.getSimpleName();

    @Bind(R.id.input_add_contact_username)
    EditText username;
    @Bind(R.id.button_add_contact_find)
    Button findContactButton;
    @Bind(R.id.listview_find_contacts)
    ListView listView;

    public static AddContactDialogFragment newInstance() {
        AddContactDialogFragment f = new AddContactDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_add_contact, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    protected void initView() {

    }

    public void setAdapter(FindContactsResultListAdapter adapter) {
        listView.setAdapter(adapter);
    }

    public ListAdapter getAdapter() {
        return listView.getAdapter();
    }

    public void clearFoundContacts() {
        ListAdapter listAdapter = listView.getAdapter();
    }
}
