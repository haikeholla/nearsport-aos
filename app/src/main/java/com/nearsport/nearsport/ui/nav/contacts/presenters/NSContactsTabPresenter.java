package com.nearsport.nearsport.ui.nav.contacts.presenters;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.FindContactsResultRequest;
import com.nearsport.nearsport.http.rest.response.FindContactsResultResponse;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.presenter.BasePresenter;
import com.nearsport.nearsport.session.NearSportSessionManager;

import javax.inject.Inject;

/**
 * Created by sguo on 6/18/16.
 */
public class NSContactsTabPresenter
        implements BasePresenter<NSContactsTabView>, IUserService.FindContactsListener {

    private NSContactsTabView contactsTabView;

    @Inject
    public UserService userService;

    @Inject
    public NearSportSessionManager sessionManager;

    public NSContactsTabPresenter() {
        NearSportApplication.component().inject(this);
    }
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(NSContactsTabView view) {
        this.contactsTabView = view;
    }

    public void getUsersByUsername(String username) {
        FindContactsResultRequest request = new FindContactsResultRequest();
        request.token = sessionManager.getSessionToken();
        request.username = "test";
        userService.findUsers(request, this);
    }

    @Override
    public void onFindContactsSuccess(FindContactsResultResponse response) {
        contactsTabView.addFindContactsResult(response.foundUsers);
    }

    @Override
    public void onFindContactsFailure(Throwable t) {

    }
}
