package com.nearsport.nearsport.ui.nav.me.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gun0912.tedpicker.Config;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.ComposePostRequest;
import com.nearsport.nearsport.http.rest.response.UploadUserPostResponse;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.model.tag.NSPOITag;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.photoviewer.NSPhotoViewActivity;
import com.nearsport.nearsport.ui.nav.contacts.activities.NSContactSelectionActivity;
import com.nearsport.nearsport.ui.nav.me.activities.NSMeSelectionLanderActivity;
import com.nearsport.nearsport.ui.nav.me.adapters.NSGridPhotoViewAdapter;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/12/16.
 */
public class NSComposePostFragment extends Fragment implements IUserService.ComposePostListener{
    private static final String TAG = NSComposePostFragment.class.getSimpleName();
    @Inject
    NearSportSessionManager sessionManager;

    protected Activity mActivity;
    @Inject
    UserService userService;

    @Bind(R.id.compose_post_content)
    TextView postContent;

    @Bind(R.id.compose_post_chosen_photos)
    GridView chosenPhotosView;

    @Bind(R.id.compose_post_location)
    TextView postLocation;

    @Bind(R.id.compose_post_policy)
    TextView postPolicy;

//    @Bind(R.id.compose_post_mentioned_contact)
//    TextView mentionedContact;
    @Bind(R.id.compose_post_mentioned_contact_photos)
    GridView mentionedContactPhotosView;

    @Bind(R.id.compose_post_location_layout)
    RelativeLayout locationLayout;
    @Bind(R.id.compose_post_mentioned_contact_layout)
    RelativeLayout mentionedContactLayout;
    @Bind(R.id.compose_post_policy_layout)
    RelativeLayout policyLayout;

    @Bind(R.id.compose_post_send_button)
    Button composePostSendButton;

    protected NSGridPhotoViewAdapter chosenPhotoViewAdapter;
    protected NSGridPhotoViewAdapter mentionedContactPhotoViewAdapter;

    protected ComposePostRequest request = new ComposePostRequest();

    protected ProgressDialog progressDialog;

    protected ArrayList<NSSelectionListItem> photoItemList = new ArrayList<>(Constant.POST_MAX_UPLOAD_PHOTO);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NearSportApplication.component().inject(this);
        Log.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null == container) return null;
        View view = inflater.inflate(R.layout.fragment_compose_post, container, false);
        ButterKnife.bind(this, view);
        initView(getArguments());
        return view;
    }

    public void initView(Bundle bundle) {
        progressDialog = new ProgressDialog(mActivity, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("sending userPost...");

        ArrayList<Uri> image_uris = bundle.getParcelableArrayList(ImagePickerActivity.EXTRA_IMAGE_URIS);

        for(Uri uri : image_uris) {
            NSSelectionListItem item = new NSSelectionListItem();
            item.leftIconUrl = uri.toString();
            photoItemList.add(item);
        }
        final ArrayList<String> photoUrlList = new ArrayList<>(photoItemList.size());
        for(NSSelectionListItem item : photoItemList) {
            photoUrlList.add(item.leftIconUrl);
        }
        int i = 0;
        for(NSSelectionListItem item : photoItemList) {
            final int index = i;
            item.onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, NSPhotoViewActivity.class);
                    intent.putStringArrayListExtra(Constant.INTENT_PARAM_PHOTO_LIST, photoUrlList);
                    intent.putExtra(Constant.INTENT_PARAM_INDEX, index);
                    mActivity.startActivityForResult(intent, Constant.INTENT_REQUEST_PHOTO_VIEWER_WITH_DELETE);
                }
            };
            ++i;
        }
        initChosenPhotoGridview(photoItemList);
        initLocation();
        initPolicy();
        initMention();
        updateButtonText(photoUrlList.size());
        initComposePostSendButton();
    }

    public void initChosenPhotoGridview(final ArrayList<NSSelectionListItem> photoUrlList) {
        chosenPhotoViewAdapter = new NSGridPhotoViewAdapter(mActivity, R.layout.photo_gridview_cell, photoUrlList, Glide.with(mActivity));
        chosenPhotoViewAdapter.setAddPhotoButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int left = Constant.POST_MAX_UPLOAD_PHOTO - photoUrlList.size();
                if (left > 0) {
                    final Config config = new Config();
                    config.setSelectionLimit(left);
                    ImagePickerActivity.setConfig(config);
                    Intent intent = new Intent(mActivity, ImagePickerActivity.class);
                    mActivity.startActivityForResult(intent, Constant.INTENT_REQUEST_ADD_MORE_POST_PHOTOS);
                } else {
                    new MaterialDialog.Builder(mActivity)
                            .title(R.string.com_nearsport_note)
                            .content(String.format("You can upload %d photos", Constant.POST_MAX_UPLOAD_PHOTO))
                            .positiveText(R.string.com_nearsport_yes)
                            .show();
                }

            }
        });
        chosenPhotosView.setAdapter(chosenPhotoViewAdapter);
    }

    public void initLocation() {
        locationLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(NSComposePostFragment.this.mActivity)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("something like GSU recreation center", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                updateLocation(input.toString());
                                request.poiTag = new NSPOITag();
                                request.poiTag.name = input.toString();
                            }
                        }).show();
            }
        });
    }

    public void initComposePostSendButton() {
        composePostSendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                request.content = postContent.getText().toString();
                request.token = sessionManager.getSessionToken();
                progressDialog.show();
                for(NSSelectionListItem item : photoItemList) {
                    if(!Util.stringIsNullOrEmpty(item.leftIconUrl)) {
                        request.photos.add(new File(item.leftIconUrl));
                    }
                }
//                String date = Util.getTodayDate(Constant.USER_POST_CREATE_DATE_FORMAT);
                request.createdTime = new Date(System.currentTimeMillis());
                try {
                    userService.composePost(sessionManager.getUserId(), request, NSComposePostFragment.this);
                } catch (JsonProcessingException e) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void initPolicy() {
        policyLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String policy = postPolicy.getText().toString();
                int preSelectedIndex = Util.getPostPolicyIndex(policy);
                new MaterialDialog.Builder(NSComposePostFragment.this.getContext())
                        .title("Please Select UserPost Policy")
                        .items(R.array.post_policy)
                        .itemsCallbackSingleChoice(preSelectedIndex, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                NSComposePostFragment.this.updateShareTo(text.toString());
                                request.policyCode = which;
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    public void initMention() {
        mentionedContactPhotoViewAdapter = new NSGridPhotoViewAdapter(mActivity, R.layout.photo_gridview_cell, new ArrayList<NSSelectionListItem>(), Glide.with(mActivity));
        mentionedContactPhotoViewAdapter.setPhotoWidth(100);
        mentionedContactPhotoViewAdapter.setPhotoHeight(100);
        mentionedContactPhotoViewAdapter.setAddPhotoButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(NSComposePostFragment.this.mActivity, NSContactSelectionActivity.class);
                    NSComposePostFragment.this.mActivity.startActivityForResult(intent, Constant.INTENT_REQUEST_SELECT_CONTACTS);
                }
        });
        mentionedContactPhotosView.setAdapter(mentionedContactPhotoViewAdapter);
    }

    public void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    public void updateShareTo(String policy) {
        if(!Util.stringIsNullOrEmpty(policy)) {
            postPolicy.setText(policy);
        }
    }

    public void updateLocation(String location) {
        if(!Util.stringIsNullOrEmpty(location)) {
            postLocation.setText(location);
        }
    }

    public void updateButtonText(int chosenPhotoCount) {
        if(chosenPhotoCount > -1) {
            composePostSendButton.setText(String.format("UserPost (%d/%d)", chosenPhotoCount, Constant.COMPOSE_POST_MAX_CHOSEN_PHOTO_COUNT));
        }
    }

    public void addMentionedContact(NSSelectionListItem contact) {
        if(null == contact) return;
        mentionedContactPhotoViewAdapter.add(contact);
    }

    @Override
    public void onComposePostSuccess(UploadUserPostResponse response) {
        progressDialog.dismiss();
        if(response.status == 0) {
            UserPost userPost = new UserPost(request);
            userPost.sid = response.id;
            userPost.save();
            if(response.uploadedPhotos != null) {
                for (NSPhoto photo : response.uploadedPhotos) {
                    NSPhoto p = new NSPhoto(photo, userPost);
//                    photo.post = userPost;
                    p.save();
                }
            }
        } else {

        }
        NSMeSelectionLanderActivity activity = (NSMeSelectionLanderActivity)mActivity;
        activity.removeCurrentFragment();
    }

    @Override
    public void onComposePostFailure(Throwable t) {
        progressDialog.dismiss();
    }

    protected void saveToDB(UploadUserPostResponse response) {
//        UserPost userPost = new UserPost();

    }

    public void removeSelectedPhotos(ArrayList<Integer> removedIndex) {
        for(int i : removedIndex) {
            photoItemList.remove(i);
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chosenPhotoViewAdapter.notifyDataSetChanged();
            }
        });
    }

    public void addPhotos(ArrayList<Uri> addedPhotoUris) {
        for(Uri uri : addedPhotoUris) {
            NSSelectionListItem item = new NSSelectionListItem();
            item.leftIconUrl = uri.toString();
            photoItemList.add(photoItemList.size()-1, item);
        }
        final ArrayList<String> photoUrlList = new ArrayList<>(photoItemList.size());
        for(NSSelectionListItem item : photoItemList) {
            if(!Util.stringIsNullOrEmpty(item.leftIconUrl)) {
                photoUrlList.add(item.leftIconUrl);
            }
        }
        int i = 0;
        for(NSSelectionListItem item : photoItemList) {
            final int index = i;
            item.onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, NSPhotoViewActivity.class);
                    intent.putStringArrayListExtra(Constant.INTENT_PARAM_PHOTO_LIST, photoUrlList);
                    intent.putExtra(Constant.INTENT_PARAM_INDEX, index);
                    mActivity.startActivityForResult(intent, Constant.INTENT_REQUEST_PHOTO_VIEWER_WITH_DELETE);
                }
            };
            ++i;
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chosenPhotoViewAdapter.notifyDataSetChanged();
            }
        });
    }
}
