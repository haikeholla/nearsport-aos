package com.nearsport.nearsport.ui.nav.me.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.gun0912.tedpicker.Config;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.NSRecycleViewFragment;
import com.nearsport.nearsport.ui.nav.me.activities.NSUserProfLanderActivity;
import com.nearsport.nearsport.ui.nav.me.adapters.NSUserProfRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.me.presenters.NSUserProfPresenter;
import com.nearsport.nearsport.ui.nav.me.presenters.NSUserProfView;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by sguo on 5/21/16.
 */
public class NSUserProfFragment extends NSRecycleViewFragment<NSUserProfRecyclerAdapter, NSSelectionListItem, NSUserProfLanderActivity>
                                implements NSUserProfView {
    public static final String TAG = NSUserProfFragment.class.getSimpleName();

    @Inject
    NearSportSessionManager sessionManager;

    @Inject
    UserService userService;

    protected NSSelectionListItem userAvatarItem;

    protected NSSelectionListItem userNameItem;

    protected NSSelectionListItem userGenderItem;

    protected NSSelectionListItem userRealNameItem;

    protected NSSelectionListItem userRegionItem;

    protected NSSelectionListItem userMoodItem;

    protected NSUserProfPresenter presenter;

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = super.onCreateView(inflater, container, savedInstanceState);
//        actionBar = mActivity.getSupportActionBar();
//        actionBar.hide();
//        return view;
//    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        NearSportApplication.component().inject(this);
        presenter = new NSUserProfPresenter(userService);
        presenter.attachView(this);
        if(itemList.isEmpty()) {
            userAvatarItem = createUserAvatarItem();
            itemList.add(userAvatarItem);
            userNameItem = createNameItem();
            itemList.add(userNameItem);
            userRealNameItem = createUserRealNameItem();
            itemList.add(userRealNameItem);
            userGenderItem = createUserGenderItem();
            itemList.add(userGenderItem);
            userRegionItem = createUserRegionItem();
            itemList.add(userRegionItem);
            userMoodItem = createUserMoodItem();
            itemList.add(userMoodItem);
        }
        mAdapter = new NSUserProfRecyclerAdapter(Glide.with(this), itemList);
        setSeparationDecoration(SeparatorDecoration.with(mActivity).colorFromResources(R.color.iron).width(Constant.LIST_VIEW_DEFAULT_SEPERATION_WIDTH).build());
    }

    protected NSSelectionListItem createUserRealNameItem() {
        final NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Real Name";
        item.detail = sessionManager.getUserStringInfo(Constant.FNAME) + " " + sessionManager.getUserStringInfo(Constant.LNAME);
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                MaterialDialog inputDialog = new MaterialDialog.Builder(NSUserProfFragment.this.getContext())
                        .title("Update Real Name")
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("firstName lastName", item.detail, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                NSUserProfFragment.this.updateUserName(input.toString());
                                Map<String, String> map = new HashMap<>();
                                String[] names = input.toString().split(" ");
                                if(names.length > 0) {
                                    map.put(Constant.FNAME, names[0]);
                                }
                                if(names.length > 1) {
                                    map.put(Constant.LNAME, names[1]);
                                }
                                presenter.updateUserProf(map, sessionManager.getSessionToken());
                            }
                        })
                        .show();
            }
        };
        return item;
    }

    protected NSSelectionListItem createUserGenderItem() {
        final String gender = sessionManager.getUserStringInfo(Constant.GENDER);
        NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Gender";
        item.detail = gender;
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int preSelectedIndex = Util.stringIsNullOrEmpty(gender) ? -1 : "female".equalsIgnoreCase(gender) ? 0 : 1;
                new MaterialDialog.Builder(NSUserProfFragment.this.getContext())
                        .title("Update Gender")
                        .items(R.array.gender)
                        .itemsCallbackSingleChoice(preSelectedIndex, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                NSUserProfFragment.this.updateUserGender(text.toString());
                                Map<String, String> map = new HashMap<>();
                                map.put(Constant.GENDER, text.toString());
                                presenter.updateUserProf(map, sessionManager.getSessionToken());
                                return true;
                            }
                        })
                        .show();
            }
        };
        return item;
    }

    protected NSSelectionListItem createUserAvatarItem() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Profile NSPhoto";
        item.rightIconUrl = sessionManager.getUserStringInfo(Constant.ICON_URL);
        item.type = NSBaseListItem.ItemType.USER_PROF;
        final Config config = new Config();
        config.setSelectionLimit(1);
        item.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ImagePickerActivity.setConfig(config);
                Intent intent  = new Intent(NSUserProfFragment.this.mActivity, ImagePickerActivity.class);
                /*
                http://stackoverflow.com/questions/10564474/wrong-requestcode-in-onactivityresult
                startActivity from within a fragment will result in random request code
                so use including activity to do so
                 */
                NSUserProfFragment.this.mActivity.startActivityForResult(intent, Constant.INTENT_REQUEST_GET_IMAGES);
            }
        });
        return item;
    }

    protected NSSelectionListItem createNameItem() {
        final NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Name";
        item.detail = sessionManager.getUserStringInfo(Constant.USERNAME);
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                MaterialDialog inputDialog = new MaterialDialog.Builder(NSUserProfFragment.this.getContext())
                        .title("Update Username")
//                        .content(R.string.input_content)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("user name", sessionManager.getUserStringInfo(Constant.USERNAME), new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                NSUserProfFragment.this.updateUserName(input.toString());
                                Map<String, String> map = new HashMap<>();
                                map.put(Constant.USERNAME, input.toString());
                                presenter.updateUserProf(map, sessionManager.getSessionToken());
                            }
                        })
                        .show();
            }
        };
        return item;
    }

    public NSSelectionListItem createUserRegionItem() {
        final NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Region";
        item.detail = sessionManager.getUserStringInfo(Constant.REGION);
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                MaterialDialog inputDialog = new MaterialDialog.Builder(NSUserProfFragment.this.getContext())
                        .title("Update Region")
//                        .content(R.string.input_content)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("region", item.detail, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                NSUserProfFragment.this.updateUserName(input.toString());
                                Map<String, String> map = new HashMap<>();
                                map.put(Constant.REGION, input.toString());
                                presenter.updateUserProf(map, sessionManager.getSessionToken());
                            }
                        })
                        .show();
            }
        };
        return item;
    }

    public NSSelectionListItem createUserMoodItem() {
        final NSSelectionListItem item = new NSSelectionListItem();
        item.title = "Mood";
        item.detail = sessionManager.getUserStringInfo(Constant.MOOD);
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                MaterialDialog inputDialog = new MaterialDialog.Builder(NSUserProfFragment.this.getContext())
                        .title("Update Region")
//                        .content(R.string.input_content)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("region", item.detail, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                NSUserProfFragment.this.updateUserName(input.toString());
                                Map<String, String> map = new HashMap<>();
                                map.put(Constant.MOOD, input.toString());
                                presenter.updateUserProf(map, sessionManager.getSessionToken());
                            }
                        })
                        .show();
            }
        };
        return item;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUserAvartar(sessionManager.getUserAvatar());
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        updateUserAvartar(sessionManager.getUserAvatar());
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.e(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void updateUserName(String name) {
        userNameItem.detail = name;
        mAdapter.notifyItemChanged(Constant.UI_NAV_PROF_USERNAME_INDEX);
        sessionManager.updateUserInfo(Constant.USERNAME, userNameItem.detail);
    }

    @Override
    public void updateUserAvartar(String url) {
        if(!userAvatarItem.rightIconUrl.equals(url)) {
            userAvatarItem.rightIconUrl = url;
            mAdapter.notifyItemChanged(Constant.UI_NAV_PROF_AVATAR_INDEX);
        }
    }

    @Override
    public void updateUserGender(String gender) {
        userGenderItem.detail = gender;
        mAdapter.notifyItemChanged(Constant.UI_NAV_PROF_GENDER_INDEX);
        sessionManager.updateUserInfo(Constant.GENDER, gender);
    }

    @Override
    public void updateUserRegion(String region) {

    }

    @Override
    public void updateUserMood(String mood) {

    }
}
