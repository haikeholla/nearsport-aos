package com.nearsport.nearsport.ui.nav.me.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.nav.me.viewholders.NSGridPhotoViewHolder;
import com.nearsport.nearsport.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sguo on 7/13/16.
 */
public class NSGridPhotoViewAdapter extends ArrayAdapter<NSSelectionListItem> {

    protected List<NSSelectionListItem> itemList;
    protected NSSelectionListItem addPhotoButtonItem;
    protected int cols;
    private int layoutResourceId;
    private RequestManager glide;
    private Activity mActivity;

    private ArrayList<String> photoList;

    private static final int DEFAULT_COLS = 3;
    private static final int DEFAULT_PHOTO_W = 300;
    private static final int DEFAULT_PHOTO_H = 300;

    private int photoW = DEFAULT_PHOTO_W;
    private int photoH = DEFAULT_PHOTO_H;

    public NSGridPhotoViewAdapter(Context context, int layoutResourceId, ArrayList<NSSelectionListItem> itemList, RequestManager glide, int cols) {
        super(context, layoutResourceId, itemList);
        this.mActivity = (Activity) context;
        this.cols = cols;
        this.layoutResourceId = layoutResourceId;
        this.glide = glide;
        this.photoList = new ArrayList<>(itemList.size());
        for(NSSelectionListItem item : itemList) {
            photoList.add(item.leftIconUrl);
        }
        initAddPhotoButtonItem();
        itemList.add(addPhotoButtonItem);
    }

    public NSGridPhotoViewAdapter(Context context, int layoutResourceId, ArrayList<NSSelectionListItem> itemList, RequestManager glide) {
        this(context, layoutResourceId, itemList, glide, DEFAULT_COLS);
    }

    private void initAddPhotoButtonItem() {
        addPhotoButtonItem = new NSSelectionListItem();
        addPhotoButtonItem.leftIcon = R.drawable.ic_plus_border_100;
        addPhotoButtonItem.type = NSBaseListItem.ItemType.ADD_ITEM_BUTTON;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        NSGridPhotoViewHolder holder;
        if (row == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new NSGridPhotoViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (NSGridPhotoViewHolder) row.getTag();
        }
        holder.photo.setLayoutParams(new LinearLayout.LayoutParams(photoW, photoH));
        NSSelectionListItem item = getItem(position);
        switch (item.type) {
            case REGULAR:
                if(!Util.stringIsNullOrEmpty(item.leftIconUrl)) {
                    glide.load(item.leftIconUrl)
                            .dontAnimate()
                            .centerCrop()
                            .error(R.drawable.user_icon_placeholder)
                            .into(holder.photo);
                }
                break;
            case ADD_ITEM_BUTTON:
                holder.photo.setImageResource(item.leftIcon);
                break;
        }
        holder.photo.setOnClickListener(item.onClickListener);

//        if(!Util.stringIsNullOrEmpty(item.leftIconUrl)) {
//            glide.load(item.leftIconUrl)
//                    .dontAnimate()
//                    .centerCrop()
//                    .error(R.drawable.user_icon_placeholder)
//                    .into(holder.photo);
//            holder.photo.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(NSGridPhotoViewAdapter.this.getContext(), NSPhotoViewActivity.class);
//                    intent.putStringArrayListExtra(Constant.INTENT_PARAM_PHOTO_LIST, photoList);
//                    intent.putExtra(Constant.INTENT_PARAM_INDEX, position);
//                    NSGridPhotoViewAdapter.this.mActivity.startActivityForResult(intent, Constant.INTENT_REQUEST_PHOTO_VIEWER_WITH_DELETE);
//                }
//            });
//        }
        return row;
    }

    public void setPhotoWidth(int w) {
        this.photoW = w;
    }

    public void setPhotoHeight(int h) {
        this.photoH = h;
    }

    public void setAddPhotoButtonClickListener(View.OnClickListener listener) {
        if(null != listener) {
            addPhotoButtonItem.onClickListener = listener;
        }
    }

    public void addPhoto(NSSelectionListItem item) {
        if(item == null) return;
        this.add(item);
        this.notifyDataSetChanged();
    }
}
