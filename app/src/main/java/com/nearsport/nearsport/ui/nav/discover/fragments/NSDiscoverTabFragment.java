package com.nearsport.nearsport.ui.nav.discover.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.nearsport.nearsport.MainTabActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.ui.nav.NSRecycleViewFragment;
import com.nearsport.nearsport.ui.nav.discover.activities.NSMomentLanderActivity;
import com.nearsport.nearsport.ui.nav.discover.adapters.NSNavDiscoverTabRecyclerAdapter;


/**
 * Created by sguo on 5/6/16.
 */
public class NSDiscoverTabFragment extends NSRecycleViewFragment<NSNavDiscoverTabRecyclerAdapter, NSSelectionListItem, MainTabActivity> {
    public static final String TAG = NSDiscoverTabFragment.class.getSimpleName();

//    @Bind(R.id.main_toolbar)
//    protected Toolbar mainToolbar;

    public NSDiscoverTabFragment() {
        super();
    }
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = super.onCreateView(inflater, container, savedInstanceState);
//        mActivity.setSupportActionBar(mainToolbar);
//        actionBar = mActivity.getSupportActionBar();
//
//        // Enable the Up button
//        actionBar.setTitle(R.string.com_nearsport_ui_toolbar_title);
//        actionBar.setDisplayShowHomeEnabled(true);
//        return view;
//    }
    @Override
    public void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        if(itemList.isEmpty()) {
            createMoments();
        }
        mAdapter = new NSNavDiscoverTabRecyclerAdapter(Glide.with(this), itemList);
        setSeparationDecoration(SeparatorDecoration.with(mActivity).colorFromResources(R.color.iron).width(20).build());
    }

    public NSSelectionListItem createMoments() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.setTitle("Moments");
        item.setLeftIcon(R.drawable.ic_moments_48);
        item.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, NSMomentLanderActivity.class);
                mActivity.startActivity(intent);
            }
        };
        itemList.add(item);
        return item;
    }
}
