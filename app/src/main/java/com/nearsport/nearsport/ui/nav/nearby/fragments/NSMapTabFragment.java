package com.nearsport.nearsport.ui.nav.nearby.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nearsport.nearsport.http.rest.request.FetchGamesRequest;
import com.nearsport.nearsport.login.LoginActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.event.otto.AnswerAvailableEvent;
import com.nearsport.nearsport.geo.GPSTracker;
import com.nearsport.nearsport.http.rest.service.GameService;
import com.nearsport.nearsport.model.GameSkeleton;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.SimpleLatLng;
import com.nearsport.nearsport.model.TeamSkeleton;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.dialog.CreateGameDialogFragment;
import com.nearsport.nearsport.ui.nav.nearby.presenters.NSMapTabPresenter;
import com.nearsport.nearsport.ui.nav.nearby.presenters.NSMapTabView;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.DimType;
import com.nightonke.boommenu.Types.PlaceType;
//import com.squareup.otto.Bus;
//import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 5/2/16.
 */
public class NSMapTabFragment extends Fragment implements
        OnMapReadyCallback,
        NSMapTabView/*,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener*/ {
    private static final String TAG = NSMapTabFragment.class.getSimpleName();

    public static final int CREATE_GAME_FRAGMENT = 1; // adding this line

//    private UiSettings mUiSettings;

    @Inject
    public GameService gameService;

    @Inject
    CreateGameDialogFragment createGameDialogFragment;

    @Inject
    public NearSportSessionManager sessionManager;

    @Bind(R.id.boom)
    protected BoomMenuButton boomMenuButton;
    protected boolean boomMenuInit;

    // GPSTracker class
    protected GPSTracker gps;

//    protected GoogleApiClient mGoogleApiClient;

//    protected Location mLastLocation;
    protected SimpleLatLng mLastLocation = new SimpleLatLng(-84.1577019, 34.1179425);

    private GoogleMap mMap;

    @Bind(R.id.input_searchbar)
    EditText searchbarInput;

//    @Bind(R.id.btn_searchbar)
//    Button searchbarBtn;


//    @Bind(R.id.layout_swipeContainer)
//    SwipeRefreshLayout swipeContainer;


//    //    @Inject
//    RxBus rxBus = new RxBus();
//    private CompositeSubscription _subscriptions;
//Bus bus = new Bus(ThreadEnforcer.MAIN);

    @Bind(R.id.btn_login)
    Button loginButton;

//    @Bind(R.id.floating_action_button_add)
//    FloatingActionButton floatingActionButtonAdd;

//    @Bind(R.id.floating_action_button_refresh)
//    FloatingActionButton floatingActionButtonRefresh;

    private SupportMapFragment mapFragment;

    private String token = "";

    private Activity mActivity;

    private boolean fetchedNearby;

    private static final float DEFAULT_ZOOM_LEVEL = 10.3f;

    protected NSMapTabPresenter presenter;

//    private LocationRequest mLocationRequest;
//    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NearSportApplication.component().inject(this);
//        buildGoogleApiClient();
        Intent intent = getActivity().getIntent();
        //Bundle args = getArguments();
        if(null == intent) {
            if(savedInstanceState != null) {
                token = savedInstanceState.getString(Constant.AUTH_TOKEN, "");
            }
        } else {
            Bundle extras = intent.getExtras();
            if(null != extras) {
                token = extras.getString(Constant.AUTH_TOKEN, "");
            }
        }
        // Create the LocationRequest object
//        mLocationRequest = LocationRequest.create()
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
//                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        presenter = new NSMapTabPresenter(gameService);
        presenter.attachView(this);
    }

    protected void initBoomMenuLogined(BoomMenuButton.Builder builder) {
        if(null == builder) return;
        for(int i = 0; i<Constant.MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_LOGINED.length; ++i) {
            builder.addSubButton(
                    getActivity(),
                    Constant.MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_LOGINED[i],
                    Constant.MAP_PAGE_BOOM_SUBBUTTON_COLOR_LOGINED,
                    getActivity().getString(Constant.MAP_PAGE_BOOM_SUBBUTTON_TITLE_LOGINED[i])
            );
        }
        builder.onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
                    @Override
                    public void onClick(int buttonIndex) {
                        // return the index of the sub button clicked
                        switch (buttonIndex) {
                            case Constant.MPA_PAGE_BOOM_SUBBUTTON_REFRESH: fetchNearbyGames(mLastLocation); break;
                            case Constant.MPA_PAGE_BOOM_SUBBUTTON_ZOOMIN: zoomIn(); break;
                            case Constant.MPA_PAGE_BOOM_SUBBUTTON_ZOOMOUT: zoomOut(); break;
                            case Constant.MPA_PAGE_BOOM_SUBBUTTON_NEW: showCreateGameDialog(); boomMenuButton.dismiss(); break;
                        }
                    }
                });
    }

    protected void initBoomMenuNonLogined(BoomMenuButton.Builder builder) {
        if(null == builder) return;
        for(int i = 0; i<Constant.MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_NON_LOGINED.length; ++i) {
            builder.addSubButton(
                    getActivity(),
                    Constant.MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_NON_LOGINED[i],
                    Constant.MAP_PAGE_BOOM_SUBBUTTON_COLOR_NON_LOGINED,
                    getActivity().getString(Constant.MAP_PAGE_BOOM_SUBBUTTON_TITLE_NON_LOGINED[i])
            );
        }
        builder.onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
            @Override
            public void onClick(int buttonIndex) {
                // return the index of the sub button clicked
                switch (buttonIndex) {
                    case Constant.MPA_PAGE_BOOM_SUBBUTTON_REFRESH: fetchNearbyGames(mLastLocation); break;
                    case Constant.MPA_PAGE_BOOM_SUBBUTTON_ZOOMIN: zoomIn(); break;
                    case Constant.MPA_PAGE_BOOM_SUBBUTTON_ZOOMOUT: zoomOut(); break;
                }
            }
        });
    }

    protected void initBoomMenu(boolean logined) {
//        if(boomMenuInit) return;
//        boomMenuInit = true;

        BoomMenuButton.Builder builder = new BoomMenuButton.Builder()
                                            .autoDismiss(false)
                                            .boom(BoomType.PARABOLA)
                                            .button(ButtonType.CIRCLE)
                                            .dim(DimType.DIM_0)
                                            .subButtonTextColor(R.color.material_white);
//                                            .subButtonsShadow(com.nightonke.boommenu.Util.getInstance().dp2px(2), com.nightonke.boommenu.Util.getInstance().dp2px(2));
        initBoomMenuNonLogined(builder);
        if(logined) {
            initBoomMenuLogined(builder);
        }
        int totalSubButtons = Constant.MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_NON_LOGINED.length + Constant.MAP_PAGE_BOOM_SUBBUTTON_DRAWABLES_LOGINED.length;
        switch(totalSubButtons) {
            case 1 : builder.place(PlaceType.CIRCLE_1_1); break;
            case 2 : builder.place(PlaceType.CIRCLE_2_1); break;
            case 3 : builder.place(PlaceType.CIRCLE_3_1); break;
            case 4 : builder.place(PlaceType.CIRCLE_4_1); break;
            case 5 : builder.place(PlaceType.CIRCLE_5_1); break;
            case 6 : builder.place(PlaceType.CIRCLE_6_1); break;
            case 7 : builder.place(PlaceType.CIRCLE_7_1); break;
            case 8 : builder.place(PlaceType.CIRCLE_8_1); break;
            case 9 : builder.place(PlaceType.CIRCLE_9_1); break;
        }
        builder.init(boomMenuButton);

//        Drawable[] subButtonDrawables = new Drawable[3];
//        int[] drawablesResource = new int[]{
//                R.drawable.ic_search_black_24dp,
//                R.drawable.ic_zoom_in_24dp,
//                R.drawable.ic_zoom_out_24dp
//        };
//        for (int i = 0; i < 3; i++)
//            subButtonDrawables[i] = ContextCompat.getDrawable(mActivity, drawablesResource[i]);
//
//        String[] subButtonTexts = new String[]{"BoomMenuButton", "View source code", "Follow me"};
//
//        int[][] subButtonColors = new int[3][2];
//        for (int i = 0; i < 3; i++) {
//            subButtonColors[i][1] = ContextCompat.getColor(mActivity, R.color.material_white);
//            subButtonColors[i][0] = com.nightonke.boommenu.Util.getInstance().getPressedColor(subButtonColors[i][1]);
//        }
//
//        // Now with Builder, you can init BMB more convenient
//        new BoomMenuButton.Builder()
//                .subButtons(subButtonDrawables, subButtonColors, subButtonTexts)
//                .button(ButtonType.CIRCLE)
//                .boom(BoomType.PARABOLA)
//                .place(PlaceType.CIRCLE_3_1)
//                .subButtonTextColor(ContextCompat.getColor(mActivity, R.color.black))
//                .subButtonsShadow(com.nightonke.boommenu.Util.getInstance().dp2px(2), com.nightonke.boommenu.Util.getInstance().dp2px(2))
//                .onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
//                    @Override
//                    public void onClick(int buttonIndex) {
//                        // return the index of the sub button clicked
//                        Toast.makeText(getContext(), "index = " + buttonIndex, Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .autoDismiss(false)
//                .init(boomMenuButton);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container == null) return null;
//        View view = (SwipeRefreshLayout)inflater.inflate(R.layout.fragment_nearsport_map, container, false);
        View view = inflater.inflate(R.layout.fragment_nearsport_map, container, false);
        ButterKnife.bind(this, view);
        initUI();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case CREATE_GAME_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent and the displaying it.
                    Bundle bundle = data.getExtras();
                    if(null != bundle) {
                        GameSkeleton game = (GameSkeleton) bundle.getParcelable(Constant.GAME);
                        addGameToMap(game);
                    }
                }
                break;
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(getContext(), "Map Ready!!!", Toast.LENGTH_SHORT).show();
        mMap = googleMap;
//        mUiSettings = mMap.getUiSettings();
//        mUiSettings.setZoomControlsEnabled(true);
        tryUpdateLocation();
    }

//    public void addNearbyGamesToMaps(FetchNearbyGamesResponse res) {
//        if(null != mMap && null != res && null != res.content) {
//            for(GameSkeleton gs : res.content) {
//                LatLng latLng = new LatLng(gs.location.latitude, gs.location.longitude);
//                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(gs.name));
//
//            }
//        }
//    }

    public <T extends GameSkeleton> void addGamesToMap(List<T> games) {
        if(null != games) {
            Log.d(TAG, "# of nearby games: " + games.size());
            for(T game : games) {
                LatLng latLng = new LatLng(game.location.latitude, game.location.longitude);
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(game.name));
            }
        }
    }

    @Override
    public <T extends TeamSkeleton> void addTeamsToMap(List<T> teams) {

    }

    @Override
    public void addUsersToMap(List<PersonSkeleton> users) {

    }

    public <T extends GameSkeleton> void addGameToMap(T game) {
        if(null != game) {
            LatLng latLng = new LatLng(game.location.latitude, game.location.longitude);
            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(game.name));
        }
    }

    public void clearMap() {
        if(mMap != null) {
            mMap.clear();
        }
    }

    public void fetchNearbyPersonAsync() {

        Log.e(TAG, "finish fetchNearbyPersonAsync");
    }

    public void fetchNearbyTeamsAsync() {

        Log.e(TAG, "finish fetchNearbyTeamsAsync");
    }

    private List<GameSkeleton> fetchNearbyGamesLocally() {
        // TO DO
        return null;
    }

    private void fetchNearbyGamesAsync(SimpleLatLng location) {
        FetchGamesRequest request = new FetchGamesRequest();
        if(null != location) {
            request.loc.longitude = location.longitude;
            request.loc.latitude = location.latitude;
        } else {
            request.loc.longitude = mLastLocation.longitude;
            request.loc.latitude = mLastLocation.latitude;
        }
        Log.e(TAG, "lng = " + request.loc.longitude + " : lat = " + request.loc.latitude);
        request.radius = 50;
        request.sportType = GameSkeleton.SportType.UNKNOWN;
        request.page = 0;
        request.size = 10;

        presenter.fetchGames(request);
        Log.e(TAG, "finish fetchNearbyGamesAsync");
    }

    public void fetchNearbyGames(SimpleLatLng location) {
        List<GameSkeleton> games = fetchNearbyGamesLocally();
        if(null == games) {
            fetchNearbyGamesAsync(location);
        }
    }

    public void fetchNearbyObjectsAsync(SimpleLatLng location) {
        fetchNearbyGames(location);
        fetchNearbyTeamsAsync();
        fetchNearbyPersonAsync();
    }

    protected void initUI() {
        mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
        if(mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

//        floatingActionButtonRefresh.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                fetchNearbyObjectsAsync(mLastLocation);
//            }
//        });
        searchbarInput.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchbarInput.getRight() - searchbarInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Log.e(TAG, "search clicked");
                        return true;
                    }
                }
                return false;
            }
        });
//        searchbarBtn.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                //loginBtn.setVisibility(View.VISIBLE);
//                Intent intent = new Intent(getActivity().getApplicationContext(), MainTabActivity.class);
//                startActivity(intent);
//                getActivity().getSupportFragmentManager().beginTransaction().remove(NSMapTabFragment.this).commit();
//                getActivity().finish();
//            }
//        });
        boolean logined = sessionManager.isLoggedIn();
        initBoomMenu(logined);
        if(logined) {
            initLoginedUI();
        } else {
            initNonLoginedUI();
            initLoginedUI();
        }
    }

    protected void initNonLoginedUI() {
        loginButton.setVisibility(View.VISIBLE);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "login button clicked");
                Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    protected void initLoginedUI() {
        Bundle bundle = new Bundle();
        bundle.putString(Constant.AUTH_TOKEN, token);
        createGameDialogFragment.setArguments(bundle);
        createGameDialogFragment.setTargetFragment(this, CREATE_GAME_FRAGMENT);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(null != floatingActionButtonAdd) {
//            floatingActionButtonAdd.setOnClickListener(null);
//        }
//        if(null != floatingActionButtonRefresh) {
//            floatingActionButtonRefresh.setOnClickListener(null);
//        }
//        if(null != loginButton) {
//            loginButton.setOnClickListener(null);
//        }
//        if(null != searchbarBtn) {
//            searchbarBtn.setOnClickListener(null);
//        }
//        if(null != mMap) {
//            mMap.clear();
//            mMap = null;
//        }
//        if(null != mapFragment) {
//            mapFragment = null;
//        }
//        if(null != mActivity) {
//            mActivity = null;
//        }
//        if(boomMenuButton != null) {
//            boomMenuButton = null;
//        }
        ButterKnife.unbind(this);
//        createGameDialogFragment = null;
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
//    protected void buildGoogleApiClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this.getActivity())
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.disconnect();
//        }
//    }

//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//        if ( ContextCompat.checkSelfPermission( this.getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED )
//        {
//            ActivityCompat.requestPermissions( this.getActivity(), new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION },
//                    1 );
//        }
//        Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        if (loc != null) {
//            mLastLocation.latitude = loc.getLatitude();
//            mLastLocation.longitude = loc.getLongitude();
//            if(null != mMap) {
//                LatLngBounds bounds = new LatLngBounds.Builder()
//                        .include(new LatLng(mLastLocation.latitude, mLastLocation.longitude))
//                        .build();
//                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
//                Log.d(TAG, "google map move to lng = " + mLastLocation.longitude + " : lat = " + mLastLocation.latitude);
//            }
//            Log.d(TAG, "lng = " + mLastLocation.longitude + " : lat = " + mLastLocation.latitude);
//        } else {
//            Toast.makeText(this.getActivity(), "No Location detected", Toast.LENGTH_LONG).show();
//        }
//        if(!fetchedNearby) {
//            fetchNearbyObjectsAsync(mLastLocation);
//            fetchedNearby = true;
//        }
//    }

    public void focusMap() {
        if(null != mMap) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.latitude, mLastLocation.longitude), DEFAULT_ZOOM_LEVEL), 1000, null);
        }
    }

//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }

    public void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onResume() {
        super.onResume();
//        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {

        super.onPause();

//        if (mGoogleApiClient.isConnected()) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//            mGoogleApiClient.disconnect();
//        }

    }

    @Override
    public void onStop() {
        super.onStop();
//        BusProvider.getInstance().unregister(this);
    }

    public void tryUpdateLocation() {
        if(gps == null) {
            gps = new GPSTracker(mActivity);
        } else {
            gps.getLocation();
            if(gps.canGetLocation()) {
                mLastLocation.latitude = gps.getLatitude();
                mLastLocation.longitude = gps.getLongitude();
                Toast.makeText(getContext(), "Your Location is - \nLat: " + mLastLocation.latitude + "\nLong: " + mLastLocation.longitude, Toast.LENGTH_LONG).show();
                fetchNearbyGames(mLastLocation);
            } else {
//            gps.showSettingsAlert();
            }
        }
    }

//    private void handleNewLocation(Location location) {
//        Log.d(TAG, location.toString());
//
//        double currentLatitude = location.getLatitude();
//        double currentLongitude = location.getLongitude();
//
//        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
//
//        //mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
//        MarkerOptions options = new MarkerOptions()
//                .position(latLng)
//                .title("I am here!");
//        mMap.addMarker(options);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//    }


//    @Override
//    public void onConnected(Bundle bundle) {
//        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
//        {
////            ActivityCompat.requestPermissions((Activity)mContext, new String[] { Manifest.permission_group.LOCATION}, NSPermissionManager.LOCATION_RESULT);
//            // Permission to access the location is missing.
//            PermissionUtils.requestPermission((AppCompatActivity)mActivity, NSPermissionManager.LOCATION_RESULT,
//                    Manifest.permission.ACCESS_FINE_LOCATION, false);
//        } else {
//            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            if (location == null) {
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//            } else {
//                handleNewLocation(location);
//            }
//        }
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        /*
//         * Google Play services can resolve some errors it detects.
//         * If the error has a resolution, try sending an Intent to
//         * start a Google Play services activity that can resolve
//         * error.
//         */
//        if (connectionResult.hasResolution()) {
//            try {
//                // Start an Activity that tries to resolve the error
//                connectionResult.startResolutionForResult(mActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
//                /*
//                 * Thrown if Google Play services canceled the original
//                 * PendingIntent
//                 */
//            } catch (IntentSender.SendIntentException e) {
//                // Log the error
//                e.printStackTrace();
//            }
//        } else {
//            /*
//             * If no resolution is available, display a dialog to the
//             * user with the error.
//             */
//            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
//        }
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        handleNewLocation(location);
//    }

    public void zoomIn() {
        if(mMap == null) return;
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
    }

    public void zoomOut() {
        if(mMap == null) return;
        mMap.animateCamera(CameraUpdateFactory.zoomOut());
    }

    public void showCreateGameDialog() {
        createGameDialogFragment.show(getFragmentManager(), "CreateGameDialogFragment");
    }
}
