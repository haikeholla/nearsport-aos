package com.nearsport.nearsport.ui.nav.chat.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageButton;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 6/26/16.
 */
public class NSChatLanderActivity extends AppCompatActivity {

    @Bind(R.id.messageEditText)
    EditText messageToSend;

    @Bind(R.id.sendMessageButton)
    ImageButton sendMessageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_lander);
        ButterKnife.bind(this);
    }
}
