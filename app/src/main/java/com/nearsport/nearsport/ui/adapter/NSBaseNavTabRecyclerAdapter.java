package com.nearsport.nearsport.ui.adapter;

import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
//import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sguo on 5/4/16.
 */
public abstract class NSBaseNavTabRecyclerAdapter<T extends NSBaseListItem> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<T> mItemList;
    protected RequestManager mGlide;

    public NSBaseNavTabRecyclerAdapter(RequestManager glide, List<T> itemList) {
        this.mGlide = glide;
        this.mItemList = itemList;
    }

//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        //ListItem listItem = navTabMeItemList.get(i);
//        View view = null;
//        ListItem.ItemType itemType = ListItem.ItemType.valueOf(i);
//        Log.e(TAG, itemType.name());
//        switch(itemType) {
//            case SEPARATOR_ROW: {
//                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview_separator_row, null);
//                break;
//            }
//            case SETTING_ROW: {
//                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview_item_row, null);
//                break;
//            }
//            case CHAT_ROW: {
//                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview_chat_item_row, null);
//            }
//        }
////        if(0 == i) {
////            Log.e(TAG, "separator");
////            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview_separator_row, null);
////        } else {
////            Log.e(TAG, "item");
////            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview_item_row, null);
////        }
//        NearSportListItemViewHolder viewHolder = new NearSportListItemViewHolder(view);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        ListItem listItem = navTabMeItemList.get(position);
//        switch(listItem.getType()) {
//            case SEPARATOR_ROW: {
//                break;
//            }
//            case SETTING_ROW: {
//                if(listItem.isUseSystemDrawable()) {
//                    Glide.with(context).load(listItem.getDrawableRes()).into(holder.iconView);
//
//                } else {
//                    Glide.with(context).load(listItem.getIcon())
////                .error(R.drawable.placeholder)
////                .placeholder(R.drawable.placeholder)
//                            .into(holder.iconView);
//                }
//                holder.titleView.setText(listItem.getTitle());
//                break;
//            }
//            case CHAT_ROW: {
//                NSChatListItem chatListItem = (NSChatListItem) listItem;
//                if(chatListItem != null) {
////                    Picasso.with(context).load(listItem.getDrawableRes()).into(holder.iconView);
//                    Glide.with(context).load(listItem.getDrawableRes()).into(holder.iconView);
//
////                } else {
////                    Picasso.with(context).load(listItem.getIcon())
////                .error(R.drawable.placeholder)
////                .placeholder(R.drawable.placeholder)
////                            .into(holder.iconView);
////                }
//                    holder.titleView.setText(listItem.getTitle());
//                    holder.contentView.setText(chatListItem.getDisplayContent(20));
//                }
//
//                break;
//            }
//        }
////        if(listItem.isSeparator()) {
////            Log.e(TAG, "onBindViewHolder sep");
//////            ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();
//////
//////            params.height = listItem.getLayoutHeight() > 0 ? listItem.getLayoutHeight() : params.height;
//////            params.width = listItem.getLayoutWidth() > 0 ? listItem.getLayoutWidth() : params.width;
//////            holder.itemView.setLayoutParams(params);
////            return;
////        }
////        if(listItem.isUseSystemDrawable()) {
////            Picasso.with(context).load(listItem.getDrawableRes()).into(holder.iconView);
////
////        } else {
////            Picasso.with(context).load(listItem.getIcon())
//////                .error(R.drawable.placeholder)
//////                .placeholder(R.drawable.placeholder)
////                    .into(holder.iconView);
////        }
////        holder.titleView.setText(listItem.getTitle());
//    }

    @Override
    public int getItemCount() {
        return (null != mItemList ? mItemList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        NSBaseListItem listItem = mItemList.get(position);
        return listItem.type.code;
    }
}
