package com.nearsport.nearsport.ui.dialog;

import android.support.v4.app.DialogFragment;

/**
 * Created by sguo on 5/7/16.
 */
public interface NearSportDialogListener {

    public void onDialogPositiveClick(DialogFragment dialog);
    public void onDialogNegativeClick(DialogFragment dialog);
}
