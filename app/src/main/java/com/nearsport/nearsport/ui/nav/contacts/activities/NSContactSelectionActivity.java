package com.nearsport.nearsport.ui.nav.contacts.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.activeandroid.query.Select;
import com.bumptech.glide.Glide;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.ui.nav.contacts.adapters.NSContactSelectionAdapter;
import com.nearsport.nearsport.ui.nav.contacts.comparators.NSContactComparator;
import com.nearsport.nearsport.util.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/16/16.
 */
public class NSContactSelectionActivity extends AppCompatActivity {
    private static final String TAG = NSContactSelectionActivity.class.getSimpleName();
    protected static final int SPAN_COUNT = 2;
    protected static final String KEY_LAYOUT_MANAGER = "layoutManager";

    @Bind(R.id.nav_tab_me)
    RecyclerView contactRecyclerView;

    @Bind(R.id.main_toolbar)
    Toolbar mainToolbar;

    protected ArrayList<NSSelectionListItem> contactList;

    protected NSContactSelectionAdapter mAdapter;

    protected RecyclerView.ItemDecoration separationDecoration;

    protected enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected LayoutManagerType currentLayoutManagerType;
    protected RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_selection);
//        NearSportApplication.component().inject(this);
        ButterKnife.bind(this);
        setSupportActionBar(mainToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Select Contacts");

        currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            currentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(currentLayoutManagerType);

        initContactList();

        mAdapter = new NSContactSelectionAdapter(this, Glide.with(this), contactList);
        setSeparationDecoration(SeparatorDecoration.with(this).colorFromResources(R.color.iron).width(Constant.LIST_VIEW_DEFAULT_SEPERATION_WIDTH).build());
        if(this.separationDecoration != null) {
            contactRecyclerView.addItemDecoration(this.separationDecoration);
        }
        contactRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_action_done, menu);
        return true;
    }

    protected ArrayList<NSSelectionListItem> getSelectedContacts() {
        ArrayList<NSSelectionListItem> ret = new ArrayList<>(10);
        for(NSSelectionListItem item : contactList) {
            if(item.isSelected) {
                ret.add(item);
            }
        }
        return ret;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent resultIntent = new Intent();
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED, resultIntent);
                finish();
                break;
            case R.id.menu_item_action_done:
                resultIntent.putParcelableArrayListExtra(Constant.CONTACT_LIST, getSelectedContacts());
//                resultIntent.putExtra(Constant.CONTACT_LIST, getSelectedContacts());
                setResult(RESULT_OK, resultIntent);
                finish();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    protected void initContactList() {
        List<PersonSkeleton> contacts = (new Select()).from(PersonSkeleton.class).orderBy(Constant.USERNAME).execute();
        contactList = new ArrayList<>(contacts.size() + Constant.alphabet.length);
        for(char ch : Constant.alphabet) {
            NSSelectionListItem item = new NSSelectionListItem();
            item.title = String.valueOf(ch);
            item.type = NSBaseListItem.ItemType.SECTION_HEADER;
            item.backgroudColor = ContextCompat.getColor(this, R.color.iron);
            contactList.add(item);
        }
        for(PersonSkeleton personSkeleton : contacts) {
            NSSelectionListItem contactItem = new NSSelectionListItem();
            contactItem.title = personSkeleton.username;
            contactItem.leftIconUrl = personSkeleton.iconUrl;
            contactItem.leftIcon = Integer.parseInt(personSkeleton.iconUrl);
            contactList.add(contactItem);
        }
        Collections.sort(contactList, new NSContactComparator());
    }

    public void setSeparationDecoration(final RecyclerView.ItemDecoration separationDecoration) {
        this.separationDecoration = separationDecoration;
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (contactRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) contactRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(this, SPAN_COUNT);
                currentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(this);
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(this);
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        contactRecyclerView.setLayoutManager(mLayoutManager);
        contactRecyclerView.scrollToPosition(scrollPosition);
    }
}
