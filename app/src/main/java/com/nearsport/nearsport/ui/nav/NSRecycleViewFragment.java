package com.nearsport.nearsport.ui.nav;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSBaseListItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 5/6/16.
 */
public class NSRecycleViewFragment<T extends RecyclerView.Adapter, L extends NSBaseListItem, A extends Activity> extends Fragment {

    private static final String TAG = NSRecycleViewFragment.class.getSimpleName();
    protected static final String KEY_LAYOUT_MANAGER = "layoutManager";
    protected static final int SPAN_COUNT = 2;


    @Bind(R.id.nav_tab_me)
    public RecyclerView mListView;

    protected T mAdapter;

    protected List<L> itemList = new ArrayList<>();

    protected A mActivity;

    protected ActionBar actionBar;

    protected enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected LayoutManagerType currentLayoutManagerType;
    protected RecyclerView.LayoutManager mLayoutManager;

    protected RecyclerView.ItemDecoration separationDecoration;
    protected RecyclerView.ItemDecoration headerDecoration;

    public NSRecycleViewFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(null == container) return null;
        View view = inflater.inflate(R.layout.fragment_nearsport_me, container, false);
        ButterKnife.bind(this, view);
        initView(savedInstanceState);

//        AppCompatActivity activity = (AppCompatActivity) getActivity();

        currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            currentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(currentLayoutManagerType);
        mListView.setAdapter(mAdapter);
        if(this.separationDecoration != null) {
            mListView.addItemDecoration(this.separationDecoration);
        }
        if(this.headerDecoration != null) {
            mListView.addItemDecoration(this.headerDecoration);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setSeparationDecoration(final RecyclerView.ItemDecoration separationDecoration) {
        this.separationDecoration = separationDecoration;
    }

    public void setHeaderDecoration(final RecyclerView.ItemDecoration headerDecoration) {
        this.headerDecoration = headerDecoration;
    }


    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mListView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mListView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                currentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mListView.setLayoutManager(mLayoutManager);
        mListView.scrollToPosition(scrollPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, currentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setRecyclerViewAdapter(T adapter) {
        this.mAdapter = adapter;
    }

    protected void initView(Bundle savedInstanceState) {
    }

    public void setActivity(A mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (A) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity = null;
//        mAdapter = null;
        ButterKnife.unbind(this);
    }
}
