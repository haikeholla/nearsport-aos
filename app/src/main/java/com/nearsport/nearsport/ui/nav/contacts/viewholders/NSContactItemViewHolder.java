package com.nearsport.nearsport.ui.nav.contacts.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by sguo on 5/15/16.
 */
public class NSContactItemViewHolder extends RecyclerView.ViewHolder {
    public NSContactItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
