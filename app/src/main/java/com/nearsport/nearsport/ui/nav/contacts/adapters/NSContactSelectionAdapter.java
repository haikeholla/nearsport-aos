package com.nearsport.nearsport.ui.nav.contacts.adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import static com.nearsport.nearsport.model.ui.NSBaseListItem.ItemType;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.viewholders.NSBaseListItemViewHolder;

import java.util.List;

/**
 * Created by sguo on 7/16/16.
 */
public class NSContactSelectionAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {

    protected Activity mActivity;
    public NSContactSelectionAdapter(Activity activity, RequestManager glide, List<NSSelectionListItem> itemList) {
        super(glide, itemList);
        mActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemType itemType = ItemType.valueOf(viewType);
        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
        viewHolder = new NSBaseListItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final NSSelectionListItem listItem = this.mItemList.get(position);
        final NSBaseListItemViewHolder viewHolder = (NSBaseListItemViewHolder) holder;
        viewHolder.title.setText(listItem.title);
        switch (listItem.type) {
            case SECTION_HEADER: {
                viewHolder.rootLayout.setBackgroundColor(listItem.backgroudColor);
                viewHolder.title.setTextColor(ContextCompat.getColor(mActivity, R.color.white));
                viewHolder.title.setTextSize(16);
                viewHolder.leftIcon.setVisibility(View.GONE);
                viewHolder.rightIcon.setVisibility(View.GONE);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.setMargins(20,5,5,0);
                viewHolder.title.setLayoutParams(params);
                break;
            }
            case REGULAR:{
                viewHolder.rootLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(viewHolder.rightIcon.getDrawable() == null) {
                            viewHolder.rightIcon.setImageResource(R.drawable.ic_checkmark_24);
                            listItem.isSelected = true;
                        } else {
                            viewHolder.rightIcon.setImageDrawable(null);
                            listItem.isSelected = false;
                        }
                    }
                });
                mGlide.load(listItem.leftIconUrl)
                        .dontAnimate()
                        .centerCrop()
                        .error(R.drawable.user_icon_placeholder)
                        .placeholder(R.drawable.user_icon_placeholder)
                        .into(viewHolder.leftIcon);
                break;
            }
        }
    }
}
