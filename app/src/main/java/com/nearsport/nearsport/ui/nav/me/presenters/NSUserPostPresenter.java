package com.nearsport.nearsport.ui.nav.me.presenters;

import com.nearsport.nearsport.http.rest.response.ApiBaseResponse;
import com.nearsport.nearsport.http.rest.service.INSPhotoService;
import com.nearsport.nearsport.http.rest.service.NSPhotoService;
import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.presenter.BasePresenter;
import com.nearsport.nearsport.session.NearSportSessionManager;

/**
 * Created by sguo on 8/7/16.
 */
public class NSUserPostPresenter
        implements
        BasePresenter<NSUserPostView>,
        INSPhotoService.GetPhotoCommentsAndLikesListener,
        INSPhotoService.AddCommentListener,
        INSPhotoService.AddLikeListener {

    private NSPhotoService photoService;
    private NearSportSessionManager sessionManager;
    private NSUserPostView view;

    public NSUserPostPresenter(NSPhotoService photoService, NearSportSessionManager sessionManager) {
        this.photoService = photoService;
        this.sessionManager = sessionManager;
    }
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(NSUserPostView view) {
        this.view = view;
    }

    @Override
    public void onGetPhotoCommentsAndLikesSuccess(NSPhoto photo) {

    }

    @Override
    public void onGetPhotoCommentsAndLikesFailure(Throwable t) {

    }

    public void fetchPhotoCommentsAndLikes(long photoId) {
        photoService.getPhotoCommentsAndLikes(photoId, sessionManager.getSessionToken(), this);
    }

    public void likePhoto(long photoId) {
        photoService.addLike(photoId, sessionManager.getSessionToken(), this);
    }

    public void commentPhoto(long photoId, String comment) {
        photoService.addComment(photoId, comment, sessionManager.getSessionToken(), this);
    }

    @Override
    public void onAddCommentSuccess(ApiBaseResponse response) {

    }

    @Override
    public void onAddCommentFailure(Throwable t) {

    }

    @Override
    public void onAddLikeSuccess(ApiBaseResponse response) {

    }

    @Override
    public void onAddLikeFailure(Throwable t) {

    }
}
