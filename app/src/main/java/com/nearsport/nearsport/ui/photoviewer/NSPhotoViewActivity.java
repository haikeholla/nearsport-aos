package com.nearsport.nearsport.ui.photoviewer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.util.Constant;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/21/16.
 */
public class NSPhotoViewActivity extends NSBaseSwipePhotoViewer {
    private static final String TAG = NSPhotoViewActivity.class.getSimpleName();

    @Bind(R.id.photo_view_content)
    public TextView content;

    @Bind(R.id.photo_view_comment)
    public TextView comment;

    @Bind(R.id.photo_view_image)
    public ImageView image;

    @Bind(R.id.photo_view_likes)
    public TextView likes;
    @Bind(R.id.main_toolbar)
    Toolbar mainToolbar;

    protected ArrayList<String> photoList;

    protected RequestManager glide;

    protected ActionBar actionBar;

    protected ArrayList<Integer> removedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        ButterKnife.bind(this);

        setSupportActionBar(mainToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = this.getIntent();
        photoList = intent.getStringArrayListExtra(Constant.INTENT_PARAM_PHOTO_LIST);
        currentIndex = intent.getIntExtra(Constant.INTENT_PARAM_INDEX, 0);
        glide = Glide.with(this);

        removedIndex = new ArrayList<>();
        count = photoList.size();

        updateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_action_delete, menu);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void updateView() {
        actionBar.setTitle(String.format("%d/%d", currentIndex+1, photoList.size()));
        glide.load(photoList.get(currentIndex))
                .centerCrop()
                .dontAnimate()
                .into(image);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent resultIntent = new Intent();
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                resultIntent.putIntegerArrayListExtra(Constant.INTENT_PARAM_PHOTO_LIST, removedIndex);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                break;
            case R.id.menu_item_action_delete:
                new MaterialDialog.Builder(this)
                        .title(R.string.com_nearsport_note)
                        .content(R.string.com_nearsport_msg_note_delete_photo)
                        .positiveText(R.string.com_nearsport_yes)
                        .negativeText(R.string.com_nearsport_cancel)
                        .positiveColorRes(R.color.green)
                        .negativeColorRes(R.color.iron)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // delete selected
                                NSPhotoViewActivity.this.removedIndex.add(NSPhotoViewActivity.this.currentIndex);
                                photoList.remove(NSPhotoViewActivity.this.currentIndex);
                                NSPhotoViewActivity.this.onLeftSwipe();
                            }
                        })
                        .show();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
