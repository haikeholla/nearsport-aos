package com.nearsport.nearsport.ui.nav.me.presenters;

import com.nearsport.nearsport.presenter.BasePresenter;

/**
 * Created by sguo on 7/14/16.
 */
public class NSComposePostPresenter implements BasePresenter<NSComposePostView> {
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(NSComposePostView view) {

    }
}
