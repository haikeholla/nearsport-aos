package com.nearsport.nearsport.ui.nav.me.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.gun0912.tedpicker.ImagePickerActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.event.otto.BusProvider;
import com.nearsport.nearsport.http.rest.response.UploadUserAvatarResponse;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.me.fragments.NSUserProfFragment;
import com.nearsport.nearsport.util.Constant;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 5/21/16.
 */
public class NSUserProfLanderActivity extends AppCompatActivity implements UserService.UploadUserAvatarListener /*implements ActivityCompat.OnRequestPermissionsResultCallback*/ {
    private static final String TAG = NSUserProfLanderActivity.class.getSimpleName();
    public static final int PROFILE = 0;

    private int currentFragmentId = -1;

    @Bind(R.id.main_toolbar)
    Toolbar mainToolbar;

    @Inject
    NSUserProfFragment userProfFragment;

    @Inject
    NearSportSessionManager sessionManager;

    @Inject
    UserService userService;

//    protected UserAvatarSelectedEvent userAvatarSelectedEvent =  new UserAvatarSelectedEvent();

//    private PhotoPickerFragment pickerFragment;
    public final static int DEFAULT_MAX_COUNT = 9;
    public final static int DEFAULT_COLUMN_NUMBER = 4;
    private int maxCount = DEFAULT_MAX_COUNT;
    /** to prevent multiple calls to inflate menu */
    private boolean menuIsInflated = false;
    private boolean showGif = false;
    private boolean showCamera = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me_selection_lander);
        NearSportApplication.component().inject(this);
        ButterKnife.bind(this);
//        BusProvider.getInstance().register(this);
        setSupportActionBar(mainToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
//        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //ab.setDisplayShowHomeEnabled(true);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constant.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant
        }

//        pickerFragment = PhotoPickerFragment.newInstance(showCamera, showGif, DEFAULT_COLUMN_NUMBER, maxCount);
        if (findViewById(R.id.me_selection_lander_fragment_container) != null) {
            Intent intent = getIntent();
            if(null != intent && intent.hasExtra("fragmentId")) {
                int fragmentId = intent.getIntExtra("fragmentId", -1);
                switch (fragmentId) {
                    case PROFILE: {
                        currentFragmentId = PROFILE;
                        ab.setTitle("My Profile");
                        userProfFragment.setActivity(this);
                        userProfFragment.setArguments(getIntent().getExtras());
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.me_selection_lander_fragment_container, userProfFragment).commit();
//                        userProfFragment.updateUserAvatar();
                        break;
                    }
                }
            }
        }
    }

    public void swtichToFragment(int fragmentId) {
        switch (fragmentId) {
            case Constant.INTENT_REQUEST_PHOTO_PICKER: {

//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.me_selection_lander_fragment_container, pickerFragment)
//                        .commit();
//                getSupportFragmentManager().executePendingTransactions();
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resuleCode, Intent intent) {
        super.onActivityResult(requestCode, resuleCode, intent);

        if (requestCode == Constant.INTENT_REQUEST_GET_IMAGES && resuleCode == Activity.RESULT_OK ) {
            ArrayList<Uri> image_uris = intent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
            for(Uri uri : image_uris) {
                Log.e(TAG, uri.getPath());
                sessionManager.updateUserInfo(Constant.ICON_URL, uri.getPath());
                userService.uploadUserAvatar(uri, sessionManager.getUserDetails().get(Constant.AUTH_TOKEN), this);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        BusProvider.getInstance().register(this);
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
//        EventBus.getDefault().unregister(this);
        super.onPause();
//        BusProvider.getInstance().unregister(this);
        Log.e(TAG, "onPause");
    }

    @Override
    public void onStart() {
        super.onStart();
//        BusProvider.getInstance().register(this);
        Log.e(TAG, "onStart");
    }

    @Override
    public void onStop() {
//        BusProvider.getInstance().unregister(this);
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        userProfFragment.setActivity(null);
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onUploadUserAvatarSuccess(UploadUserAvatarResponse response) {

    }

    @Override
    public void onUploadUserAvatarFailure(Throwable t) {

    }

//    @Override
//    public void onBackStackChanged() {
//        shouldDisplayHomeUp();
//    }
//
//    public void shouldDisplayHomeUp(){
//        //Enable Up button only  if there are entries in the back stack
//        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
//        getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
//    }
//
//    @Override
//    public boolean onSupportNavigateUp() {
//        //This method is called when the up button is pressed. Just the pop back stack.
//        getSupportFragmentManager().popBackStack();
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
