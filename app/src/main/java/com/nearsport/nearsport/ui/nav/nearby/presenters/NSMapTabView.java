package com.nearsport.nearsport.ui.nav.nearby.presenters;

import com.nearsport.nearsport.model.GameSkeleton;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.TeamSkeleton;
import com.nearsport.nearsport.presenter.View;

import java.util.List;

/**
 * Created by sguo on 6/15/16.
 */
public interface NSMapTabView extends View {
    <T extends GameSkeleton> void addGamesToMap(List<T> games);
    <T extends TeamSkeleton> void addTeamsToMap(List<T> teams);
    void addUsersToMap(List<PersonSkeleton> users);
    void clearMap();
    void focusMap();
}
