package com.nearsport.nearsport.ui.nav.viewholders;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 5/14/16.
 */
public class NSBaseListItemViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.listview_item_left_icon)
    public ImageView leftIcon;
    @Bind(R.id.listview_item_right_icon)
    public ImageView rightIcon;
    @Bind(R.id.listview_item_title)
    public TextView title;
    @Bind(R.id.listview_item_detail)
    public TextView detail;
    @Bind(R.id.listview_item_root_layout)
    public RelativeLayout rootLayout;
//    public LinearLayout rootLayout;

    public NSBaseListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
