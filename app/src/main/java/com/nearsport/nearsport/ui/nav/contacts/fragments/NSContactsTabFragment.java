package com.nearsport.nearsport.ui.nav.contacts.fragments;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.nearsport.nearsport.MainTabActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.http.rest.response.FindContactsResultUser;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.ui.nav.NSRecycleViewFragment;
import com.nearsport.nearsport.ui.nav.contacts.adapters.FindContactsResultListAdapter;
import com.nearsport.nearsport.ui.nav.contacts.adapters.NSNavContactsTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.contacts.comparators.NSContactComparator;
import com.nearsport.nearsport.ui.nav.contacts.presenters.NSContactsTabPresenter;
import com.nearsport.nearsport.ui.nav.contacts.presenters.NSContactsTabView;
import com.nearsport.nearsport.util.Constant;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Types.BoomType;
import com.nightonke.boommenu.Types.ButtonType;
import com.nightonke.boommenu.Types.DimType;
import com.nightonke.boommenu.Types.PlaceType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * Created by sguo on 5/15/16.
 */
public class NSContactsTabFragment
        extends NSRecycleViewFragment<NSNavContactsTabRecyclerAdapter, NSSelectionListItem, MainTabActivity>
        implements NSContactsTabView, SearchView.OnQueryTextListener, TextWatcher {
    public static final String TAG = NSContactsTabFragment.class.getSimpleName();
    @Bind(R.id.boom)
    protected BoomMenuButton boomMenuButton;

    protected NSContactsTabPresenter presenter;

    protected MaterialDialog searchDialog;
    protected MaterialDialog addContactDialog;
    protected ListView foundContactListView;
    protected FindContactsResultListAdapter foundContactsResultListAdapter;
    protected EditText findContactText;

    //    @Bind(R.id.main_toolbar)
//    protected Toolbar mainToolbar;
    public NSContactsTabFragment() {
        super();
        presenter = new NSContactsTabPresenter();
        presenter.attachView(this);
    }

    protected void initBoomMenu() {
        BoomMenuButton.Builder builder = new BoomMenuButton.Builder()
                .autoDismiss(false)
                .boom(BoomType.PARABOLA)
                .button(ButtonType.CIRCLE)
                .dim(DimType.DIM_0)
                .subButtonTextColor(R.color.material_white)
                .place(PlaceType.CIRCLE_2_1);
        for(int i=0; i<Constant.CONTACT_PAGE_BOOM_SUBBUTTON_TITLE.length; ++i) {
            builder.addSubButton(
                    getActivity(),
                    Constant.CONTACT_PAGE_BOOM_SUBBUTTON_DRAWABLES[i],
                    Constant.MAP_PAGE_BOOM_SUBBUTTON_COLOR_LOGINED,
                    getActivity().getString(Constant.CONTACT_PAGE_BOOM_SUBBUTTON_TITLE[i])
            );
        }
        builder.onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
            @Override
            public void onClick(int buttonIndex) {
                // return the index of the sub button clicked
                switch (buttonIndex) {
                    case Constant.CONTACT_PAGE_BOOM_SUBBUTTON_SEARCH: showFindPeopleDialog(); break;
                    case Constant.CONTACT_PAGE_BOOM_SUBBUTTON_ADD: showAddContactDialog(); break;
                }
                boomMenuButton.dismiss();
            }
        }).animator(new BoomMenuButton.AnimatorListener() {
            @Override
            public void toShow() {

            }

            @Override
            public void showing(float fraction) {

            }

            @Override
            public void showed() {

            }

            @Override
            public void toHide() {

            }

            @Override
            public void hiding(float fraction) {
                foundContactsResultListAdapter.clear();
                findContactText.setText("");
            }

            @Override
            public void hided() {
            }
        });
        builder.init(boomMenuButton);
    }

    protected void initDialogs() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .customView(R.layout.fragment_dialog_search, false);
        searchDialog = builder.build();
        View view = searchDialog.getCustomView();
        EditText searchText = (EditText) view.findViewById(R.id.dialog_search_text);
        searchText.addTextChangedListener(this);

        builder = new MaterialDialog.Builder(getContext())
                .customView(R.layout.fragment_dialog_add_contact, false);
        addContactDialog = builder.build();
        view = addContactDialog.getCustomView();
        Button findContactButton = (Button) view.findViewById(R.id.button_add_contact_find);
        findContactText = (EditText) view.findViewById(R.id.input_add_contact_username);
        foundContactsResultListAdapter = new FindContactsResultListAdapter(Glide.with(this), getContext(), new ArrayList<FindContactsResultUser>());
        foundContactListView = (ListView) view.findViewById(R.id.listview_find_contacts);
        foundContactListView.setAdapter(foundContactsResultListAdapter);

        findContactButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view1) {
                presenter.getUsersByUsername(findContactText.getText().toString());
            }
        });
    }

    protected void showFindPeopleDialog() {
//        searchDialogFragment.show(getFragmentManager(), "search contacts");
        searchDialog.show();
    }

    protected void showAddContactDialog() {
//        addContactDialogFragment.show(getFragmentManager(), "add contact");
        addContactDialog.show();
    }


    @Override
    public void initView(Bundle savedInstanceState) {
//        setHasOptionsMenu(true);
        super.initView(savedInstanceState);
        boomMenuButton.setVisibility(View.VISIBLE);
        initContactList();
        if(null == mAdapter) {
            mAdapter = new NSNavContactsTabRecyclerAdapter(Glide.with(this), itemList);
        }
        setSeparationDecoration(SeparatorDecoration.with(mActivity).colorFromResources(R.color.iron).width(Constant.LIST_VIEW_DEFAULT_SEPERATION_WIDTH).build());
        initBoomMenu();
        initDialogs();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private List<NSSelectionListItem> getContactsWithLetter(char letter) {
        List<NSSelectionListItem> contacts = new ArrayList<>();

        for (NSSelectionListItem item : itemList) {
            if (item.title.charAt(0) == letter) {
                contacts.add(item);
            }
        }

        return contacts;
    }


    @Override
    public void addContants(Map<Character, List<NSSelectionListItem>> contacts) {

    }

    @Override
    public void addFindContactsResult(List<FindContactsResultUser> results) {
        foundContactsResultListAdapter.addFoundContacts(results);
    }

    @Override
    public boolean onQueryTextChange(String query) {

        // getSectionsMap requires library version 1.0.4+
//        for (Section section : mAdapter.getSectionsMap().values()) {
//            if (section instanceof FilterableSection) {
//                ((FilterableSection)section).filter(query);
//            }
//        }
        mAdapter.notifyDataSetChanged();

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    protected void initContactList() {
        if(!itemList.isEmpty()) return;
        List<PersonSkeleton> contacts = (new Select()).from(PersonSkeleton.class).orderBy(Constant.USERNAME).execute();
        for(char ch : Constant.alphabet) {
            NSSelectionListItem item = new NSSelectionListItem();
            item.title = String.valueOf(ch);
            item.type = NSBaseListItem.ItemType.SECTION_HEADER;
            item.backgroudColor = ContextCompat.getColor(mActivity, R.color.iron);
            itemList.add(item);
        }
        for(PersonSkeleton personSkeleton : contacts) {
            NSSelectionListItem contactItem = new NSSelectionListItem();
            contactItem.title = personSkeleton.username;
            contactItem.leftIconUrl = personSkeleton.iconUrl;
            contactItem.leftIcon = Integer.parseInt(personSkeleton.iconUrl);
            itemList.add(contactItem);
        }
        NSContactComparator comparator = new NSContactComparator();
        Collections.sort(itemList, comparator);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        List<NSSelectionListItem> filteredList = filter(itemList, s.toString());
        mAdapter.animateTo(filteredList);
        mListView.scrollToPosition(0);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_contacts, menu);
//
//        final MenuItem item = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(this);
//    }

    private List<NSSelectionListItem> filter(List<NSSelectionListItem> contacts, String query) {
        query = query.toLowerCase();

        final List<NSSelectionListItem> filteredModelList = new ArrayList<>();
        for (NSSelectionListItem contact : contacts) {
            if(contact.type == NSBaseListItem.ItemType.REGULAR) {
                final String text = contact.title.toLowerCase();
                if (text.contains(query)) {
                    filteredModelList.add(contact);
                }
            }
        }
        return filteredModelList;
    }
}
