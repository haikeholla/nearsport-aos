package com.nearsport.nearsport.ui.nav.chat.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 5/15/16.
 */
public class NSChatItemViewHolder extends RecyclerView.ViewHolder  {
    @Bind(R.id.listview_chat_item_icon)
    public ImageView leftIcon;
    @Bind(R.id.listview_chat_item_title)
    public TextView title;
    @Bind(R.id.listview_chat_item_content)
    public TextView content;
    @Bind(R.id.listview_chat_item_tag)
    public TextView timestamp;
    @Bind(R.id.listview_chat_row_root_layout)
    public RelativeLayout rootLayout;
    @Bind(R.id.listview_chat_item_rightmost_icon)
    public ImageView rightMostIcon;

    public NSChatItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
