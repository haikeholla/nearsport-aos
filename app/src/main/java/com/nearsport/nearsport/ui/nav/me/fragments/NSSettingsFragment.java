package com.nearsport.nearsport.ui.nav.me.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.NSRecycleViewFragment;
import com.nearsport.nearsport.ui.nav.me.activities.NSMeSelectionLanderActivity;
import com.nearsport.nearsport.ui.nav.me.adapters.NSSettingsRecyclerAdapter;

import javax.inject.Inject;

/**
 * Created by sguo on 5/15/16.
 */
public class NSSettingsFragment extends NSRecycleViewFragment<NSSettingsRecyclerAdapter, NSSelectionListItem, NSMeSelectionLanderActivity> {
    public static final String TAG = NSSettingsFragment.class.getSimpleName();

    @Inject
    NearSportSessionManager sessionManager;

    public NSSettingsFragment() {
        super();
    }
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = super.onCreateView(inflater, container, savedInstanceState);
//        actionBar = mActivity.getSupportActionBar();
//        actionBar.setTitle("WTF");
//        return view;
//    }
    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        NearSportApplication.component().inject(this);
        if(itemList.isEmpty()) {
            itemList.add(createLogoutItem());
        }
        mAdapter = new NSSettingsRecyclerAdapter(Glide.with(this), itemList);
        setSeparationDecoration(SeparatorDecoration.with(mActivity).colorFromResources(R.color.iron).width(10).build());
    }

    protected NSSelectionListItem createLogoutItem() {
        NSSelectionListItem item = new NSSelectionListItem();
        item.setTitle("Log Out");
        item.setType(NSSelectionListItem.ItemType.REGULAR);
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                        .setMessage("Are you sure to logout?")
                        .setTitle("are you sure to logout?")
                        .setPositiveButton(R.string.com_nearsport_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // FIRE ZE MISSILES!
                                sessionManager.logoutUser();
                                NSSettingsFragment.this.mActivity.finish();
                            }
                        }).setNegativeButton(R.string.com_nearsport_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // FIRE ZE MISSILES!
                            }
                        })
                        .create();
                alertDialog.show();
            }
        });
        return item;
    }
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        initUI();
//    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = super.onCreateView(inflater, container, savedInstanceState);
//        return view;
//    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "Setting on pause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "Setting on resume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "Setting on destroy");
    }

    @Override
    public void onStop() {
        super.onDestroy();
        Log.e(TAG, "Setting on stop");
    }
}
