package com.nearsport.nearsport.ui.nav.me.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/13/16.
 */
public class NSGridPhotoViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.gridview_photo)
    public ImageView photo;

    public NSGridPhotoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
