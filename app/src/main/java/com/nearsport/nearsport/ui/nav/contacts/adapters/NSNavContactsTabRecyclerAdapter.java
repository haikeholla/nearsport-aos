package com.nearsport.nearsport.ui.nav.contacts.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.viewholders.NSBaseListItemViewHolder;

import java.util.List;

/**
 * Created by sguo on 5/15/16.
 */
public class NSNavContactsTabRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {

    public static interface Sectionizer<T> {
        String getSectionTitleForItem(T item);
    }

    private Sectionizer<String> alphabetSectionizer;

    public NSNavContactsTabRecyclerAdapter(RequestManager glide, List<NSSelectionListItem> itemList) {
        super(glide, itemList);
        alphabetSectionizer = new Sectionizer<String>() {
            @Override
            public String getSectionTitleForItem(String item) {
                return item.trim().substring(0,1).toUpperCase();
            }
        };
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        NSBaseListItem.ItemType itemType = NSBaseListItem.ItemType.valueOf(viewType);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
        NSBaseListItemViewHolder viewHolder = new NSBaseListItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSSelectionListItem listItem = this.mItemList.get(position);
        NSBaseListItemViewHolder viewHolder = (NSBaseListItemViewHolder) holder;
        viewHolder.title.setText(listItem.getTitle());
        switch (listItem.getType()) {
            case REGULAR:{
                viewHolder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, listItem.getRightIcon(), 0);
                mGlide.load(listItem.leftIconUrl).placeholder(listItem.getLeftIcon()).into(viewHolder.leftIcon);
                viewHolder.rootLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                viewHolder.rootLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });
                break;
            }
            case SECTION_HEADER: {
                viewHolder.rootLayout.setBackgroundColor(listItem.backgroudColor);
                viewHolder.title.setTextSize(16);
                viewHolder.leftIcon.setVisibility(View.GONE);
                viewHolder.rightIcon.setVisibility(View.GONE);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.setMargins(20,5,5,0);
                viewHolder.title.setLayoutParams(params);
                break;
            }
        }
    }

    public void animateTo(List<NSSelectionListItem> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<NSSelectionListItem> newModels) {
        for (int i = mItemList.size() - 1; i >= 0; i--) {
            final NSSelectionListItem model = mItemList.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<NSSelectionListItem> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final NSSelectionListItem model = newModels.get(i);
            if (!mItemList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<NSSelectionListItem> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final NSSelectionListItem model = newModels.get(toPosition);
            final int fromPosition = mItemList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public NSSelectionListItem removeItem(int position) {
        final NSSelectionListItem model = mItemList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, NSSelectionListItem model) {
        mItemList.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final NSSelectionListItem model = mItemList.remove(fromPosition);
        mItemList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}
