package com.nearsport.nearsport.ui.nav.contacts.comparators;

import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.util.Util;

import java.util.Comparator;
import java.util.regex.Pattern;

/**
 * Created by sguo on 7/16/16.
 */
public class NSContactComparator implements Comparator<NSSelectionListItem> {
    private static final String START_WITH_ALPH = "^[A-Za-z](.*)";
    private Pattern p = Pattern.compile(START_WITH_ALPH);
    @Override
    public int compare(NSSelectionListItem lhs, NSSelectionListItem rhs) {
        if(lhs.type == NSBaseListItem.ItemType.SECTION_HEADER && rhs.type == NSBaseListItem.ItemType.SECTION_HEADER) {
            if(lhs.title.equals("#")) {
                return 1;
            } else if(rhs.title.equals("#")) {
                return -1;
            } else {
                return lhs.title.compareTo(rhs.title);
            }
        } else if(lhs.type == NSBaseListItem.ItemType.SECTION_HEADER) {
            if(lhs.title.equals("#")) {
//                    if(rhs.title.matches(START_WITH_ALPH)) {
                if(p.matcher(rhs.title).matches()) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
//                    if(rhs.title.matches(START_WITH_ALPH)) {
                if(p.matcher(rhs.title).matches()) {
                    char ch = rhs.title.toUpperCase().charAt(0);
                    if(ch == lhs.title.charAt(0)) {
                        return -1;
                    }  else {
                        return lhs.title.charAt(0) - ch;
                    }
                } else {
                    return -1;
                }
            }

        } else if(rhs.type == NSBaseListItem.ItemType.SECTION_HEADER) {
            if(rhs.title.equals("#")) {
//                    if(lhs.title.matches(START_WITH_ALPH)) {
                    if(p.matcher(lhs.title).matches()) {
//                if(true) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                if(lhs.title.matches(START_WITH_ALPH)) {
                    char ch = lhs.title.toUpperCase().charAt(0);
                    if(ch == rhs.title.charAt(0)) {
                        return 1;
                    } else {
                        return ch - rhs.title.charAt(0);
                    }
                } else {
                    return 1;
                }
            }
        } else  {
            if(lhs.title.equalsIgnoreCase(rhs.title)) {
                return 0;
            } else {
                for(int i=0; i<lhs.title.length() && i<rhs.title.length(); ++i) {
                    char leftCh = lhs.title.charAt(i);
                    char rightCh = rhs.title.charAt(i);
                    if(leftCh == rightCh) continue;
                    else if(Util.isAlphabet(leftCh) && Util.isAlphabet(rightCh)) {
                        return leftCh - rightCh;
                    } else if(Util.isAlphabet(leftCh)) {
                        return -1;
                    } else if(Util.isAlphabet(rightCh)) {
                        return 1;
                    } else {
                        return leftCh - rightCh;
                    }
                }
                return lhs.title.length() > rhs.title.length() ? 1 : -1;
            }
        }
    }

    @Override
    public boolean equals(Object object) {
        return false;
    }
}