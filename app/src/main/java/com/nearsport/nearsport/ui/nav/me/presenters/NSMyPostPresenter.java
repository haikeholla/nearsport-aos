package com.nearsport.nearsport.ui.nav.me.presenters;

import com.activeandroid.query.Select;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.model.ui.NSMyPostListItem;
import com.nearsport.nearsport.presenter.BasePresenter;
import com.nearsport.nearsport.session.NearSportSessionManager;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sguo on 7/10/16.
 */
public class NSMyPostPresenter implements BasePresenter<NSMyPostView>, IUserService.GetUserPostListener {

    protected NSMyPostView view;

    protected IUserService userService;

    protected NearSportSessionManager sessionManager;

    public NSMyPostPresenter(IUserService userService, NearSportSessionManager sessionManager) {
        this.userService = userService;
        this.sessionManager = sessionManager;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(NSMyPostView view) {
        this.view = view;
    }

    public void loadMyPosts(int offset, int limit) {
        if(loadMyPostsFromDB(offset, limit) > 0) {
            return;
        } else {
            loadMyPostsFromSr(sessionManager.getUserId(), offset, limit, sessionManager.getSessionToken());
        }
    }

    public int loadMyPostsFromDB(int offset, int limit) {
        List<UserPost> myUserPosts = new Select().from(UserPost.class).orderBy("createdTime DESC").offset(offset).limit(limit).execute();
//        List<UserPost> myUserPosts = new Select().from(UserPost.class).offset(offset).limit(limit).execute();
        this.view.appendPosts(myUserPosts);
        return myUserPosts.size();
    }

    protected void loadMyPostsFromSr(long userId, int offset, int limit, String token) {
        userService.getUserPost(userId, offset, limit, token, this);
    }

    @Override
    public void onGetUserPostSuccess(List<UserPost> response) {
        for(UserPost userPost : response) {
            userPost.save();
            for(NSPhoto photo : userPost.photos()) {
                NSPhoto p = new NSPhoto(photo, userPost);
                p.save();
            }
        }
        this.view.addNewPosts(response);
    }

    @Override
    public void onGetUserPostFailure(Throwable t) {

    }

    public void loadNewPosts(Date newerThan) {
        List<UserPost> myPosts = new Select().from(UserPost.class).where("createdTime > ?", newerThan.getTime()).execute();
        if(myPosts == null || myPosts.size() == 0) return;
        this.view.addNewPosts(myPosts);
    }
}
