package com.nearsport.nearsport.ui.nav.me.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSChatListItem;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.chat.viewholders.NSChatItemViewHolder;
import com.nearsport.nearsport.ui.nav.viewholders.NSBaseListItemViewHolder;
import com.nearsport.nearsport.util.Util;

import java.util.List;

/**
 * Created by sguo on 5/14/16.
 */
public class NSNavMeTabRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {

    protected NSChatItemViewHolder userProfileViewHolder;

    public NSNavMeTabRecyclerAdapter(RequestManager glide, List<NSSelectionListItem> list) {
        super(glide, list);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        NSBaseListItem.ItemType itemType = NSBaseListItem.ItemType.valueOf(viewType);
        switch(itemType) {
            case SETTINGS:
            case REGULAR: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
                NSBaseListItemViewHolder holder = new NSBaseListItemViewHolder(view);
                return holder;
            }
            case USER_PROF: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_chat_item_row, null);
                NSChatItemViewHolder holder = new NSChatItemViewHolder(view);
                return holder;
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSSelectionListItem listItem = this.mItemList.get(position);
        switch(listItem.getType()) {
            case REGULAR:
            case SETTINGS: {
                final NSBaseListItemViewHolder itemViewHolder = (NSBaseListItemViewHolder) holder;
                itemViewHolder.leftIcon.setImageResource(listItem.leftIcon);
                itemViewHolder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, listItem.getRightIcon(), 0);
                itemViewHolder.title.setText(listItem.getTitle());
                itemViewHolder.rootLayout.setOnClickListener(listItem.onClickListener);
                break;
            }
//            case REGULAR: {
//                final NSBaseListItemViewHolder itemViewHolder = (NSBaseListItemViewHolder) holder;
//                if(listItem.leftIcon > 0) {
//                    itemViewHolder.title.setCompoundDrawablesWithIntrinsicBounds(listItem.leftIcon, 0, listItem.getRightIcon(), 0);
//                }
//                itemViewHolder.title.setText(listItem.getTitle());
//                break;
//            }
            case USER_PROF: {
                userProfileViewHolder = (NSChatItemViewHolder) holder;
                final NSChatListItem chatListItem = (NSChatListItem) listItem;
                userProfileViewHolder.title.setText(chatListItem.lastMsger);
                userProfileViewHolder.content.setText(chatListItem.lastMsg);
                userProfileViewHolder.rootLayout.setOnClickListener(chatListItem.onClickListener);
                if(!Util.stringIsNullOrEmpty(listItem.leftIconUrl)) {
                    mGlide.load(listItem.leftIconUrl)
                            .dontAnimate()
                            .centerCrop()
                            .placeholder(R.drawable.user_icon_placeholder_48px)
                            .error(R.drawable.user_icon_placeholder)
                            .into(userProfileViewHolder.leftIcon);
                }
                if(listItem.rightMostIcon > 0) {
                    mGlide.load(listItem.rightMostIcon)
                            .dontAnimate()
                            .centerCrop()
//                        .placeholder(R.drawable.user_icon_placeholder_48px)
//                        .error(R.drawable.user_icon_placeholder)
                            .into(userProfileViewHolder.rightMostIcon);
                }
                break;
            }
        }
    }

//    public void updateUserAvatar(String imageUrl) {
//        if(userProfileViewHolder == null || Util.stringIsNullOrEmpty(imageUrl)) return;
//        mGlide.load(imageUrl)
//                .dontAnimate()
//                .centerCrop()
//                .error(R.drawable.user_icon_placeholder)
//                .into(userProfileViewHolder.leftIcon);
//    }
}
