package com.nearsport.nearsport.ui.nav.discover.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.viewholders.NSBaseListItemViewHolder;

import java.util.List;

/**
 * Created by sguo on 5/15/16.
 */
public class NSNavDiscoverTabRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {

    public NSNavDiscoverTabRecyclerAdapter(RequestManager glide, List<NSSelectionListItem> list) {
        super(glide, list);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        NSBaseListItem.ItemType itemType = NSBaseListItem.ItemType.valueOf(viewType);
        switch(itemType) {
            case REGULAR: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
                NSBaseListItemViewHolder viewHolder = new NSBaseListItemViewHolder(view);
                return viewHolder;
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSSelectionListItem listItem = this.mItemList.get(position);

        switch(listItem.getType()) {
            case REGULAR: {
                final NSBaseListItemViewHolder itemViewHolder = (NSBaseListItemViewHolder) holder;
                itemViewHolder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, listItem.getRightIcon(), 0);
                mGlide.load(listItem.leftIconUrl).placeholder(listItem.getLeftIcon()).into(itemViewHolder.leftIcon);
                itemViewHolder.title.setText(listItem.getTitle());
                //itemViewHolder.content.setText(listItem.getDisplayContent(20));
                itemViewHolder.rootLayout.setOnClickListener(listItem.onClickListener);
                break;
            }
            case USER_PROF: {
                break;
            }
        }
    }
}
