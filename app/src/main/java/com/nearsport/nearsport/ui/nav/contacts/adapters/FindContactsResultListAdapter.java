package com.nearsport.nearsport.ui.nav.contacts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.http.rest.response.FindContactsResultUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 6/21/16.
 */
public class FindContactsResultListAdapter extends ArrayAdapter<FindContactsResultUser> {
    private final Context context;
    private final List<FindContactsResultUser> values;
    private RequestManager mGlide;

    @Bind(R.id.listview_find_contacts_result_user_icon)
    ImageView icon;

    @Bind(R.id.listview_find_contacts_result_username)
    TextView username;

    @Bind(R.id.listview_find_contacts_result_user_add)
    Button addButton;

    public FindContactsResultListAdapter(RequestManager requestManager, Context context, List<FindContactsResultUser> values) {
        super(context, -1, values);
        this.mGlide = requestManager;
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.listview_find_contacts_result_row, parent, false);
        ButterKnife.bind(this, rowView);
        final FindContactsResultUser contact = values.get(position);
        username.setText(contact.username);
        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Toast.makeText(FindContactsResultListAdapter.this.context, contact.username, Toast.LENGTH_SHORT).show();
            }
        });
        return rowView;
    }

    public void clear() {
        values.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        return values.size();
    }

    public void addFoundContacts(List<FindContactsResultUser> foundContacts) {
        values.addAll(foundContacts);
        notifyDataSetChanged();
    }
}
