package com.nearsport.nearsport.ui.nav.me.presenters;

import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.presenter.View;

import java.util.List;

/**
 * Created by sguo on 7/10/16.
 */
public interface NSMyPostView extends View {
    void displayMyPosts();
    void appendPosts(List<UserPost> userPosts);
    void addNewPosts(List<UserPost> userPosts);
}
