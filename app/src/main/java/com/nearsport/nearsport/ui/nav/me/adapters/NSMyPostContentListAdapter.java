package com.nearsport.nearsport.ui.nav.me.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.me.viewholders.NSMyPostContentListHolder;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.util.List;

/**
 * Created by sguo on 7/12/16.
 */
public class NSMyPostContentListAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {

    protected final Context context;

    public NSMyPostContentListAdapter(Context context, RequestManager glide, List<NSSelectionListItem> itemList) {
        super(glide, itemList);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_my_post_row, null);
//        view.setBackgroundColor(ContextCompat.getColor(context, R.color.iron));
        NSMyPostContentListHolder viewHolder = new NSMyPostContentListHolder(view);
//        viewHolder.coverPhoto.setBackgroundResource(R.drawable.my_custom_background);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSSelectionListItem item = mItemList.get(position);
        NSMyPostContentListHolder contentListHolder = (NSMyPostContentListHolder)holder;
        switch (item.type) {
            case CREATE_POST:
                mGlide.load(item.leftIcon)
                        .override(200, 200)
                        .dontAnimate()
                        .centerCrop()
                        .error(R.drawable.user_icon_placeholder)
                        .into(contentListHolder.coverPhoto);
                break;
            case REGULAR:

                List<NSPhoto> photos = item.userPost.photos();
                if(photos.size() > 0) {
                    NSPhoto photo = photos.get(0);
                    if (!Util.stringIsNullOrEmpty(photo.localPath)) {
                        mGlide.load(photo.localPath)
                                .override(200, 200)
                                .dontAnimate()
                                .centerCrop()
                                .placeholder(R.drawable.ic_rectangle_48)
                                .error(R.drawable.ic_rectangle_48)
                                .into(contentListHolder.coverPhoto);
                    } else {
                        mGlide.load(Constant.BASE_URL+photo.url)
                                .override(200, 200)
                                .dontAnimate()
                                .centerCrop()
                                .placeholder(R.drawable.ic_rectangle_48)
                                .error(R.drawable.ic_rectangle_48)
                                .into(contentListHolder.coverPhoto);
                    }
                }
                if(item.userPost.photos().size() > 0) {
                    contentListHolder.footage.setText(String.format("%d photos", item.userPost.photos().size()));
                }
                if(!Util.stringIsNullOrEmpty(item.userPost.content)) {
                    contentListHolder.content.setText(item.userPost.content);
                }
                break;
        }
        contentListHolder.coverPhoto.setOnClickListener(item.onClickListener);
    }
}
