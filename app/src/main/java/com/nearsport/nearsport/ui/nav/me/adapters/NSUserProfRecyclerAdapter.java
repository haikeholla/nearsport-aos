package com.nearsport.nearsport.ui.nav.me.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.viewholders.NSBaseListItemViewHolder;
import com.nearsport.nearsport.util.Util;

import java.util.List;

/**
 * Created by sguo on 5/21/16.
 */
public class NSUserProfRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {
    private static final String TAG = NSUserProfRecyclerAdapter.class.getSimpleName();

    private NSBaseListItemViewHolder userAvatarItemViewHolder;

    public NSUserProfRecyclerAdapter(RequestManager glide, List<NSSelectionListItem> itemList) {
        super(glide, itemList);
//        BusProvider.getInstance().register(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NSBaseListItem.ItemType itemType = NSBaseListItem.ItemType.valueOf(viewType);
        NSBaseListItemViewHolder holder = null;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
        holder = new NSBaseListItemViewHolder(view);
//        switch (itemType) {
//            case USER_PROF: {
//                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
//                holder = new NSBaseListItemViewHolder(view);
//                break;
//            }
//        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSBaseListItemViewHolder itemViewHolder = (NSBaseListItemViewHolder) holder;
        NSSelectionListItem listItem = this.mItemList.get(position);
        switch(listItem.type) {
            case REGULAR:
                itemViewHolder.title.setText(listItem.title);
                itemViewHolder.detail.setText(listItem.detail);
                break;
            case USER_PROF:
                itemViewHolder.title.setText(listItem.title);
                if(listItem.rightIcon > 0) {
                    itemViewHolder.rightIcon.setImageResource(listItem.rightIcon);
                } else if(!Util.stringIsNullOrEmpty(listItem.rightIconUrl)) {
                    mGlide.load(listItem.rightIconUrl)
                            .dontAnimate()
                            .centerCrop()
                            .placeholder(R.drawable.user_icon_placeholder_48px)
                            .error(R.drawable.user_icon_placeholder_48px)
                            .into(itemViewHolder.rightIcon);
                }
                break;
        }
//        if(!Util.stringIsNullOrEmpty(listItem.title)) {
//            itemViewHolder.title.setText(listItem.title);
//        }
//        if(!Util.stringIsNullOrEmpty(listItem.detail)) {
//            itemViewHolder.detail.setText(listItem.detail);
//        }
//        if(listItem.leftIcon > 0) {
//            itemViewHolder.leftIcon.setImageResource(listItem.leftIcon);
//        } else if(!Util.stringIsNullOrEmpty(listItem.leftIconUrl)) {
//            mGlide.load(listItem.leftIconUrl)
//                    .dontAnimate()
//                    .centerCrop()
//                    .error(R.drawable.user_icon_placeholder_48px)
//                    .into(itemViewHolder.leftIcon);
//        }
//
//        if(listItem.rightIcon > 0) {
//            itemViewHolder.rightIcon.setImageResource(listItem.rightIcon);
//        } else if(!Util.stringIsNullOrEmpty(listItem.rightIconUrl)) {
//            mGlide.load(listItem.rightIconUrl)
//                    .dontAnimate()
//                    .centerCrop()
//                    .error(R.drawable.user_icon_placeholder_48px)
//                    .into(itemViewHolder.rightIcon);
//        }
        if(null != listItem.onClickListener) {
            itemViewHolder.rootLayout.setOnClickListener(listItem.onClickListener);
        }
//        switch (listItem.type) {
//            case USER_PROF: {
//                userAvatarItemViewHolder.title.setText(listItem.title);
//                userAvatarItemViewHolder.rightIcon.setBackgroundResource(R.drawable.image_border);
//                mGlide.load(listItem.rightIconUrl).dontAnimate()
//                        .centerCrop()
//                        .error(R.drawable.user_icon_placeholder_48px)
//                        .into(userAvatarItemViewHolder.rightIcon);
//                userAvatarItemViewHolder.rootLayout.setOnClickListener(listItem.onClickListener);
//                break;
//            }
//            default: {
//                userAvatarItemViewHolder.title.setText(listItem.title);
//                userAvatarItemViewHolder.detail.setText(listItem.detail);
//            }
//        }
    }

    @Override
    public int getItemViewType(int position) {
        NSSelectionListItem item = mItemList.get(position);
        return item.type.code;
    }

//    @com.squareup.otto.Subscribe
//    public void onUserAvatarSelectedEvent(UserAvatarSelectedEvent event) {
//        if(event == null || Util.stringIsNullOrEmpty(event.imageUrl) || userAvatarItemViewHolder == null ) return;
//        Log.e(TAG, "*****_" + event.imageUrl);
//        Glide.with(mContext)
//                .load(event.imageUrl)
//                .dontAnimate()
//                .centerCrop()
//                .error(R.drawable.user_icon_placeholder_48px)
//                .into(userAvatarItemViewHolder.rightIcon);
//    }

    public void updateUserAvatar(String imageUrl) {
        if(userAvatarItemViewHolder == null || Util.stringIsNullOrEmpty(imageUrl)) return;
        mGlide.load(imageUrl)
                .dontAnimate()
                .centerCrop()
                .error(R.drawable.user_icon_placeholder_48px)
                .into(userAvatarItemViewHolder.rightIcon);
    }

//    public void onUserAvatarSelectedEvent(String imageUrl) {
//        if(Util.stringIsNullOrEmpty(imageUrl) || userAvatarItemViewHolder == null ) return;
//        Log.e(TAG, "*****_" + imageUrl);
//        Glide.with(mContext)
//                .load(imageUrl)
//                .dontAnimate()
//                .centerCrop()
//                .error(R.drawable.user_icon_placeholder_48px)
//                .into(userAvatarItemViewHolder.rightIcon);
//    }

}
