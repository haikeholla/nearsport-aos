package com.nearsport.nearsport.ui.nav.me.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.model.UserPost;
import com.nearsport.nearsport.model.ui.NSMyPostListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.model.ui.SeparatorDecoration;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.me.activities.NSUserPostLanderActivity;
import com.nearsport.nearsport.ui.photoviewer.NSPhotoViewActivity;
import com.nearsport.nearsport.ui.nav.me.activities.NSMeSelectionLanderActivity;
import com.nearsport.nearsport.ui.nav.me.adapters.NSMyPostsRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.me.presenters.NSMyPostPresenter;
import com.nearsport.nearsport.ui.nav.me.presenters.NSMyPostView;
import com.nearsport.nearsport.util.Constant;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/10/16.
 */
public class NSMyPostsFragment extends Fragment implements NSMyPostView {

    private static final String TAG = NSMyPostsFragment.class.getSimpleName();
    protected static final String KEY_LAYOUT_MANAGER = "layoutManager";
    protected static final int SPAN_COUNT = 2;

    @Inject
    NearSportSessionManager sessionManager;

    protected NSMyPostPresenter presenter;

    protected Activity mActivity;
    @Inject
    UserService userService;

    @Bind(R.id.my_post_recycler_listview)
    RecyclerView myPostsListRecyclerView;

    @Bind(R.id.moment_header_cover)
    ImageView coverImage;

    @Bind(R.id.userAvatar)
    ImageView userAvatar;

    @Bind(R.id.userNick)
    TextView username;

    @Bind(R.id.userMood)
    TextView userMood;

    protected NSMyPostsRecyclerAdapter myPostAdapter;

    protected List<NSMyPostListItem> itemList = new ArrayList<>();

    protected NSMyPostListItem createPostItem;

    protected RecyclerView.LayoutManager mLayoutManager;
    protected LayoutManagerType currentLayoutManagerType;

    protected enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NearSportApplication.component().inject(this);
        Log.e(TAG, "onCreate");
        presenter = new NSMyPostPresenter(userService, sessionManager);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null == container) return null;
        View view = inflater.inflate(R.layout.fragment_my_posts, container, false);
        ButterKnife.bind(this, view);
        currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            currentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(currentLayoutManagerType);
        initView(savedInstanceState);
        return view;
    }

    protected void initHeaderView() {
//        coverImage
        username.setText(sessionManager.getUserStringInfo(Constant.USERNAME));
        userMood.setText(sessionManager.getUserStringInfo(Constant.MOOD));

        Glide.with(mActivity).load(sessionManager.getMomentCoverImage())
                .dontAnimate()
                .centerCrop()
                .placeholder(R.drawable.moment_cover_image_placeholder)
                .error(R.drawable.moment_cover_image_placeholder)
                .into(coverImage);

        Glide.with(mActivity).load(sessionManager.getUserAvatar())
                .dontAnimate()
                .centerCrop()
                .placeholder(R.drawable.user_icon_placeholder_48px)
                .error(R.drawable.user_icon_placeholder)
                .into(userAvatar);
    }

    protected void initBodyListView() {
        if(itemList.size() == 1) {
            NSMyPostListItem postListItem = itemList.get(0);
            if(postListItem.contentListItemList.size() > 1) {
                UserPost post = postListItem.contentListItemList.get(1).userPost;
                presenter.loadNewPosts(post.createdTime);
            } else {
                presenter.loadMyPosts(0, Constant.MOMENT_POSTS_PER_LOAD);
            }
        } else if(itemList.size() > 1) {
            NSMyPostListItem postListItem = itemList.get(0);
            if(postListItem.contentListItemList.size() > 1) {
                UserPost post = postListItem.contentListItemList.get(1).userPost;
                presenter.loadNewPosts(post.createdTime);
            } else {
                postListItem = itemList.get(1);
                UserPost post = postListItem.contentListItemList.get(0).userPost;
                presenter.loadNewPosts(post.createdTime);
            }
        } else {
            createPostItem = createTodayPostItem();
            itemList.add(createPostItem);
            presenter.loadMyPosts(0, Constant.MOMENT_POSTS_PER_LOAD);
            myPostAdapter = new NSMyPostsRecyclerAdapter(mActivity, Glide.with(this), itemList);
        }
        myPostsListRecyclerView.setAdapter(myPostAdapter);
        myPostsListRecyclerView.addItemDecoration((SeparatorDecoration.with(mActivity).colorFromResources(R.color.white).width(30).build()));
    }

    protected void initView(Bundle savedInstanceState) {
        initHeaderView();
        initBodyListView();
    }

    public void addNewPosts(List<UserPost> userPosts) {
        for(final UserPost userPost : userPosts) {
            NSMyPostListItem postItem = itemList.get(1);
            NSSelectionListItem listItem = postItem.contentListItemList.get(0);
            NSSelectionListItem newPostEntry = new NSSelectionListItem();
            newPostEntry.userPost = userPost;
            newPostEntry.onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, NSUserPostLanderActivity.class);
                    intent.putParcelableArrayListExtra(Constant.INTENT_PARAM_USER_POST, userPost.photos);
                    mActivity.startActivity(intent);
                }
            };
            if(listItem.userPost.createdTime == userPost.createdTime) {
                postItem.contentListItemList.add(0, newPostEntry);
            } else {
                NSMyPostListItem newDayPostItem = new NSMyPostListItem();
                newDayPostItem.contentListItemList = new ArrayList<>();
                newDayPostItem.contentListItemList.add(newPostEntry);
                itemList.add(1, newDayPostItem);
            }
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myPostAdapter.notifyDataSetChanged();
            }
        });
    }

    protected NSMyPostListItem createTodayPostItem() {
        NSMyPostListItem todayPostItem = new NSMyPostListItem();
        todayPostItem.title = new Date(System.currentTimeMillis()).toString();
        NSSelectionListItem contentListItem = new NSSelectionListItem();
        contentListItem.type = NSSelectionListItem.ItemType.CREATE_POST;
        contentListItem.leftIcon = R.drawable.camera_con;
        contentListItem.onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(NSMyPostsFragment.this.mActivity, ImagePickerActivity.class);
                NSMyPostsFragment.this.mActivity.startActivityForResult(intent, NSMeSelectionLanderActivity.CREATE_POST);
            }
        };
        todayPostItem.contentListItemList.add(contentListItem);
        return todayPostItem;
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (myPostsListRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) myPostsListRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                currentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        myPostsListRecyclerView.setLayoutManager(mLayoutManager);
        myPostsListRecyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    public void displayMyPosts() {

    }

    @Override
    public void appendPosts(List<UserPost> userPosts) {
        for(UserPost userPost : userPosts) {
            appendPost(userPost);
        }
    }

    private void appendPost(final UserPost userPost) {
        final StringBuilder date = new StringBuilder();
        NSSelectionListItem newPostEntry = new NSSelectionListItem();
        if(itemList.size() == 0) {
            NSMyPostListItem postListItem = new NSMyPostListItem();
            postListItem.title = userPost.createdTime.toString();
            postListItem.contentListItemList = new ArrayList<>();
            newPostEntry.userPost = userPost;
            postListItem.contentListItemList.add(newPostEntry);
            itemList.add(postListItem);
        } else {
            NSMyPostListItem lastPostListItem = itemList.get(itemList.size() - 1);
            if(lastPostListItem.title.equals(userPost.createdTime.toString())) {
                date.append(lastPostListItem.title);
                newPostEntry.userPost = userPost;
                lastPostListItem.contentListItemList.add(newPostEntry);
            } else {
                NSMyPostListItem postListItem = new NSMyPostListItem();
                postListItem.title = userPost.createdTime.toString();
                postListItem.contentListItemList = new ArrayList<>();
                newPostEntry.userPost = userPost;
                postListItem.contentListItemList.add(newPostEntry);
                itemList.add(postListItem);
            }
        }
        newPostEntry.userPost = userPost;
        newPostEntry.onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, NSUserPostLanderActivity.class);
//                intent.putExtra(Constant.INTENT_PARAM_USER_POST, userPost);
                intent.putExtra(Constant.INTENT_PARAM_USER_POST, userPost.getId());
                mActivity.startActivity(intent);
            }
        };
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(null != myPostAdapter) {
                    myPostAdapter.notifyDataSetChanged();
                    myPostAdapter.notifyInnerDataSetChanged(date.toString());
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    public void setActivity(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }
}
