package com.nearsport.nearsport.ui.nav.me.presenters;

import com.nearsport.nearsport.model.NSPhoto;
import com.nearsport.nearsport.presenter.View;

/**
 * Created by sguo on 8/7/16.
 */
public interface NSUserPostView extends View {
    void showCommentsAndLikes(NSPhoto photo);
    void updateCommentsCount(int c);
    void updateLikesCount(int c);
}
