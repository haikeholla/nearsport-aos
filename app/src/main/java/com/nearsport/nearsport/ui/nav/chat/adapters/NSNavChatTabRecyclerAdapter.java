package com.nearsport.nearsport.ui.nav.chat.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSChatListItem;
import com.nearsport.nearsport.model.ui.NSBaseListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.chat.viewholders.NSChatItemViewHolder;

import java.util.List;

/**
 * Created by sguo on 5/15/16.
 */
public class NSNavChatTabRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSChatListItem> {

    public NSNavChatTabRecyclerAdapter(RequestManager glide, List<NSChatListItem> itemList) {
        super(glide, itemList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        NSBaseListItem.ItemType itemType = NSBaseListItem.ItemType.valueOf(viewType);
        switch(itemType) {
            case CHAT: {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_chat_item_row, null);
                NSChatItemViewHolder holder = new NSChatItemViewHolder(view);

                holder.rootLayout.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {

                    }
                });
                return holder;
            }
        }
        return null;
    }

//    if(chatListItem != null) {
////                    Picasso.with(context).load(listItem.getDrawableRes()).into(holder.iconView);
//                    Glide.with(context).load(listItem.getDrawableRes()).into(holder.iconView);
//
////                } else {
////                    Picasso.with(context).load(listItem.getIcon())
////                .error(R.drawable.placeholder)
////                .placeholder(R.drawable.placeholder)
////                            .into(holder.iconView);
////                }
//                    holder.titleView.setText(listItem.getTitle());
//                    holder.contentView.setText(chatListItem.getDisplayContent(20));
//                }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSChatListItem listItem = this.mItemList.get(position);
        switch(listItem.getType()) {
            case CHAT: {
                final NSChatItemViewHolder itemViewHolder = (NSChatItemViewHolder) holder;
                mGlide.load(listItem.leftIconUrl).placeholder(R.drawable.user_icon_placeholder).into(itemViewHolder.leftIcon);
                itemViewHolder.title.setText(listItem.getTitle());
                itemViewHolder.content.setText(listItem.getDisplayContent(20));
                itemViewHolder.rootLayout.setOnClickListener(listItem.onClickListener);
                break;
            }
            case USER_PROF: {
                break;
            }
        }

    }
}
