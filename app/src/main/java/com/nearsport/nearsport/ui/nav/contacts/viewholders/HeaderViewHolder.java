package com.nearsport.nearsport.ui.nav.contacts.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 6/18/16.
 */
public class HeaderViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.listview_item_title)
    public TextView title;

    public HeaderViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);
    }
}
