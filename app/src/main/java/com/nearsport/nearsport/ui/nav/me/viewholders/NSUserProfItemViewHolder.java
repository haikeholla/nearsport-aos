package com.nearsport.nearsport.ui.nav.me.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by sguo on 5/15/16.
 */
public class NSUserProfItemViewHolder extends RecyclerView.ViewHolder {


    public NSUserProfItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
