package com.nearsport.nearsport.ui.nav.me.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSChatListItem;
import com.nearsport.nearsport.model.ui.NSSelectionListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.chat.viewholders.NSChatItemViewHolder;
import com.nearsport.nearsport.ui.nav.viewholders.NSBaseListItemViewHolder;
import com.nearsport.nearsport.util.Util;

import java.util.List;

/**
 * Created by sguo on 5/15/16.
 */
public class NSSettingsRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSSelectionListItem> {

    public NSSettingsRecyclerAdapter(RequestManager glide, List<NSSelectionListItem> itemList) {
        super(glide, itemList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NSSelectionListItem.ItemType itemType = NSSelectionListItem.ItemType.valueOf(viewType);
        RecyclerView.ViewHolder holder = null;
        switch(itemType) {
            case USER_PROF: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_chat_item_row, null);
                holder = new NSChatItemViewHolder(view);
                break;
            }
            case REGULAR: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item_row, null);
                holder = new NSBaseListItemViewHolder(view);
                break;
            }
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSSelectionListItem listItem = this.mItemList.get(position);

        switch (listItem.type) {
            case USER_PROF: {
                final NSChatItemViewHolder itemViewHolder = (NSChatItemViewHolder) holder;
                final NSChatListItem chatListItem = (NSChatListItem) listItem;
                mGlide.load(listItem.leftIconUrl).placeholder(R.drawable.user_icon_placeholder).into(itemViewHolder.leftIcon);
                itemViewHolder.title.setText(chatListItem.lastMsger);
                itemViewHolder.content.setText(chatListItem.lastMsg);
                itemViewHolder.rootLayout.setOnClickListener(listItem.getOnClickListener());
                break;
            }
            case REGULAR: {
                final NSBaseListItemViewHolder itemViewHolder = (NSBaseListItemViewHolder) holder;
                itemViewHolder.title.setText(listItem.getTitle());
                itemViewHolder.rootLayout.setOnClickListener(listItem.getOnClickListener());
                if(Util.stringIsNullOrEmpty(listItem.leftIconUrl)) {
                    itemViewHolder.leftIcon.setVisibility(View.GONE);
                }
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        NSSelectionListItem item = mItemList.get(position);
        return item.type.code;
    }
}
