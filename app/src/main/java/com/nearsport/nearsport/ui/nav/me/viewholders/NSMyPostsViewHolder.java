package com.nearsport.nearsport.ui.nav.me.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/10/16.
 */
public class NSMyPostsViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.my_post_item_title)
    public TextView title;

    @Bind(R.id.my_post_item_content)
    public RecyclerView listView;

    public NSMyPostsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
