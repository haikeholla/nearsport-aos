package com.nearsport.nearsport.ui.nav.nearby.presenters;

import com.nearsport.nearsport.http.rest.request.CreateCasualGameRequest;
import com.nearsport.nearsport.http.rest.request.FetchGamesRequest;
import com.nearsport.nearsport.http.rest.response.CreateCasualGameResponse;
import com.nearsport.nearsport.http.rest.response.FetchGamesResponse;
import com.nearsport.nearsport.http.rest.service.IGameService;
import com.nearsport.nearsport.presenter.BasePresenter;

import javax.inject.Inject;

/**
 * Created by sguo on 6/15/16.
 */
public class NSMapTabPresenter implements BasePresenter<NSMapTabView>, IGameService.FetchGamesListener, IGameService.CreateGameListener {
    protected NSMapTabView mapTabView;

    public IGameService gameService;

    public NSMapTabPresenter(IGameService gameService) {
        this.gameService = gameService;
    }
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    public void fetchGames(FetchGamesRequest request) {
        gameService.fetchGames(request, this);
    }

    public void createGame(CreateCasualGameRequest request) {
        gameService.createGame(request, this);
    }

    @Override
    public void attachView(NSMapTabView view) {
        mapTabView = view;
    }

    @Override
    public void onFetchGamesSuccess(FetchGamesResponse response) {
        if(null == response) return;
        this.mapTabView.addGamesToMap(response.content);
    }

    @Override
    public void onFetchGamesFailure(Throwable t) {

    }

    @Override
    public void onCreateGameSuccess(CreateCasualGameResponse response) {
    }

    @Override
    public void onCreateGameFailure(Throwable t) {

    }
}
