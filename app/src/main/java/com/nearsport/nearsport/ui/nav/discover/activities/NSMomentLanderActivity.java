package com.nearsport.nearsport.ui.nav.discover.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import net.datafans.android.timeline.controller.TimelineViewController;

/**
 * Created by sguo on 7/6/16.
 */
public class NSMomentLanderActivity extends TimelineViewController {

    private static final String TAG = NSMomentLanderActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        addItems();

        super.onCreate(savedInstanceState);

        Log.d(TAG, "init timeline");

        setHeader();

    }

    @Override
    protected String getNavTitle() {
        return "Moments";
    }


    private void setHeader() {
        String coverUrl = String.format("http://file-cdn.datafans.net/temp/12.jpg_%dx%d.jpeg", coverWidth, coverHeight);
        setCover(coverUrl);


        String userAvatarUrl = String.format("http://file-cdn.datafans.net/avatar/1.jpeg_%dx%d.jpeg", userAvatarSize, userAvatarSize);
        setUserAvatar(userAvatarUrl);


        setUserNick("Allen");

        setUserSign("梦想还是要有的 万一实现了呢");

        setUserId(123456);
    }

    private void addItems() {

    }

    @Override
    protected void onCommentCreate(long itemId, long commentId, String text) {

    }

    @Override
    protected void onLikeCreate(long itemId) {

    }

    @Override
    public void onRefresh() {
        super.onRefresh();

        onEnd();

    }

    @Override
    public void onLoadMore() {
        super.onLoadMore();


//        addItem(textImageItem3);

        onEnd();

    }

    @Override
    protected void onUserClick(int userId) {
        Log.d(TAG, "user-click: " + userId);

        Intent intent = new Intent(this, NSUserMomentFeedActivity.class);
        startActivity(intent);
    }
}
