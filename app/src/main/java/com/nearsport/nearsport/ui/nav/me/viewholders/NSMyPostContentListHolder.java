package com.nearsport.nearsport.ui.nav.me.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearsport.nearsport.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sguo on 7/12/16.
 */
public class NSMyPostContentListHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.listview_my_post_row_image)
    public ImageView coverPhoto;
    @Bind(R.id.listview_my_post_row_content)
    public TextView content;
    @Bind(R.id.listview_my_post_row_footage)
    public TextView footage;

    public NSMyPostContentListHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
