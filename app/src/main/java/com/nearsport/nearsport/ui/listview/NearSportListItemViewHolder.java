package com.nearsport.nearsport.ui.listview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearsport.nearsport.R;

/**
 * Created by sguo on 5/4/16.
 */
public class NearSportListItemViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public ImageView iconView;
    public TextView contentView;

    public NearSportListItemViewHolder(View view) {
        super(view);
        this.titleView = (TextView) view.findViewById(R.id.listview_item_title);
//        this.iconView  = (ImageView) view.findViewById(R.id.listview_item_icon);
//        this.contentView = (TextView) view.findViewById(R.id.listview_item_content);
    }
}
