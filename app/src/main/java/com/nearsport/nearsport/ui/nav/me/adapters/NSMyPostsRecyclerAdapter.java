package com.nearsport.nearsport.ui.nav.me.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.model.ui.NSMyPostListItem;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;
import com.nearsport.nearsport.ui.nav.me.viewholders.NSMyPostsViewHolder;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sguo on 7/10/16.
 */
public class NSMyPostsRecyclerAdapter extends NSBaseNavTabRecyclerAdapter<NSMyPostListItem> {

    private Context context;

    private Map<String, NSMyPostContentListAdapter> innerAdapters = new HashMap<>();

    public NSMyPostsRecyclerAdapter(Context context, RequestManager glide, List<NSMyPostListItem> itemList) {
        super(glide, itemList);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_post_item, null);
        NSMyPostsViewHolder viewHolder = new NSMyPostsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NSMyPostListItem item = mItemList.get(position);
        NSMyPostsViewHolder viewHolder = (NSMyPostsViewHolder) holder;
        viewHolder.title.setText(item.title);
        NSMyPostContentListAdapter postContentListAdapter = new NSMyPostContentListAdapter(context, mGlide, item.contentListItemList);

        viewHolder.listView.setAdapter(postContentListAdapter);
        viewHolder.listView.setLayoutManager(new LinearLayoutManager(context));
        innerAdapters.put(item.title, postContentListAdapter);
    }

    public void notifyInnerDataSetChanged(String date) {
        if(innerAdapters.containsKey(date)) {
            innerAdapters.get(date).notifyDataSetChanged();
        }
    }
}
