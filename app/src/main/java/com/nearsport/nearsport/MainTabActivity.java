package com.nearsport.nearsport;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.model.PersonSkeleton;
import com.nearsport.nearsport.permission.NSPermissionManager;
import com.nearsport.nearsport.permission.PermissionUtils;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.ui.nav.chat.fragments.NSChatTabFragment;
import com.nearsport.nearsport.ui.nav.contacts.fragments.NSContactsTabFragment;
import com.nearsport.nearsport.ui.nav.discover.fragments.NSDiscoverTabFragment;
import com.nearsport.nearsport.ui.nav.nearby.fragments.NSMapTabFragment;
import com.nearsport.nearsport.ui.nav.me.fragments.NSMeTabFragment;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainTabActivity
        extends
            AppCompatActivity
        implements
            ActivityCompat.OnRequestPermissionsResultCallback,
            IUserService.GetContactsListener
{
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private CoordinatorLayout coordinatorLayout;

    @Bind(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;

    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    private Fragment currentFragment;

    private FragmentManager fragmentManager = getFragmentManager();

    @Inject
    NSMapTabFragment nearSportMapFragment;

    @Inject
    NSMeTabFragment nearSportMeFragment;

    @Inject
    NSChatTabFragment nearSportChatFragment;

    @Inject
    NSDiscoverTabFragment nearSportDiscoverFragment;

    @Inject
    NSContactsTabFragment nearSportContactsFragment;

    @Inject
    NearSportSessionManager sessionManager;

    @Inject
    public NSPermissionManager permissionManager;

    @Inject
    UserService userService;

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);
        ButterKnife.bind(this);
        NearSportApplication.component().inject(this);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_tab_coordinator_layout);
        permissionManager.requestLocationPermission(this);
        initUI();
        initDBFromServer();
    }

    /*
        init Map Tab
     */
    private void initMapTab() {
        nearSportMapFragment.setActivity(this);
    }

    /*
    init Contacts Tab
     */
    private void initContactsTab() {
        nearSportContactsFragment.setActivity(this);
    }

    /*
    init Me Tab
     */
    private void initMeTab() {
        nearSportMeFragment.setActivity(this);
    }

    /*
    init Chat Tab
     */
    private void initChatTab() {
        this.nearSportChatFragment.setActivity(this);
    }

    /*
    init Discover Tab
     */
    private void initDiscoverTab() {
        nearSportDiscoverFragment.setActivity(this);
    }

    /*
    init nav tabs
     */
    private void initNavTabs() {
        if (bottomNavigation == null) {
            bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        }
        AHBottomNavigationItem itemNearby = new AHBottomNavigationItem(R.string.com_nearsport_ui_nav_bottom_tab_nearby, R.drawable.ic_location_on_black_24dp, R.color.color_tab_1);
        AHBottomNavigationItem itemChat = new AHBottomNavigationItem(R.string.com_nearsport_ui_nav_bottom_tab_chat, R.drawable.ic_chat_black_24dp, R.color.color_tab_2);
        AHBottomNavigationItem itemContacts = new AHBottomNavigationItem(R.string.com_nearsport_ui_nav_bottom_tab_contacts, R.drawable.ic_contacts_black_24dp, R.color.color_tab_3);
        AHBottomNavigationItem itemDiscover = new AHBottomNavigationItem(R.string.com_nearsport_ui_nav_bottom_tab_discover, R.drawable.ic_explore_black_24dp, R.color.color_tab_3);
        AHBottomNavigationItem itemMe = new AHBottomNavigationItem(R.string.com_nearsport_ui_nav_bottom_tab_me, R.drawable.ic_person_black_24dp, R.color.color_tab_3);

        bottomNavigationItems.add(itemNearby);
        bottomNavigationItems.add(itemChat);
        bottomNavigationItems.add(itemContacts);
        bottomNavigationItems.add(itemDiscover);
        bottomNavigationItems.add(itemMe);

        bottomNavigation.addItems(bottomNavigationItems);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, boolean wasSelected) {
                Toast.makeText(getBaseContext(), "tab selected: " + position, Toast.LENGTH_SHORT).show();
                switch (position) {
                    case Constant.UI_NAV_TAB_NEARBY: {
                        switchToMapView();
                        break;
                    }
                    case Constant.UI_NAV_TAB_CHAT: {
                        switchToChatView();
                        break;
                    }
                    case Constant.UI_NAV_TAB_DISCOVER: {
                        switchToDiscoverView();
                        break;
                    }
                    case Constant.UI_NAV_TAB_ME: {
                        switchToMeView();
                        break;
                    }
                    case Constant.UI_NAV_TAB_CONTACTS: {
                        switchToContactsView();
                        break;
                    }
                }
            }
        });
        switchToMapView();
    }

    /*
    init UI
     */
    private void initUI() {
        initMapTab();
        initMeTab();
        initChatTab();
        initContactsTab();
        initDiscoverTab();
        initNavTabs();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private void switchToView(Fragment fragment) {
        currentFragment = fragment;

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, currentFragment)
                .commit();
    }

    private void switchToMapView() {
        switchToView(nearSportMapFragment);
    }

    private void switchToMeView() {
        switchToView(nearSportMeFragment);
    }

    private void switchToChatView() {
        switchToView(nearSportChatFragment);
    }

    private void switchToDiscoverView() {
        switchToView(nearSportDiscoverFragment);
    }

    private void switchToContactsView() {
        switchToView(nearSportContactsFragment);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case NSPermissionManager.LOCATION_RESULT:
                if (PermissionUtils.isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    this.nearSportMapFragment.tryUpdateLocation();
                } else {
                    PermissionUtils.makePostRequestSnack(coordinatorLayout, "Location is required to access nearby games", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
                break;
        }
    }

    private void initDBFromServer() {
        if(Util.isDBEmpty()) {
//            userService.getContacts(sessionManager.getUserId(), sessionManager.getSessionToken(), this);
        }
    }

    @Override
    public void onGetContactsSuccess(List<PersonSkeleton> response) {
        if(null != response) {
            for(PersonSkeleton personSkeleton : response) {
                personSkeleton.save();
            }
        }
    }

    @Override
    public void onGetContactsFailure(Throwable t) {

    }
}
