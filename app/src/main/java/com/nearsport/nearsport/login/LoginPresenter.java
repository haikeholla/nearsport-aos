package com.nearsport.nearsport.login;

import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.http.rest.service.IUserService;
import com.nearsport.nearsport.presenter.BasePresenter;
import com.nearsport.nearsport.util.Constant;

/**
 * Created by sguo on 6/10/16.
 */
public class LoginPresenter implements BasePresenter<LoginView>,  IUserService.DoLoginListener {
    private LoginView loginView;
    private IUserService userService;
    public LoginPresenter(IUserService userService) {
        this.userService = userService;
    }
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(LoginView view) {
        this.loginView = view;
    }

    @Override
    public void onDoLoginSuccess(DoLoginResponse response) {
        if(null != loginView) {
            loginView.showLoginSuccess(response);
        }
    }

    @Override
    public void onDoLoginFailure(Throwable t) {
        if(null != loginView) {
            loginView.showLoginFailure(t);
        }
    }

    public void login(UserLoginRequest request) {
        if(null != userService) {
            if(null != loginView) {
                loginView.showProgressing();
            }
            if(validate(request)) {
                userService.doLogin(request, this);
            }
        }
    }

    public boolean validate(UserLoginRequest request) {
        boolean valid = true;

        String email = request.email;
        String password = request.password;

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginView.showEmailError("enter a valid email address");
            valid = false;
        }

        if (password.isEmpty() || password.length() < Constant.USER_PASSWORD_MIN_LEN || password.length() > Constant.USER_PASSWORD_MAX_LEN) {
            loginView.showPasswordError("between " + Constant.USER_PASSWORD_MIN_LEN + " and " + Constant.USER_PASSWORD_MAX_LEN + " alphanumeric characters");
            valid = false;
        }

        return valid;
    }
}
