package com.nearsport.nearsport.login;

import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.presenter.View;

/**
 * Created by sguo on 6/10/16.
 */
public interface LoginView extends View {
    void showProgressing();
    void showLoginSuccess(DoLoginResponse res);
    void showLoginFailure(Throwable t);

    void showEmailError(String msg);
    void showPasswordError(String msg);
}
