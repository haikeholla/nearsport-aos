package com.nearsport.nearsport.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nearsport.nearsport.MainTabActivity;
import com.nearsport.nearsport.MapsActivity;
import com.nearsport.nearsport.R;
import com.nearsport.nearsport.application.NearSportApplication;
import com.nearsport.nearsport.http.rest.request.UserLoginRequest;
import com.nearsport.nearsport.http.rest.response.DoLoginResponse;
import com.nearsport.nearsport.http.rest.service.UserService;
import com.nearsport.nearsport.session.NearSportSessionManager;
import com.nearsport.nearsport.signup.SignupActivity;
import com.nearsport.nearsport.signup.SignupPresenter;
import com.nearsport.nearsport.util.Constant;
import com.nearsport.nearsport.util.Util;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private static final int REQUEST_MAPS_NON_LOGIN = 1;

    @Inject
    public UserService userService;

    @Bind(R.id.input_email)
    EditText emailText;
    @Bind(R.id.input_password)
    EditText passwordText;
    @Bind(R.id.btn_login)
    Button loginButton;
    @Bind(R.id.link_signup)
    TextView signupLink;
    @Bind(R.id.link_maps_non_login)
    TextView mapsLink;

    private ProgressDialog progressDialog;

    @Inject
    public NearSportSessionManager sessionManager;

    private LoginPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        NearSportApplication.component().inject(this);
        ButterKnife.bind(this);
        initUI();
        initializePresenter();
    }

    public void initUI() {
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the login activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
            }
        });

        mapsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivityForResult(intent, REQUEST_MAPS_NON_LOGIN);
                finish();
            }
        });

        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
    }

    private void initializePresenter() {
        presenter = new LoginPresenter(userService);
        presenter.attachView(this);
    }

    public void login() {
        Log.d(TAG, "Login");
        UserLoginRequest req = new UserLoginRequest();
        req.email = emailText.getText().toString();
        req.password = passwordText.getText().toString();
        presenter.login(req);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void toggleProgressDialog() {
        if(!progressDialog.isShowing()) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void goToMainLanding(String msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), MainTabActivity.class);
        intent.putExtra(Constant.AUTH_TOKEN, msg);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoginSuccess(DoLoginResponse response) {
        progressDialog.dismiss();
        loginButton.setEnabled(true);
        if(null != response && response.status == 0) {
            sessionManager.createLoginSession(response.personSkeleton);
            this.goToMainLanding(response.personSkeleton.authToken);
        } else {
            Log.e(TAG, response.msg);
            Toast.makeText(getBaseContext(), response.msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showLoginFailure(Throwable t) {
        Log.e(TAG, t.toString());
        loginButton.setEnabled(true);
        progressDialog.dismiss();
        Toast.makeText(getBaseContext(), t.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmailError(String msg) {
        if(!Util.stringIsNullOrEmpty(msg)) {
            emailText.setError(msg);
        }
    }

    @Override
    public void showPasswordError(String msg) {
        if(!Util.stringIsNullOrEmpty(msg)) {
            passwordText.setError(msg);
        }
    }

    @Override
    public void showProgressing() {
        loginButton.setEnabled(false);
        progressDialog.show();
    }
}
