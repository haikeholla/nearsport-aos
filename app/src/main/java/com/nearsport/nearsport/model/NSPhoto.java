package com.nearsport.nearsport.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by sguo on 7/21/16.
 */
@Table(name = "NSPhoto")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NSPhoto extends Model implements Parcelable {

    @Column(name = "sid", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public long id;

    @Column(name = "localPath")
    public String localPath;

    @Column(name = "remoteUrl")
    public String url;

    @Column(name = "post")
    public UserPost post;

    public List<UserComment> comments;

    public List<UserLike> likes;

    public NSPhoto(UserPost userPost) {
        super();
        this.post = userPost;
    }

    public NSPhoto(NSPhoto photo, UserPost userPost) {
        this(userPost);
        this.id = photo.id;
        this.url = photo.url;
        this.localPath = photo.localPath;
    }

    public NSPhoto() {
        super();
    }

    public NSPhoto(Parcel in) {
        this.id = in.readLong();
        this.localPath = in.readString();
        this.url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.localPath);
        dest.writeString(this.url);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public NSPhoto createFromParcel(Parcel in) {
            return new NSPhoto(in);
        }

        public NSPhoto[] newArray(int size) {
            return new NSPhoto[size];
        }
    };
}
