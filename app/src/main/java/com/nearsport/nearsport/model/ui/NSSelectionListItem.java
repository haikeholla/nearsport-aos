package com.nearsport.nearsport.model.ui;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.view.View;

import com.nearsport.nearsport.model.UserPost;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by sguo on 5/4/16.
 */
public class NSSelectionListItem extends NSBaseListItem {

    public String leftIconUrl;
    public String rightIconUrl;
    public String detail;
    public View.OnClickListener onClickListener;
    public View.OnLongClickListener onLongClickListener;
    public int backgroudColor;
    public boolean isSelected;
    public UserPost userPost;
    public static final View.OnClickListener EMPTY_ON_CLICK_LISTENER = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public NSSelectionListItem createFromParcel(Parcel in) {
            return new NSSelectionListItem(in);
        }

        public NSSelectionListItem[] newArray(int size) {
            return new NSSelectionListItem[size];
        }
    };

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener != null ? onClickListener : EMPTY_ON_CLICK_LISTENER;
    }

    public void setLeftIcon(int leftIcon) {
        this.leftIcon = leftIcon;
    }

    public int getLeftIcon() {
        return leftIcon;
    }

    public int getRightIcon() {
        return rightIcon;
    }

    public boolean leftIconUseSystemDrawable;
    public boolean rightIconUseSystemDrawable;

    public @DrawableRes int leftIcon;
    public @DrawableRes int rightIcon;
    public @DrawableRes int rightMostIcon;

    public NSSelectionListItem() {
        super();
    }

    public NSSelectionListItem(Parcel in) {
        super(in);
        this.detail = in.readString();
        this.leftIconUrl = in.readString();
        this.rightIconUrl = in.readString();
        this.leftIcon = in.readInt();
        this.rightIcon = in.readInt();
        this.isSelected = in.readInt() > 0;
        this.backgroudColor = in.readInt();
        this.rightMostIcon = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.detail);
        dest.writeString(leftIconUrl);
        dest.writeString(rightIconUrl);
        dest.writeInt(leftIcon);
        dest.writeInt(rightIcon);
        dest.writeInt(isSelected ? 1 : 0);
        dest.writeInt(backgroudColor);
        dest.writeInt(rightMostIcon);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType name) {
        this.type = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
