package com.nearsport.nearsport.model.ui;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sguo on 7/10/16.
 */
public class NSMyPostListItem extends NSBaseListItem {
    public List<NSSelectionListItem> contentListItemList = new ArrayList<>();
}
