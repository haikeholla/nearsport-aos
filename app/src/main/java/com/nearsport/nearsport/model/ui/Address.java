package com.nearsport.nearsport.model.ui;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sguo on 5/8/16.
 */
public class Address implements Parcelable {
    public String street;
    public String city;
    public String state;
    public String zipcode;
    public String country;

    public Address() {}

    public Address(String street, String city, String state, String zipcode, String country) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.country = country;
    }

    public Address(Parcel in) {
        this.street = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.zipcode = in.readString();
        this.country = in.readString();
    }

    public Address(String street, String city, String state, String zipcode) {
        this(street, city, state, zipcode, "USA");
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.zipcode);
        dest.writeString(this.country);
    }
}
