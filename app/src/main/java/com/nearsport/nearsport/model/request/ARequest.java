package com.nearsport.nearsport.model.request;

import java.sql.Timestamp;

/**
 * Created by sguo on 4/30/16.
 */
public class ARequest {
    public Long rid;

    public String msg;

    public Timestamp timestamp;

    public RequestStatus status;

    public ARequest() {
        this.status = RequestStatus.PENDING;
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    public static enum RequestStatus {
        REJECT,
        PENDING,
        GRANT;
    }

    public void reject() {
        this.status = RequestStatus.REJECT;
    }

    public void grant() {
        this.status = RequestStatus.GRANT;
    }
}
