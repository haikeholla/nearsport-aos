
package com.nearsport.nearsport.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "pid",
        "authToken",
        "email",
        "fname",
        "lname",
        "phoneNum",
        "username",
        "gender",
        "iconUrl"
})
@Table(name = "Persons")
public class PersonSkeleton extends Model implements Parcelable {

    @JsonProperty("pid")
    @Column(name = "pid", index = true)
    public Long pid;
    @JsonProperty("authToken")
    public String authToken;
    @JsonProperty("email")
    public String email;
    @JsonProperty("fname")
    public String fname;
    @JsonProperty("lname")
    public String lname;
    @JsonProperty("phoneNum")
    public String phoneNum;
    @JsonProperty("username")
    @Column(name = "username")
    public String username;
    @JsonProperty("gender")
    @Column(name = "gender")
    public String gender;
    @JsonProperty("iconUrl")
    @Column(name = "iconUrl")
    public String iconUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PersonSkeleton() {}

    public PersonSkeleton(Parcel in) {
        this.pid = in.readLong();
        this.authToken = in.readString();
        this.email = in.readString();
        this.fname = in.readString();
        this.lname = in.readString();
        this.phoneNum = in.readString();
        this.username = in.readString();
        this.gender = in.readString();
        this.iconUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.pid);
        dest.writeString(this.authToken);
        dest.writeString(this.email);
        dest.writeString(this.fname);
        dest.writeString(this.lname);
        dest.writeString(this.phoneNum);
        dest.writeString(this.username);
        dest.writeString(this.gender);
        dest.writeString(this.iconUrl);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public PersonSkeleton createFromParcel(Parcel in) {
            return new PersonSkeleton(in);
        }

        public PersonSkeleton[] newArray(int size) {
            return new PersonSkeleton[size];
        }
    };
}
