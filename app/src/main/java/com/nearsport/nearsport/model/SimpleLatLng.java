package com.nearsport.nearsport.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sguo on 4/28/16.
 */
public class SimpleLatLng implements Parcelable {
    public double latitude;
    public double longitude;

    public SimpleLatLng() {}

    public SimpleLatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public SimpleLatLng(Parcel in) {

        double[] data = new double[3];

        in.readDoubleArray(data);
        this.latitude = data[0];
        this.longitude = data[1];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeDoubleArray(new double[] {this.latitude, this.longitude});
    }


    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public SimpleLatLng createFromParcel(Parcel in) {
            return new SimpleLatLng(in);
        }

        public SimpleLatLng[] newArray(int size) {
            return new SimpleLatLng[size];
        }
    };
}
