package com.nearsport.nearsport.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.nearsport.nearsport.model.ui.Address;

/**
 * Created by sguo on 4/30/16.
 */
public class Game extends GameSkeleton {
    public PersonSkeleton creator;

    public Game() {}

    public Game(Parcel in) {
        this.gid = in.readLong();
        this.name = in.readString();
        this.location = in.readParcelable(SimpleLatLng.class.getClassLoader());
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.startTime = in.readString();
        this.endTime = in.readString();
        this.description = in.readString();
        this.sportType = in.readString();
        this.status = in.readString();
        this.accessLevel = in.readString();
        this.creator = in.readParcelable(PersonSkeleton.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.gid);
        dest.writeString(this.name);
        dest.writeParcelable(this.location, flags);
        dest.writeParcelable(this.address, flags);
        dest.writeString(this.startTime);
        dest.writeString(this.endTime);
        dest.writeString(this.description);
        dest.writeString(this.sportType);
        dest.writeString(this.status);
        dest.writeString(this.accessLevel);
        dest.writeParcelable(this.creator, flags);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        public Game[] newArray(int size) {
            return new Game[size];
        }
    };
}
