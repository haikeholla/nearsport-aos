package com.nearsport.nearsport.model.ui;

import android.support.annotation.DrawableRes;

import com.nearsport.nearsport.util.Util;

/**
 * Created by sguo on 5/6/16.
 */
public class ChatListItem extends ListItem {

    public enum ChatType {
        INDIVIDUAL,
        GROUP;
    }
    public String lastMsger;
    public String lastMsg;
    public ChatType chatType;

//    public ChatListItem(String title, String icon, ChatType chatType, String lastMsger, String lastMsg) {
//        super(ItemType.CHAT, title, icon);
//        this.lastMsg = lastMsg;
//        this.lastMsger = lastMsger;
//        this.chatType = chatType;
//    }
//
//    public ChatListItem(String title, @DrawableRes int drawableRes, ChatType chatType, String lastMsger, String lastMsg) {
//        super(ItemType.CHAT, title, drawableRes);
//        this.lastMsger = lastMsger;
//        this.lastMsg = lastMsg;
//        this.chatType = chatType;
//    }

    public String getDisplayContent(int limit) {
        switch (chatType) {
            case INDIVIDUAL: {
                return Util.abbrieveate(lastMsg, limit);
            }
            case GROUP: {
                return Util.abbrieveate(lastMsger + ":" + lastMsg, limit);
            }
            default:
                return "";
        }
    }
}
