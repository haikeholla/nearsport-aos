package com.nearsport.nearsport.model.tag;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sguo on 7/17/16.
 */
public class NSTag {
    @JsonProperty("name")
    public String name;

    @Override
    public String toString() {
        return name;
    }
}
