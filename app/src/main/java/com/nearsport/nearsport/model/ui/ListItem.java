package com.nearsport.nearsport.model.ui;

import android.content.ClipData;
import android.support.annotation.DrawableRes;

import com.nearsport.nearsport.ui.Size;

/**
 * Created by sguo on 5/4/16.
 */
public class ListItem {

    public static enum ItemType {
        SEPARATOR(0),
        REGULAR(1),
        CHAT(2),
        USER_PROF(3),
        SECTION_HEADER(4),
        SETTINGS(5);

        public int code;
        ItemType(int code) {
            this.code = code;
        }

        public static ItemType valueOf(int code) {
            switch(code) {
                case 1: return REGULAR;
                case 2: return CHAT;
                case 3: return USER_PROF;
                case 4: return SECTION_HEADER;
                case 5: return SETTINGS;
                default: return REGULAR;
            }
        }
    }

    private String leftIconUrl;
    private String rightIconUrl;
    private String title;

    public int getRightMostIcon() {
        return rightMostIcon;
    }

    public String getLeftIconUrl() {
        return leftIconUrl;
    }

    public String getRightIconUrl() {
        return rightIconUrl;
    }

    public boolean isLeftIconUseSystemDrawable() {
        return leftIconUseSystemDrawable;
    }

    public boolean isRightIconUseSystemDrawable() {
        return rightIconUseSystemDrawable;
    }

    public void setLeftIconUrl(String leftIconUrl) {
        this.leftIconUrl = leftIconUrl;
    }

    public void setRightIconUrl(String rightIconUrl) {
        this.rightIconUrl = rightIconUrl;
    }

    public void setLeftIconUseSystemDrawable(boolean leftIconUseSystemDrawable) {
        this.leftIconUseSystemDrawable = leftIconUseSystemDrawable;
    }

    public void setRightIconUseSystemDrawable(boolean rightIconUseSystemDrawable) {
        this.rightIconUseSystemDrawable = rightIconUseSystemDrawable;
    }

    public void setLeftIcon(int leftIcon) {
        this.leftIcon = leftIcon;
    }

    public void setRightIcon(int rightIcon) {
        this.rightIcon = rightIcon;
    }

    public void setRightMostIcon(int rightMostIcon) {
        this.rightMostIcon = rightMostIcon;
    }

    public int getLeftIcon() {
        return leftIcon;
    }

    public int getRightIcon() {
        return rightIcon;
    }

    private boolean leftIconUseSystemDrawable;
    private boolean rightIconUseSystemDrawable;
    private ItemType type;

    private @DrawableRes int leftIcon;
    private @DrawableRes int rightIcon;
    private @DrawableRes int rightMostIcon;

//    private boolean isSeparator = true;

//    private Size size = new Size(-1, -1);

    public ListItem() {
    }

//    public static class Builder {
//        public ListItem build();
//    }
//
//    public ListItem(ItemType type, String title, String leftIconUrl, String rightIconUrl) {
//        this(type, title);
//        this.leftIconUrl = leftIconUrl;
//        this.rightIconUrl = rightIconUrl;
//        this.useSystemDrawable = false;
//    }
//
//    public ListItem(ItemType type, String title) {
//        this.type = type;
//        this.title = title;
//        this.isSeparator = false;
//    }
//
//    public ListItem(ItemType type, String title, @DrawableRes int leftIcon) {
//        this(type, title);
//        this.leftIcon = leftIcon;
//        this.useSystemDrawable = true;
//    }
//
//    public String getIconUrl() {
//        return iconUrl;
//    }
//
//    public void setIconUrl(String iconUrl) {
//        this.iconUrl = iconUrl;
//    }
//
    public ItemType getType() {
        return type;
    }

    public void setType(ItemType name) {
        this.type = name;
    }
//
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
//
//    public @DrawableRes int getDrawableRes() {
//        return drawableRes;
//    }
//
//    public boolean isUseSystemDrawable() {
//        return useSystemDrawable;
//    }
//
//    public boolean isSeparator() {
//        return isSeparator;
//    }
//
//    public ListItem setLayoutHeight(int h) {
//        size.h = h;
//        return this;
//    }
//
//    public int getLayoutHeight() { return size.h; }
//
//
//    public ListItem setLayoutWidth(int w) {
//        size.w = w;
//        return this;
//    }
//
//    public int getLayoutWidth() { return size.w; }


}
