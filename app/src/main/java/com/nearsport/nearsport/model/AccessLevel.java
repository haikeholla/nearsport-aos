package com.nearsport.nearsport.model;

/**
 * Created by sguo on 4/30/16.
 */
public enum AccessLevel {
    PUBLIC(1),
    PRIVATE(2),
    APP_MEMBER_ONLY(3);

    private int code;
    AccessLevel(int code) {
        this.code = code;
    }

    public static AccessLevel fromInt(int code) {
        switch(code) {
            case 1: return PUBLIC;
            case 2: return PRIVATE;
            case 3: return APP_MEMBER_ONLY;
            default: return PUBLIC;
        }
    }

    public static AccessLevel fromString(String level) {
        switch (level.toLowerCase()) {
            case "private" : return PRIVATE;
            case "app_member_only": return APP_MEMBER_ONLY;
            default: return PUBLIC;
        }
    }
}
