package com.nearsport.nearsport.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;

/**
 * Created by sguo on 8/6/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserComment {
    public long id;
    public long commenterId;
    public String commenterUserName;
    public String comment;
    public Timestamp createdTime;
}
