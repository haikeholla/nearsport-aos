package com.nearsport.nearsport.model.request;

import com.nearsport.nearsport.model.Person;
import com.nearsport.nearsport.model.Team;

/**
 * Created by sguo on 4/30/16.
 */
public class UserJoinTeamRequest extends ARequest {

    public Person requestor;

    public Team team;

    public UserJoinTeamRequest() {
        super();
    }
}
