package com.nearsport.nearsport.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nearsport.nearsport.http.rest.request.ComposePostRequest;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sguo on 7/21/16.
 */
@Table(name = "UserPost")
public class UserPost extends Model implements Parcelable {
    @JsonProperty("id")
    @Column(name = "sid", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public long sid;
    @JsonProperty("content")
    @Column(name = "content")
    public String content;
    @JsonProperty("policyCode")
    @Column(name = "policyCode")
    public int policyCode;
    @JsonProperty("poiTag")
    @Column(name = "poiTag")
    public String poiTag;

    @JsonProperty("photos")
    public List<NSPhoto> photos() {
        return getMany(NSPhoto.class, "post");
    }

    public ArrayList<NSPhoto> photos = new ArrayList<>();

    @JsonProperty("mentionedContacts")
    @Column(name = "mentionedContacts")
    public String mentionedContacts;

    @Column(name = "createdTime")
    public Date createdTime;
//
//    @Column(name = "createdDate")
//    public Date createdDate;

    public UserPost(ComposePostRequest request) {
        super();
        this.content = request.content;
        this.createdTime = request.createdTime;
        this.policyCode = request.policyCode;
        this.poiTag = request.poiTag.toString();
    }

    public UserPost() {
        super();
    }

    public UserPost(Parcel in) {
        this.sid = in.readLong();
        this.content = in.readString();
        this.policyCode = in.readInt();
        this.poiTag = in.toString();
        this.mentionedContacts = in.readString();
        this.createdTime = Date.valueOf(in.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.sid);
        dest.writeString(this.content);
        dest.writeInt(this.policyCode);
        dest.writeString(this.poiTag);
        dest.writeString(this.mentionedContacts);
        dest.writeString(this.createdTime.toString());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public UserPost createFromParcel(Parcel in) {
            return new UserPost(in);
        }

        public UserPost[] newArray(int size) {
            return new UserPost[size];
        }
    };
}
