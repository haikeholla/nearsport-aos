package com.nearsport.nearsport.model;

import com.nearsport.nearsport.model.request.UserJoinTeamRequest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sguo on 4/30/16.
 */
public class Person  {

    public PersonSkeleton skeleton;

    public Set<Team> coachedTeams = new HashSet<>();

    public Set<Team> captainedTeams = new HashSet<>();

    public Set<Team> joinedTeams = new HashSet<>();

    public Set<Game> joinedGames = new HashSet<>();

    public List<Game> createdGames = new ArrayList<>();

    public List<Team> createdTeams = new ArrayList<>();

    public List<UserJoinTeamRequest> requests = new ArrayList<>();
}
