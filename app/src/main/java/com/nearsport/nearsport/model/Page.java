package com.nearsport.nearsport.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sguo on 4/30/16.
 */
public class Page<T> {
    public int number;
    public int size;
    public int numberOfElements;
    public List<T> content = new ArrayList<>();
    public int totalPages;
    public int totalElements;
    public Sort sort;
    public boolean last;
    public boolean first;
}
