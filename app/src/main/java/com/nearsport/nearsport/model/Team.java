package com.nearsport.nearsport.model;

import com.nearsport.nearsport.model.request.UserJoinTeamRequest;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sguo on 4/30/16.
 */
public class Team extends TeamSkeleton {

    public Person captain;

    public Person coach;

    public Set<Game> attendGames = new HashSet<>();

    public Set<Person> members = new HashSet<>();

    private Timestamp foundTime;

    public Person creator;

    public Set<UserJoinTeamRequest> userJoinTeamRequestBox = new HashSet<>();
}
