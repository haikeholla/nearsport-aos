package com.nearsport.nearsport.model.ui;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nearsport.nearsport.ui.adapter.NSBaseNavTabRecyclerAdapter;

/**
 * Created by sguo on 7/10/16.
 */
public class NSBaseListItem implements Parcelable{

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public NSBaseListItem createFromParcel(Parcel in) {
            return new NSBaseListItem(in);
        }

        public NSBaseListItem[] newArray(int size) {
            return new NSBaseListItem[size];
        }
    };

    public NSBaseListItem() {}

    public NSBaseListItem(Parcel in) {
        this.title = in.readString();
        this.type = ItemType.valueOf(in.readInt());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(type.code);
    }

    public static enum ItemType {
        SEPARATOR(0),
        REGULAR(1),
        CHAT(2),
        USER_PROF(3),
        SECTION_HEADER(4),
        SETTINGS(5),
        CREATE_POST(6),
        MENTIONED_CONTACT(7),
        ADD_ITEM_BUTTON(8),
        ;

        public int code;
        ItemType(int code) {
            this.code = code;
        }

        public static ItemType valueOf(int code) {
            switch(code) {
                case 1: return REGULAR;
                case 2: return CHAT;
                case 3: return USER_PROF;
                case 4: return SECTION_HEADER;
                case 5: return SETTINGS;
                case 6: return CREATE_POST;
                case 7: return MENTIONED_CONTACT;
                case 8: return ADD_ITEM_BUTTON;
                default: return REGULAR;
            }
        }
    }

    public String title = "";

    public ItemType type = ItemType.REGULAR;
}
