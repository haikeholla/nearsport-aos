package com.nearsport.nearsport.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

import com.google.android.gms.maps.model.LatLng;
import com.nearsport.nearsport.model.ui.Address;
import com.nearsport.nearsport.util.Util;

/**
 * Created by sguo on 4/28/16.
 */
public class GameSkeleton implements Parcelable {
    public long gid;
    public String name;
    public SimpleLatLng location;
    public Address address;
    public String startTime;
    public String endTime;
    public String description;
    public String sportType;
    public String status;
    public String accessLevel;

    public GameSkeleton() {}

    public GameSkeleton(Parcel in) {
        this.gid = in.readLong();
        this.name = in.readString();
        this.location = in.readParcelable(SimpleLatLng.class.getClassLoader());
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.startTime = in.readString();
        this.endTime = in.readString();
        this.description = in.readString();
        this.sportType = in.readString();
        this.status = in.readString();
        this.accessLevel = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.gid);
        dest.writeString(this.name);
        dest.writeParcelable(this.location, flags);
        dest.writeParcelable(this.address, flags);
        dest.writeString(this.startTime);
        dest.writeString(this.endTime);
        dest.writeString(this.description);
        dest.writeString(this.sportType);
        dest.writeString(this.status);
        dest.writeString(this.accessLevel);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public GameSkeleton createFromParcel(Parcel in) {
            return new GameSkeleton(in);
        }

        public GameSkeleton[] newArray(int size) {
            return new GameSkeleton[size];
        }
    };

    public static enum SportType {
        BASKETBALL(0),
        VOLLEYBALL(1),
        SOCCER(2),
        FOOTBALL(3),
        TENNIS(4),
        TABLE_TENNIS(5),
        UNKNOWN(-1);

        private int code;
        public static final String[] nameArr = Util.getNames(SportType.class);
        SportType(int code) {
            this.code = code;
        }
        public static SportType fromString(String str) {
            switch(str.toLowerCase()) {
                case "basketball" :
                    return BASKETBALL;
                case "volleyball" :
                    return VOLLEYBALL;
                case "soccer" :
                    return SOCCER;
                case "football" :
                    return FOOTBALL;
                case "tennis" :
                    return TENNIS;
                case "table_tennis" :
                    return TABLE_TENNIS;
                default:
                    return UNKNOWN;
            }
        }

        public static String[] toStringArray() {
            return nameArr;
        }
    }

    public static enum GameStatus {
        SCHEDULED,
        CANCELED,
        UNKNOWN;

        public static GameStatus fromString(String str) {
            switch (str) {
                case "canceled":
                    return CANCELED;
                case "SCHEDULED":
                    return SCHEDULED;
                default:
                    return UNKNOWN;
            }
        }
    }
}
