package com.nearsport.nearsport.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;

/**
 * Created by sguo on 8/6/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLike {
    public long id;
    public long likerId;
    public long likerUserName;
    public Timestamp createdTime;
}
