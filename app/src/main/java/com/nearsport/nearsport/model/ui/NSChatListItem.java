package com.nearsport.nearsport.model.ui;

import com.nearsport.nearsport.util.Util;

/**
 * Created by sguo on 5/6/16.
 */
public class NSChatListItem extends NSSelectionListItem {

    public enum ChatType {
        INDIVIDUAL,
        GROUP;
    }
    public String lastMsger;
    public String lastMsg;
    public ChatType chatType;

    public String getDisplayContent(int limit) {
        switch (chatType) {
            case INDIVIDUAL: {
                return Util.abbrieveate(lastMsg, limit);
            }
            case GROUP: {
                return Util.abbrieveate(lastMsger + ":" + lastMsg, limit);
            }
            default:
                return "";
        }
    }
}
